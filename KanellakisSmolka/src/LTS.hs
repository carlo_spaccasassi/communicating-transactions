module LTS where

import Data.Graph.Inductive.PatriciaTree
import Data.Graph.Inductive.Graph
import Data.Graph.Inductive.Graphviz
import Data.Unique
import qualified Data.List as L
import qualified Data.Set  as S
import Prelude hiding ( id )


------------------------------------------------------------------------------
-- LTS and Bisimulation definitions
------------------------------------------------------------------------------
type Label           = String             -- decorates a state
type Action          = String             -- identifies a state transition
type State           = LNode Label        -- A labeled state, together with
                                          -- an internal unique ID (Integer)
type Transition      = LEdge Action       -- a labelled edge; it must contain 
                                          -- the IDs of the nodes it connects
type TransitionGraph = Gr Label Action   -- a graph whose nodes hold a String
                                          -- and whose edges hold an Action
type Block           = S.Set Node         -- a set of unlabelled states; it only
                                          -- contains the unique ID of a State,
                                          -- without the label
type Bisimulation    = S.Set Block        -- a bisimulation is a collection of
                                          -- equivalence blocks
type Splitter        = Maybe (Block, Block, Block )
type LTS = (  S.Set State,                -- the set of states of the system 
              S.Set Action,               -- the set of actions 
              TransitionGraph)            -- the transition relation

------------------------------------------------------------------------------
-- Creation utilities
------------------------------------------------------------------------------

createState :: String -> IO State
createState s = do
  uniqueId <- newUnique             -- generates unique ID
  let id = hashUnique uniqueId 
  return ( id, s )                  -- an LNode is just a tuple :: (Integer, a) 

createTransition :: State -> State -> Action -> Transition 
createTransition (n1, _) (n2, _) a = (n1, n2, a)

mkLTS :: [State] -> [Transition] -> [Action] -> LTS
mkLTS s t a = (S.fromList s, S.fromList a, mkGraph s t ) 


------------------------------------------------------------------------------
-- Printing routines to GraphViz for LTS and bisimulation.
-- Modified from the source code of Data.Graph.Inductive.Graphviz 
------------------------------------------------------------------------------
sq :: String -> String
sq s@[_]                     = s
sq ('"':s)  | last s == '"'  = init s
      | otherwise      = s
sq ('\'':s) | last s == '\'' = init s
      | otherwise      = s
sq s                         = s


sl :: (Show a) => a -> String
sl a =
    let l = sq (show a)
    in if l /= "()" then " [label = \"" ++ l ++ "\"]" else ""

i2d :: Int -> Double
i2d = fromInteger . toInteger


mygraphviz :: TransitionGraph
                             -> String
                             -> (Double, Double)
                             -> (Int, Int)
                             -> Orient
                             -> [[Int]]
                             -> IO ()
mygraphviz g t (w, h) p@(pw', ph') o bisimulation =
    let n = labNodes g
        e = labEdges g
        ns = concatMap sn n
        es = concatMap se e
        bs = concatMap sb bisimulation
        sz w' h' = if o == Portrait 
                      then show w' ++ "," ++ show h' 
                      else show h' ++ "," ++ show w'
        ps = show w++","++show h
        (pw, ph) = if o == Portrait 
                    then p 
                    else (ph', pw')
        --gs = show ((w*(i2d pw))-m)++","++show ((h*(i2d ph))-m)
        gs = sz (w * i2d pw) (h * i2d ph)
          in putStrLn $ "digraph "++t++" {\n"
            ++"\tmargin = \"0\"\n"
            ++"\tpage = \""++ps++"\"\n"
            ++"\tsize = \""++gs++"\"\n"
            ++"\tratio = \"fill\"\n"
            ++ns
            ++es
            ++"\n\tsubgraph Rel1{\n\t\tedge[dir=none,color=red,style=dotted,constraint=false]\n"
            ++bs
            ++"\t}\n}"
          where 
                se (n1, n2, b) = '\t':(show n1 ++ " -> " ++ show n2 
                                                          ++ sl b ++ "\n")
                sn (n, a) | sl a == ""  = ""
                          | otherwise = '\t':(show n ++ sl a ++ "\n")
                sb b = "\t\t" ++ (L.intercalate " -> " (L.map show b) ++ "\n" )

printLTS :: LTS -> IO ()
printLTS lts = printBisimulation lts S.empty

printBisimulation :: LTS -> Bisimulation -> IO ()
printBisimulation (_, _, g) bisim = do
   let bisim'       = L.map S.toList (S.toList bisim)
   mygraphviz g "fgl" (9.5,7) (1,1) Portrait bisim'
   
   
   
   