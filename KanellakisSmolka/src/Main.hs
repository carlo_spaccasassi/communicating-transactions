module Main where

import LTS
import BisimulationAlgorithm
import TCCS.Defs
import TCCS.Bisim

createLTS1 :: IO LTS
createLTS1 = do
  p1 <- createState "p1"
  p2 <- createState "p2"
  p3 <- createState "p3"
  
  q1 <- createState "q1"
  q2 <- createState "q2"
  
  let a = "a"
  
  let e1 = createTransition p1 p2 a
  let e2 = createTransition p1 p3 a
  let e3 = createTransition q1 q2 a 
  
  let states      = [p1,p2,p3,q1,q2]
  let transitions = [e1,e2,e3]
  let actions     = [a] 
  
  return (mkLTS states transitions actions)




createLTS2 :: IO LTS
createLTS2 = do
  s  <- createState "s"
  s1 <- createState "s1"
  s2 <- createState "s2"
  
  t  <- createState "t"
  t1 <- createState "t1"
  
  let a = "a"
  let b = "b"
  
  let e1 = createTransition s  s1 a
  let e2 = createTransition s  s2 a
  let e3 = createTransition s1 s2 b
  let e4 = createTransition s2 s2 b
  
  let e5 = createTransition t  t1 a
  let e6 = createTransition t1 t1 b 
  
  let states      = [s,s1,s2,t,t1]
  let transitions = [e1,e2,e3,e4,e5,e6]
  let actions     = [a,b] 
  
  return (mkLTS states transitions actions)



createLTS3 :: IO LTS
createLTS3 = do
  s0 <- createState "s0"
  s1 <- createState "s1"
  s2 <- createState "s2"
  s3 <- createState "s3"
  s4 <- createState "s4"
  
  t0 <- createState "t0"
  t1 <- createState "t1"
  t2 <- createState "t2"
  t3 <- createState "t3"
  t4 <- createState "t4"
  
  z0 <- createState "z0"
  z1 <- createState "z1"
  z2 <- createState "z2"
  
  let a = "a"
  let b = "b"
  
  let states      = [s0,s1,s2,s3,s4,t0,t1,t2,t3,t4,z0,z1,z2]
  let transitions = [ 
        createTransition s0 s1 a,
        createTransition s0 s2 a,
        createTransition s1 s3 a,
        createTransition s1 s4 b,
        createTransition s2 s4 a,
        createTransition s3 s0 a,
        createTransition s4 s0 a,
        createTransition t0 t1 a,
        createTransition t0 t3 a,
        createTransition t1 t2 a,
        createTransition t1 t2 b,
        createTransition t2 t0 a,
        createTransition t3 t4 a,
        createTransition t4 t0 a,
        createTransition z0 z1 a,
        createTransition z1 z2 a,
        createTransition z1 z2 b,
        createTransition z2 z0 a
        ]
  let actions     = [a,b] 
  
  return (mkLTS states transitions actions)



createLTS4 :: IO LTS
createLTS4 = do
  s0 <- createState "s0"
  s2 <- createState "s2"
  s1 <- createState "s1"
  s4 <- createState "s4"
  s3 <- createState "s3"
  
  t1 <- createState "t1"
  t0 <- createState "t0"
  t2 <- createState "t2"
  t4 <- createState "t4"
  t3 <- createState "t3"
  t5 <- createState "t5"
  
  let a = "a"
  let b = "b"
  
  let states      = [s0,s1,s2,s3,s4,t0,t1,t2,t3,t4,t5]
  let transitions = [ 
        createTransition s0 s1 a,
        createTransition s0 s2 a,
        createTransition s1 s3 a,
        createTransition s1 s4 b,
        createTransition s2 s4 a,
        createTransition s3 s0 a,
        createTransition s4 s0 a,
        createTransition t0 t1 a,
        createTransition t0 t3 a,
        createTransition t1 t2 a,
        createTransition t1 t2 b,
        createTransition t1 t5 b,
        createTransition t2 t0 a,
        createTransition t3 t4 a,
        createTransition t4 t0 a,
        createTransition t5 t0 a,
        createTransition t5 t4 a
        ]
  let actions     = [a,b] 
  
  return (mkLTS states transitions actions)



createLTS5 :: IO LTS
createLTS5 = do
  s0 <- createState "s0"
  s2 <- createState "s2"
  s1 <- createState "s1"
  s3 <- createState "s3"
  
  t1 <- createState "t1"
  t0 <- createState "t0"
  t2 <- createState "t2"
  t4 <- createState "t4"
  t3 <- createState "t3"
  
  let a = "a"
      b = "b"
      c = "c"
  
  let states      = [s0,s1,s2,s3,t0,t1,t2,t3,t4]
  let transitions = [ 
        createTransition s0 s1 a,
        createTransition s1 s2 b,
        createTransition s1 s3 c,
        
        createTransition t0 t1 a,
        createTransition t0 t2 a,
        createTransition t1 t3 b,
        createTransition t2 t4 c
        ]
  let actions     = [a,b,c] 
  
  return (mkLTS states transitions actions)



createLTS6 :: IO LTS
createLTS6 = do
  s0 <- createState "k0"
  s1 <- createState "k1"
  s2 <- createState "k2"
  s3 <- createState "k3"
  s4 <- createState "co"
  
  t0 <- createState "l0"
  t1 <- createState "l1"
  t2 <- createState "l2"
  t3 <- createState "co"
  
  let a  = "a"
      b  = "b"
      c  = "c"
      co = "d"
  
  let states      = [s0,s1,s2,s3,s4,t0,t1,t2,t3]
  let transitions = [ 
        createTransition s0 s1 a,
        createTransition s1 s2 b,
        createTransition s2 s4 co,
        createTransition s1 s3 c,
        
        createTransition t0 t1 a,
        createTransition t1 t2 b,
        createTransition t2 t3 co
        ]
  let actions     = [a,b,c,co] 
  
  return (mkLTS states transitions actions)



     
p1 :: Proc
p1 =  Tran 0 (Sum [(Act "a", prefix (Act "b") $ Commit nil),
                   (Act "b", prefix (Act "a") $ Commit nil)]) nil
            
        
p2 :: Proc
p2 =  Restr "p"
        (Par
           (Tran 0 (prefix (Act "a") $ prefix (Act "p") $ Commit nil) nil)
           (Tran 1 (prefix (Act "b") $ prefix (Bar "p") $ Commit nil) nil))


main::IO ()
main = do 
   --lts <- createLTS6
   lts1 <- makeLTS p1
   lts2 <- makeLTS p2
   let b = calculateTBisim (lts1, lts2)
               
--   lts <- makeLTS (Restr "p" (Par (Tran 0 (prefix (Act "a") (prefix (Act "p") (Commit nil))) nil) 
--                                  (Tran 1 (prefix (Act "b") (prefix (Bar "p") (Commit nil))) nil))) 
                                  -- Sum [(Tau, Sum []), (Act "a", Sum [])])
   printLTS lts1
   --let bisim = findBisimulation lts
   --printBisimulation lts bisim