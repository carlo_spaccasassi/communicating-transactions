
module BisimulationAlgorithm where

import LTS
import Data.Set ( Set, fold, toList, empty, intersection, fromList, filter, 
                  null, insert, singleton, map, delete)
import qualified Data.Graph.Inductive.Graph as G



getStateBlocks :: Set Block -> G.Node -> Action -> TransitionGraph -> Set Block
getStateBlocks p s a g = 
  -- sReach is the set of successor states of state s after performing action a
  let sReach = Data.Set.fromList 
                $ Prelude.map (\(_, z, _) -> z ) 
                $ Prelude.filter (\(_, _, a') -> a == a' ) $ G.out g s
  -- return the blocks that share some states with sReach  
  in Data.Set.filter 
      (\block -> not ( Data.Set.null (block `intersection` sReach) )) 
      p 


doSplit :: Block -> Action -> TransitionGraph -> Set Block -> Splitter
doSplit b a g p = 
  let s       = head $ toList b
      sBlocks  = getStateBlocks p s a g  
      (bl1, bl2) = fold (\s' (b1, b2) -> if getStateBlocks p s' a g == sBlocks
                                          then ( s' `insert` b1, b2 )
                                          else ( b1, s' `insert` b2 ) 
                 ) (empty, empty) b
  in if Data.Set.null bl2
      then Nothing 
      else Just (b, bl1, bl2) 
  

doSteps :: Set Action -> TransitionGraph -> Bisimulation -> Bisimulation   
doSteps act g p = 
  let maybeSplit = fold                            -- fold over bisimilar blocks
            (\b p' -> case p' of  
                        Just _ -> p'
                        Nothing -> fold (\a p'' -> case p'' of  -- fold over Act  
                                           Just _  -> p''             
                                           Nothing -> doSplit b a g p
                                        ) p' act        
            ) Nothing p  
  in case maybeSplit of
    Nothing         -> p
    Just (b, b1, b2) -> doSteps act g ( b `delete` (b1 `insert` (b2 `insert ` p))) 
 

findBisimulation :: LTS -> Bisimulation
findBisimulation ( ss, act, g ) = doSteps act g (singleton $ Data.Set.map fst ss)