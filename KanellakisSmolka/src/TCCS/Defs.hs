module TCCS.Defs where

import Control.Arrow (second)
import Data.List (intercalate, nub, (\\))
import Data.Maybe ( mapMaybe )
import qualified Data.Map  as M
import qualified Data.Set  as S

import Data.Graph.Inductive.Graph

import LTS

--import Debug.Trace (trace)
import Data.Graph.Inductive.PatriciaTree (Gr)

type Var   = String

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

data Act   =  Act   Action 
            | Bar Action
            | Tau
  deriving ( Eq )

instance Show Act where
  show (Act l) = l
  show (Bar l) = '~' : l
  show Tau     = "tau"



data Beta  = Co   Int
           | Ab   Int
           | New  Int
  deriving ( Eq )

instance Show Beta where
  show (Co  i) = "co "  ++ show i
  show (Ab  i) = "ab "  ++ show i
  show (New i) = "new " ++ show i



data Gamma = CCSAct Act 
          | NewAct Int Act
          | OldAct Int Act
          | Beta Beta
  deriving ( Eq )

instance Show Gamma where
  show (CCSAct a)   = show a
  show (NewAct i a) = show i ++ "(" ++ show a ++ ")_new"
  show (OldAct i a) = show i ++ "(" ++ show a ++ ")_old"
  show (Beta   b)   = show b

instance Ord Gamma where                        -- TODO: find a better ordering
  compare g1 g2 = compare (show g1) (show g2)  
  
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
data Proc  = Sum    [(Act, Proc)]
          | Par    Proc   Proc
          | Restr  Action Proc 
          | V      Var 
          | Rec    Var    Proc
          | Tran   Int    Proc Proc 
          | Commit Proc 
          | Atomic Proc   Proc
  deriving ( Eq )


nil :: Proc
nil = Sum []

prefix :: Act -> Proc -> Proc
prefix a p = Sum [(a, p)]

instance Show Proc where
  show proc = case proc of 
    Sum ps      ->  if Prelude.null ps
                      then  "nil"
                      else  let showBranch (a, p) = show a ++ "." ++ show p
                                branches          = map showBranch ps
                            in intercalate " + " branches 
    Par p q     -> show p ++ " || " ++ show q 
    Restr a p   -> "#" ++ a ++ "." ++ show p 
    V x         -> x
    Rec x p     -> "rec " ++ x ++ "." ++ show p
    Tran k p q  -> "[ " ++ show p ++ " <" ++ show k ++ "> " ++ show q ++ " ]"
    Commit p    -> "co." ++ show p
    Atomic p q  -> "[ " ++ show p ++ " <*> " ++ show q ++ " ]"

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

data Config = Cfg Proc Int
  deriving ( Eq )

instance Show Config where
  show (Cfg p n) = "{ "++ show p ++ " }"++ show n

--instance Ord Config where
--  compare (Cfg p n) (Cfg p' n')
--    | p == p' && n == n' = EQ
--    | n < n'             = LT
--    | otherwise          = GT

data Sigma = Epsilon
           | Sub (Int -> Int)

instance Eq Sigma where
  (==) Epsilon Epsilon = True
  (==) (Sub _) (Sub _) = True
  (==) _       _       = False

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


freshFrom :: Int -> Proc -> Bool
freshFrom i proc = case proc of 
  Par p q     -> freshFrom i p && freshFrom i q
  Restr _ p   -> freshFrom i p
  Tran j _ _  -> i == j 
  _           -> True

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

data TTransition = Tr Config Gamma Config Sigma
  deriving ( Eq )

canSync :: Act -> Act -> Bool
canSync (Act l) (Bar l') = l == l'
canSync (Bar l) (Act l') = l == l'
canSync _       _        = False

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
doAbort :: Proc -> Int -> Proc
doAbort proc i = case proc of 
  Par p q     -> Par     (doAbort p i) (doAbort q i)
  Restr a p   -> Restr a (doAbort p i)
  Tran j p q  -> case compare j i of
                  GT -> Tran (j-1) p q
                  EQ -> q
                  LT -> proc
  _           -> proc


getAborts :: Config -> [TTransition]
getAborts c@(Cfg p n) =
  let ps            = map (doAbort p) [0..n-1]
      cfgs          = map (\x -> Cfg x (n-1)) ps
      mkTr c' i     = Tr c (Beta $ Ab i) c' Epsilon
  in zipWith mkTr cfgs [0 .. n-1]

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
getNewProcs :: Proc -> Int -> [Proc]
getNewProcs proc n = case proc of
  Par p q     -> let ps = getNewProcs p n 
                     qs = getNewProcs p n
                     par'  = map (`Par` q) ps
                     par'' = map (Par p)   qs
                 in par' ++ par''
  Restr a p   -> map (Restr a) (getNewProcs p n)
  Atomic p q  -> [Tran n p q]
  _           -> []
  
getNews :: Config -> [TTransition]
getNews c@(Cfg p n) =
  let ps      = getNewProcs p n
      cfgs    = map (\x -> Cfg x (n+1)) ps
      mkTr c' = Tr c (Beta $ New (n+1)) c' Epsilon
  in map mkTr cfgs
  
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
hasCommit :: Proc -> Bool
hasCommit proc = case proc of 
  Par p q   -> hasCommit p || hasCommit q
  Restr _ p -> hasCommit p
  Commit _  -> True
  _         -> False

canCommit :: Proc -> Int -> Bool
canCommit proc i = case proc of 
  Par p q     -> canCommit p i && canCommit q i
  Restr _ p   -> canCommit p i
  Tran j p _  -> (i /= j) || hasCommit p
  _           -> True

removeCommit :: Proc -> Proc
removeCommit proc = case proc of
  Par p q     -> Par     (removeCommit p) (removeCommit q)
  Restr a p   -> Restr a (removeCommit p)
  Commit p    -> p
  _           -> proc

doCommit :: Proc -> Int -> Proc
doCommit proc i = case proc of 
  Par p q     -> let p' = doCommit p i
                     q' = doCommit q i
                 in Par p' q'
  Restr a p   -> Restr a (doCommit p i)
  Tran j p q  -> case compare j i of
                  GT -> Tran (j-1) p q
                  EQ -> removeCommit p
                  LT -> proc
  _           -> proc
  
getCommits :: Config -> [TTransition]
getCommits c@(Cfg p n) = 
  let 
      committables  = filter (canCommit p) [0..n-1] 
      ps            = map (doCommit p) committables 
      cfgs          = map (\x -> Cfg x (n-1)) ps
      mkTr c' i     = Tr c (Beta $ Co i) c' Epsilon
  in zipWith mkTr cfgs committables

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
init' :: Config -> Config
init' s@(Cfg proc n) = case proc of 
    Par p q     -> let (Cfg p' n')  = init' (Cfg p n)
                       (Cfg q' n'') = init' (Cfg q n')
                   in Cfg (Par p' q') n''  
    Restr a p   -> let (Cfg p' n') = init' (Cfg p n)
                   in Cfg (Restr a p') n' 
    Tran _ p q  -> Cfg (Tran n p q) (n+1)
    _           -> s

initState :: Proc -> Config
initState p = init' (Cfg p 0)

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


isCCSTr :: TTransition -> Bool
isCCSTr (Tr _ (CCSAct _) _ _) = True
isCCSTr _                     = False

makeCCSBranch :: Config -> (Act, Proc) -> TTransition
makeCCSBranch s@(Cfg _ n) (a, p) = Tr s (CCSAct a) (Cfg p n) Epsilon

makeKBranch :: Config -> Proc -> (Act, Proc) -> Maybe TTransition
makeKBranch s@(Cfg _ n) q (a, p) = 
  if a /= Tau
    then Just $ Tr s (NewAct (n+1) a) (Cfg (Tran n p q) (n+1)) Epsilon 
    else Nothing
    
ccs2Old :: Int -> Gamma -> Gamma
ccs2Old i (CCSAct a) = OldAct i a 
ccs2Old _ x          = x 

ccs2OldTr :: Int -> TTransition -> TTransition
ccs2OldTr i (Tr c g c' e) = Tr c (ccs2Old i g) c' e 

freshFromAct :: Action -> Act -> Bool
freshFromAct l act = case act of
  Act l' -> l /= l'
  Bar l' -> l /= l'
  Tau    -> True

freshFromGamma :: Action -> Gamma -> Bool
freshFromGamma l lab = case lab of
  CCSAct a   -> l `freshFromAct` a 
  NewAct _ a -> l `freshFromAct` a
  OldAct _ a -> l `freshFromAct` a
  Beta _      -> True


freshFromTr :: Action -> TTransition -> Bool
freshFromTr l (Tr _ g _ _) = freshFromGamma l g



subst :: (Var, Proc) -> Proc -> Proc
subst sub@(x, p') proc = case proc of
  Sum  ps -> Sum (map (second (subst sub)) ps)
  Par _ _     -> error "parallel cannot occur under recursive terms"
  Restr  _ _  -> error "restriction cannot occur under recursive terms"
  V y     -> if x == y 
              then p'
              else proc
  Rec y p -> if x == y 
              then proc -- variables clash
              else Rec y (subst sub p)
  Tran i p q -> Tran i (subst sub p) (subst sub q) 
  Commit p   -> Commit (subst sub p)
  Atomic p q -> Atomic (subst sub p) (subst sub q)
          
          
          
updateIndexes :: Proc -> Sigma -> Proc
updateIndexes proc Epsilon = proc
updateIndexes proc s@(Sub f)       = case proc of
  Par    p q    -> Par (updateIndexes p s) (updateIndexes q s)
  Restr  a p    -> Restr a (updateIndexes p s) 
  Tran   i p q  -> Tran (f i) p q 
  _             -> proc



updatePQ :: (Int -> Int) -> Proc -> Proc -> (Sigma, Proc, Proc)
updatePQ f p q = 
  let s  = Sub f
      p' = updateIndexes p s
      q' = updateIndexes q s
  in  (s, p', q')

sync :: TTransition -> TTransition -> Maybe TTransition
sync (Tr c act1 (Cfg p n) _) (Tr _ act2 (Cfg q n') _) =
  case act1 of 
    ----------------------------
    -- synchronize CCS action
    ----------------------------
    CCSAct a -> case act2 of 
      CCSAct a' -> if a `canSync` a'
                    then Just (Tr c (CCSAct Tau) (Cfg (Par p q) n) Epsilon) 
                    else Nothing
      _      -> Nothing
    
    ----------------------------
    -- synchronize NEW action
    ----------------------------
    NewAct i a -> case act2 of
      NewAct _ a' -> if a `canSync` a'
                      then Just (Tr c (NewAct i Tau) (Cfg (Par p q) n) Epsilon) 
                      else Nothing
      OldAct j a' -> if a `canSync` a'
                      then  
                          let -- index updating function
                              f idx | idx == i  = j
                                    | otherwise = idx
                              (s, p', q') = updatePQ f p q
                          in Just (Tr c (OldAct j Tau) (Cfg (Par p' q') n') s)
                      else Nothing
      _           -> Nothing
    
    ----------------------------
    -- synchronize OLD action
    ----------------------------
    OldAct i a -> case act2 of 
      NewAct j a' -> if a `canSync` a'
                      then  
                          let -- index updating function
                              f idx | idx == j  = i
                                    | otherwise = idx
                              (s, p', q') = updatePQ f p q
                          in Just (Tr c (OldAct i Tau) (Cfg (Par p' q') n) s)
                      else Nothing
      OldAct j a' -> if a `canSync` a'
                      then
                          if j == i
                            then Just (Tr c 
                                          (OldAct i Tau) 
                                          (Cfg (Par p q) n') 
                                          Epsilon)
                            else
                              let -- index updating function
                                  maxIdx = max i j
                                  minIdx = min i j
                                  f idx | idx == maxIdx  = minIdx
                                        | idx >  maxIdx   = idx - 1
                                        | otherwise = idx
                                  (s, p', q') = updatePQ f p q
                              in Just (Tr c 
                                          (OldAct minIdx Tau)
                                          (Cfg (Par p' q') (n -1)) 
                                          s)
                      else Nothing
      
      _ -> Nothing
    
    ----------------------------
    -- remaining actions
    ----------------------------
    _ -> Nothing

---- sync NEW actions
--sync (Tr c (NewAct n a) (Cfg p n) _) (Tr _ (NewAct _ a') (Cfg q _) _) =
--        if a `canSync` a'
--          then Just     (Tr c (NewAct n Tau) (Cfg (Par p q) n) Epsilon)
--          else Nothing
--          
----betas cannot be synced..
--sync (Tr _ (Beta _) _ _) _ = Nothing
--sync _ (Tr _ (Beta _) _ _) = Nothing



getComms :: Config -> [TTransition]
getComms cfg@(Cfg proc n) = case proc of 
  Sum ps  -> let  -- do a normal CCS action
                  ccss  = map (makeCCSBranch cfg) ps
                  ks    = mapMaybe (makeKBranch cfg (Sum ps)) ps
             in ccss ++ ks

  Par p q     -> let ts1 = getComms (Cfg p n)
                     ts2 = getComms (Cfg q n)
                     -- forward actions through the parallel construct
                     mkTrP (Tr _ g (Cfg p' n') sigma) = 
                            let q' = updateIndexes q sigma
                            in  Tr cfg g (Cfg (Par p' q') n') sigma
                     mkTrQ (Tr _ g (Cfg q' n') sigma) = 
                            let p' = updateIndexes p sigma
                            in  Tr cfg g (Cfg (Par p' q') n') sigma
                     ts1' = map mkTrP ts1
                     ts2' = map mkTrQ ts2
                     -- possible synchronizations
                     syncs = foldl (\acc x -> 
                              foldl (\acc2 y -> case sync x y of
                                Nothing -> acc2
                                Just z  -> z:acc2 ) 
                              acc
                              ts2) 
                             [] ts1
                 in ts1' ++ ts2' ++ syncs
                    -- deal with Act actions
--                    let tr1'' = map (\(Tr _ a (St p')) -> Tr s a (St (Par p' q))) tr1
--                    let tr2'' = map (\(Tr _ a (St q')) -> Tr s a (St (Par p q'))) tr2
--                    let zz   = map (\t@(Tr _ a1 (St p')) -> let syncable = filter (canSync t) tr2 in undefined ) tr1
--                    return undefined
  Restr a p   -> let ts         = getComms (Cfg p n)
                     allowedTs  = filter (a `freshFromTr`) ts
                     updateTs (Tr _ g (Cfg p' n') e) = Tr cfg g (Cfg (Restr a p') n') e
                 in map updateTs allowedTs
                  
  V _         -> []
  Rec x p     -> [Tr cfg (CCSAct Tau) (Cfg (subst (x,p) p) n) Epsilon]
  Tran i p q  -> let ts  = getComms (Cfg p n)
                     cs = filter isCCSTr ts
                     ts' = map (ccs2OldTr i) cs
                 in map (\(Tr _ g (Cfg p' n') s) -> Tr cfg g (Cfg (Tran i p' q) n') s) ts'
  Commit _    -> [] 
  Atomic _ _  -> []

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

getSucc :: TTransition -> Config
getSucc (Tr _ _ s _) = s


getTransitions :: Config -> [TTransition]
getTransitions c = 
  let comms   = getComms   c
      aborts  = getAborts  c
      commits = getCommits c
      news    = getNews    c
  in comms ++ aborts ++ commits ++ news

getStates :: Config -> ([Config], [TTransition]) -> ([Config], [TTransition])
getStates s (states, ts) = 
  let transitions = getTransitions s
      ts'         = nub (transitions ++ ts)
      successors  = nub (map getSucc ts') 
      states'     = nub (states ++ successors)
      diff        = (states' \\ states)
  in  if null diff
        then (states', ts')
        else foldr getStates (states', ts') diff

type TTransitionGraph = Gr Label TTransition
type TTran  = LEdge TTransition

createTTransition :: State -> State -> TTransition -> TTran 
createTTransition (n1, _) (n2, _) t = (n1, n2, t)



makeTLTS :: Proc -> IO TrLTS
makeTLTS p = do
  let s = initState p
  let (states, transitions) = getStates s ([s], [])
  let slist = nub states
  ss    <- mapM (createState . show) slist
  let smap  = M.fromList (zip (map show slist) ss)
  let lkup  = M.findWithDefault (error "error in lookup") 
  let ts    = map (\t@(Tr c _ c' _) -> createTTransition (lkup (show c) smap) (lkup (show c') smap) t) transitions
      ts'   = nub ts
      acts  = nub $ map (\(_,_,Tr _ a _ _) -> show a) ts'
  let lts = mkTLTS ss ts' acts      
  -- add the new states and the transitions to the LTS
  -- if no new states are added, terminate
  -- else repeat
  return lts


-- TODO:  this dummy definition is required by S.fromList in mkTLTS. 
--        Remove if possible
instance Ord Config where
  compare _ _ = EQ

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
type TrLTS = (  S.Set State,                 -- the set of states of the system 
                S.Set Action,                 -- the set of actions 
                TTransitionGraph)             -- the transition relation

------------------------------------------------------------------------------
-- Creation utilities
------------------------------------------------------------------------------


mkTLTS :: [State] -> [TTran] -> [Action] -> TrLTS
mkTLTS s t a = (S.fromList s, S.fromList a, mkGraph s t )







