module TCCS.Bisim where

import Data.Maybe
import Data.Unique
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Graph.Inductive.PatriciaTree
import Data.Graph.Inductive.Graph
import Control.Monad.State.Strict 

import LTS
import TCCS.Defs
import Data.Map (adjust)



type AreRelated  = Bool

data RevisitData = VisitP | VisitQ


type BNode = (Int, Int)


--setRelated :: BNode -> Bool -> BNode 
--setRelated (nid, bdata) b = (nid, bdata{related=b})
--
--mustRevisit :: BNode -> Bool
--mustRevisit (_, BData _ _ _ b _) = b
--
--setRevisit :: BNode -> Bool -> BNode 
--setRevisit (nid, bdata) b = (nid, bdata{related=b})

--type EData = Bool
--type BEdge = LEdge EData


--type TBisim = Gr BData BEdge
--
--mkBNode :: BData-> IO BNode
--mkBNode d = do
--  uniqueId <- newUnique                 -- generates unique ID
--  let edgeId = hashUnique uniqueId 
--  return ( edgeId, d )                  -- 'LNode a' is a tuple (Integer, a) 
--
--mkTransition :: BNode -> BNode -> EData -> BEdge
--mkTransition (n1, _) (n2, _) a = (n1, n2, a)


--insBNode :: TBisim -> BNode -> TBisim
--insBNode g b = insNode b g





--findTBisim :: LTS -> Bisimulation
--findTBisim ( ss, act, g ) = doSteps act g (singleton $ Data.Set.map fst ss)


data Side = L | R
type BisimGraph = Gr (Int, Int) (Gamma, Side)


--------------------------------------------------------------------------------
-- 
--------------------------------------------------------------------------------

type MyState = (TrLTS, 
                TrLTS, 
                S.Set (Int, Int), 
                S.Set BNode, 
                BisimGraph, 
                M.Map (Int, Gamma, Int) Int)


addBNode :: BNode -> StateT MyState IO (LNode BNode)
addBNode n = do
  (_,_,_,_,gr,_) <- get
  newId <- lift newUnique
  let bnode = (hashUnique newId, n)
  let gr'   = insNode bnode gr
  modify (\(a,b,c,d,_,e) -> (a,b,c,d,gr',e)) 
  return bnode


getSuccessors :: Int -> Side -> StateT MyState IO [(Int, TTransition)]
getSuccessors p side = do
  ((_,_,g1), (_,_,g2), _,_,_,_) <- get
  let g = case side of
                  L -> g1
                  R -> g2
  let successors = lsuc g p
  return successors

getActSuccessors :: Node -> Gamma -> Side -> StateT MyState IO [Node]
getActSuccessors p g side = do
  ((_,_,gr1), (_,_,gr2), _,_,_,_) <- get
  let gr = case side of
                  L -> gr1
                  R -> gr2
  let successors = lsuc gr p
  let succ' = filter (\(_, Tr _ g' _ _ ) -> g == g' ) successors
  return (map fst succ')

cleanup :: LNode BNode -> Side -> StateT MyState IO Bool
cleanup n side = undefined

opposite :: Side -> Side
opposite L = R
opposite R = L

updateIndexMap :: M.Map (Int, Gamma, Int) Int -> MyState -> MyState 
updateIndexMap m (a,b,c,d,e,_) = (a,b,c,d,e,m)


search :: (Int, TTransition) -> Int -> Side -> StateT MyState IO Bool
search pTr@(p', Tr p a _ _) q side = do
  (_, _, _, _, _, mm) <- get
  i <- case M.lookup (p',a , q) mm of 
              Nothing ->  do  
                        let mm' = M.insert (p', a, q) 1 M.empty
                        modify (updateIndexMap mm') 
                        return 1
              Just x  ->  return x
  qts <- getActSuccessors q a (opposite side)
  let qts' = drop i qts
  if i < length qts
    then do
      let q' = qts!!i
      b <- preorder (p',q')
      if b
        then do
          (_,_,_,_,g,_) <- get
          let g' = insEdge ((p',q'), (p,q), a) g 
          -- add edge to G
          undefined
        else do -- try another q'  
          let mm' = adjust (+1) (p',a,q) mm
          s <- get
          let s' = updateIndexMap mm' s
          put s'
          search pTr q side      -- retry
    else return False

-- TODO: test gelem node. Does it test equality on the node id, or on the data?
preorder :: (Int, Int) -> StateT MyState IO Bool 
--preorder _            _  (_,   BData _ _ False _ _) = False 
preorder (p, q) = do
  (_, _, rejs, _, gr,_) <- get
  
  -- have the nodes been rejected previously?
  if (p, q) `S.member` rejs
    then return False
    else 
      -- are the nodes already in G?
      if (p, q) `elem` map snd (labNodes gr)
        then return True
        else do
          -- assume p and q bisimilar
          n <- addBNode (p, q)
          
          ----------------------------
          -- check their bisimilarity
          ----------------------------
          
          -- get successors of p
          pts <- getSuccessors p L
          allLeftCovered <- foldM  (\acc pt -> if acc then search pt q L
                                                      else return acc) 
                                   True 
                                   pts 
          if allLeftCovered
            then do
              qts <- getSuccessors q R
              allRightCovered <- foldM  (\acc qt -> if acc then search qt p R
                                                           else return acc)
                                   True 
                                   qts
              if allRightCovered
                then return True
                else cleanup n R
            else cleanup n L
--  b3 <- fold (\ t1@(Tr cfg1 a1 cfg1' sigma2) acc -> do
--            b <- acc
--            if b
--              then do
--                createIdx cfg1' q a
--                let ts2 = filter (definedOn a1) $ transitions q
--                case searchMatching t1 ts2 Left of
--                  Nothing   -> revisit n Left >> return False
--                  Just t2   -> return True
--              else return b
--            )
--            graph
--            ts1
          
          
--  return undefined
--    gelem nid g ||
--  (let g' = insNode n g
--       n' = setRelated n True
--       sReach = out lts1 p
--     in undefined)
             
    


--preorder (p, q) = do

--  b1 <- isRejected (p, q)
--  Control.Monad.when b1 $ return False
--  b2 <- alreadyRelated (p, q)
--  Control.Monad.when b2 $ return True 
--  
--  -- add nodes in graph
--  n <- addBNode (p,q)
--  
--  -- match each derivative of p with some derivative of q
--  let ts1   = transitions p    
--  b3 <- fold (\ t1@(Tr cfg1 a1 cfg1' sigma2) acc -> do
--            b <- acc
--            if b
--              then do
--                createIdx cfg1' q a
--                let ts2 = filter (definedOn a1) $ transitions q
--                case searchMatching t1 ts2 Left of
--                  Nothing   -> revisit n Left >> return False
--                  Just t2   -> return True
--              else return b
--            )
--            graph
--            ts1
--  if b3 
--    then do
--      b4 <- fold undefined  -- do the same for q
--      if b4
--        then return True
--        else do
--          putInRevisit (neighbours n, Left)
--          removeBNode n
--          addRejected (p,q)
--    else do
--        putInRevisit (neighbours n, Right)
--        removeBNode n
--        addRejected (p,q)
--
--  -- revisit (p,q)
--  
--  
----  doRevisit :: IO ()
----  doRevisit = do
----    l <- toRevisitLength
----    when (l > 0) $ do
----      (r, a, r', s, side)<- getOneEdgeToRevisit
----      if side == Left
----        then do
----          increment r' s a
----          b <- searchLeft ((r, a, r',), s)
----          when (not b) $ do
----            putInRevisit (neighbours (r,s), Left)
----            removeBNode (r,s)
----            addRejected (r,s)
----        else do
----          increment r' s a
----          b <- searchRight ((r, a, r',), s)
----          when (not b) $ doW
----            putInRevisit (neighbours (r,s), Right)
----            removeBNode (r,s)
----            addRejected (r,s)
--
--
--searchLeft (p, a, p') q = do
--  i   <- getHighIndex (p', a', q)
--  qs' <- getQSuccessors q a
--  if i < length qs'
--    then do
--      b <- preorder(p', qs' !! i)
--      if b
--        then do
--          addEdge ( (p', qs' !! i), a, Right, (p, q) )
--          return True
--        else do
--          incrementHigh p' a q
--          searchLeft (p, a, p') q
--    else return False
-- same for searchRight





































