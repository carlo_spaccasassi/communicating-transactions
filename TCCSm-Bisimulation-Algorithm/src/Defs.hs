module Defs where

import Control.Monad
import Control.Arrow (second)
import Data.List (intercalate, nub, (\\))
--import Data.Maybe ( mapMaybe )
import qualified Data.Map  as M
import qualified Data.Set  as S

--import Data.Graph.Inductive.Graph

import LTS

--import Debug.Trace (trace)
--import Data.Graph.Inductive.PatriciaTree (Gr)
import qualified Control.Monad.State.Strict as St




  
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
data Proc  = Sum    [(Act, Proc)]
          | Par    Proc   Proc
          | Restr  Action Proc 
          | V      Var 
          | Rec    Var    Proc
          | Tran   Int    Proc Proc 
          | Commit Proc 
          | Atomic Proc   Proc
  deriving ( Eq )


nil :: Proc
nil = Sum []

prefix :: Act -> Proc -> Proc
prefix a p = Sum [(a, p)]

instance Show Proc where
  show proc = case proc of 
    Sum ps      ->  if Prelude.null ps
                      then  "nil"
                      else  let showBranch (a, p) = show a ++ "." ++ show p
                                branches          = map showBranch ps
                            in intercalate " + " branches 
    Par p q     -> show p ++ " || " ++ show q 
    Restr a p   -> "#" ++ a ++ "." ++ show p 
    V x         -> x
    Rec x p     -> "rec " ++ x ++ "." ++ show p
    Tran k p q  -> "[ " ++ show p ++ " <" ++ show k ++ "> " ++ show q ++ " ]"
    Commit p    -> "co." ++ show p
    Atomic p q  -> "[ " ++ show p ++ " <*> " ++ show q ++ " ]"

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--instance Ord Config where
--  compare (Cfg p n) (Cfg p' n')
--    | p == p' && n == n' = EQ
--    | n < n'             = LT
--    | otherwise          = GT




applySub :: Sigma -> Proc -> Proc
applySub s proc = case proc of
  Sum _       -> proc
  Par p q     -> Par (applySub s p) (applySub s q)
  Restr a p   -> Restr a (applySub s p) 
  V _         -> proc 
  Rec _ _     -> proc
  Tran k p q  -> Tran (case applySubDep s (Tr k) of
                        Tr z -> z
                        _    -> k
                       ) p q 
  Commit _    -> proc 
  Atomic _  _ -> proc

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

procSubst :: Var -> Proc -> Proc -> Proc
procSubst x proc pr = case pr of
  Sum ps -> Sum $ map (second f) ps
  V y -> if x == y 
          then proc
          else pr
  Par p q     -> Par (f p) (f q)
  Restr a p   -> Restr a (f p) 
  Rec y p     -> if x == y
                  then Rec y p
                  else Rec y (f p)
  Tran k p q  -> Tran k (f p) (f q) 
  Commit p    -> Commit (f p) 
  Atomic p  q -> Atomic (f p) (f q)
 where f = procSubst x proc 

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


freshFrom :: Tid -> Proc -> Bool
freshFrom k proc = case proc of 
  Par p q     -> freshFrom k p && freshFrom k q
  Restr _ p   -> freshFrom k p
  Tran l _ _  -> k /= l 
  _           -> True

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


canSync :: Act -> Act -> Bool
canSync (Act l) (Bar l') = l == l'
canSync (Bar l) (Act l') = l == l'
canSync _       _        = False

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
doAbort :: Proc -> Tid -> Proc
doAbort proc k = case proc of 
  Par p q     -> Par     (doAbort p k) (doAbort q k)
  Restr a p   -> Restr a (doAbort p k)
  Tran l _ q  -> if l == k
                    then q 
                    else proc
  _           -> proc

ftn :: Proc -> S.Set Tid
ftn p = case p of
  Sum _       -> S.empty
  Par p1 p2   -> ftn p1 `S.union` ftn p2
  Restr _ p1  -> ftn p1 
  V _         -> S.empty 
  Rec _ _     -> S.empty
  Tran k _ _  -> S.singleton k 
  Commit _    -> S.empty 
  Atomic _ _  -> S.empty


getAborts :: Proc -> [(Proc, TTransition, Proc)]
getAborts p =
  let n             = S.toList (ftn p)
      ps            = map (doAbort p) n
      mkTr i p'  = (p, TTr XTau (M.singleton i AbL), p')
  in zipWith mkTr n ps

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
getNewProcs :: Proc -> Tid -> [Proc]
getNewProcs proc n = case proc of
  Par p q     -> let ps = getNewProcs p n 
                     qs = getNewProcs q n
                     par'  = map (`Par` q) ps
                     par'' = map (Par p)   qs
                 in par' ++ par''
  Restr a p   -> map (Restr a) (getNewProcs p n)
  Atomic p q  -> [Tran n p q]
  _           -> []


nextTName :: [Tid] -> Tid
nextTName [] = 0
nextTName ns = minimum ([0..length ns] \\ ns)

getNews :: Proc -> Tid -> [(Proc, TTransition, Proc)]
getNews p newName =
  let ps      = getNewProcs p newName
      newTr   = TTr XTau M.empty
  in map (\p' -> (p, newTr, p')) ps
  
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
hasCommit :: Proc -> Bool
hasCommit proc = case proc of 
  Par p q   -> hasCommit p || hasCommit q
  Restr _ p -> hasCommit p
  Commit _  -> True
  _         -> False

canCommit :: Proc -> Tid -> Bool
canCommit proc k = case proc of 
  Par p q     -> canCommit p k && canCommit q k
  Restr _ p   -> canCommit p k
  Tran l p _  -> (k /= l) || hasCommit p
  _           -> True

removeCommit :: Proc -> Proc
removeCommit proc = case proc of
  Par p q     -> Par     (removeCommit p) (removeCommit q)
  Restr a p   -> Restr a (removeCommit p)
  Commit p    -> p
  _           -> proc 
  -- TODO: remove other commits too

doCommit :: Proc -> Tid -> Proc
doCommit proc k = case proc of 
  Par p q     -> let p' = doCommit p k
                     q' = doCommit q k
                 in Par p' q'
  Restr a p   -> Restr a (doCommit p k)
  Tran l p _  -> if k == l 
                  then removeCommit p
                  else proc
  _           -> proc
  
getCommits :: Proc -> [(Proc, TTransition, Proc)]
getCommits p = 
  let n             = S.toList (ftn p)
      committables  = filter (canCommit p) n 
      ps            = map (doCommit p) committables 
      mkTr i        = TTr XTau (M.singleton i CoL)
  in zipWith (\a b -> (p, mkTr a, b)) committables ps

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--init' :: Proc -> Proc
--init' s@(Cfg proc n) = case proc of 
--    Par p q     -> let (Cfg p' n')  = init' (Cfg p n)
--                       (Cfg q' n'') = init' (Cfg q n')
--                   in Cfg (Par p' q') n''  
--    Restr a p   -> let (Cfg p' n') = init' (Cfg p n)
--                   in Cfg (Restr a p') n' 
--    Tran _ p q  -> Cfg (Tran n p q) (n+1)
--    _           -> s
--
--initState :: Proc -> Config
--initState p = init' (Cfg p 0)

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


isCCSTr :: TTransition -> Bool
isCCSTr (TTr (CCSAct _) _) = True
isCCSTr _                  = False


--makeKBranch :: Proc -> Tid -> Proc -> (Act, Proc) -> Maybe TTransition
--makeKBranch p' n q (a, p) = 
--  if a /= Tau
--    then Just $ TTr (KAct (n+1) a) Epsilon 
--    else Nothing
    
freshFromAct :: Action -> Act -> Bool
freshFromAct l act = case act of
  Act l' -> l /= l'
  Bar l' -> l /= l'
  Tau    -> True

freshFromGamma :: Action -> Gamma -> Bool
freshFromGamma l g = case g of
  CCSAct a   -> l `freshFromAct` a 
  KAct _ a -> l `freshFromAct` a
--  Beta _      -> True


freshFromTr :: Action -> TTransition -> Bool
freshFromTr l (TTr g _) = freshFromGamma l g



          
sync :: (Proc, TTransition, Proc) -> (Proc, TTransition, Proc) -> Tid
                                              -> Maybe (Proc, TTransition, Proc)
sync (p0, TTr act1 s1, p1) (q0, TTr act2 s2, q1) k' =
  case act1 of 
    ----------------------------
    -- synchronize CCS action
    ----------------------------
    CCSAct a -> case act2 of 
      CCSAct a' -> if a `canSync` a'
                    then Just (Par p0 q0, TTr (CCSAct Tau) (s1 `compSub` s2), 
                               Par p1 q1) 
                    else Nothing
      _      -> Nothing
    
    ----------------------------
    -- synchronize NEW action
    ----------------------------
    KAct (Tr _) a -> case act2 of
      KAct (Tr _) a' -> if a `canSync` a'
                      then let s = (s1 `compSub` s2)
                           in Just  (Par p0 q0, 
                                     TTr (KAct (Tr k') Tau) s, 
                                     Par p1 q1) 
                      else Nothing
      _ -> Nothing
    
    ----------------------------
    -- remaining actions
    ----------------------------
    _ -> Nothing




getComms :: Proc -> Tid -> [(Proc, TTransition, Proc)]
getComms proc k' = case proc of 
  Sum ps  ->   map (\(a,p') -> (proc, TTr (CCSAct a) M.empty, p')) ps
    ++ map (\(a, p') -> (proc, TTr (KAct (Tr k') a) M.empty, Tran k' (Par p' (Commit nil)) (Sum ps))) ps

  Par p q -> let ts1 = getComms p k'
                 ts2 = getComms q k'
                 
                 -- forward actions through the parallel construct
                 mkTrP (p0, TTr g sigma, p1) = (Par p0 q, TTr g sigma, 
                                                  Par p1 (applySub sigma q))
                 mkTrQ (q0, TTr g sigma, q1) = (Par p q0, TTr g sigma, 
                                                  Par (applySub sigma p) q1)
                 
                 ts1' = map mkTrP ts1
                 ts2' = map mkTrQ ts2
                 
                 -- possible synchronizations
                 syncs = foldl (\acc x -> 
                          foldl (\acc2 y -> case sync x y k' of
                            Nothing -> acc2
                            Just z  -> z:acc2 ) 
                          acc
                          ts2)
                         [] ts1
             in ts1' ++ ts2' ++ syncs
  
  Restr a p   -> let ts         = getComms p k'
                     allowedTs  = filter (\(_,x,_) -> a `freshFromTr` x) ts
                     updateTs (p0, tr, p1) = (Restr a p0, tr, Restr a p1)
                 in map updateTs allowedTs
                  
  V _         -> []
  Rec x p     -> [(Rec x p, TTr (CCSAct Tau) M.empty, procSubst x (Rec x p) p)] 
    --TODO
  Tran i p q  -> let ts  = getComms p k'
                     ccsTs = filter (\(_,tt,_) -> case tt of 
                                        TTr (CCSAct _) _ -> True
                                        _                -> False) ts
                 in map (\(p0, TTr (CCSAct a) _, p1) -> 
                          (Tran i p0 q, 
                           TTr (KAct (Tr k') a) (M.singleton i (Tr k')), 
                           Tran k' p1 q)
                        ) ccsTs
  Commit _    -> [] 
  Atomic _ _  -> []

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
getTransitions :: Proc -> [(Proc, TTransition, Proc)]
getTransitions c = 
  let comms   = getComms   c (nextTName $ S.toList $ ftn c)
      aborts  = getAborts  c
      commits = getCommits c
      news    = getNews    c (nextTName $ S.toList $ ftn c)
  in comms ++ aborts ++ commits ++ news

getStates' :: Proc -> St.State [Proc] [(Proc, TTransition, Proc)]
getStates' curr = do 
        visited <- St.get
        
        if curr `elem` visited 
          then return []
          else  do
               let visited' = nub $ curr:visited 
--               trace (show (length visited)) (return ())
               St.put visited'
               let ts'' = getTransitions curr
                   s'  = map (\(_,_,x) -> x) ts''
                   s'' = filter (`notElem` visited') s'
               ts''' <- liftM concat (mapM getStates'  s'')
               return (ts'' ++ ts''')

getStates :: Proc -> [(Proc, TTransition, Proc)]
getStates s = St.evalState (getStates' s) []
--      if null diff
--        then undefined
--        else foldr (\(_,_,p') acc -> getStates p' acc) ts' diff 




makeTLTS :: Proc -> S.Set Action -> IO TrLTS
makeTLTS s acts = do
  let transitions = getStates s 
      states      = nub (concatMap (\(x,_,y)-> [x,y]) transitions)
  ss    <- mapM (createState . show) states
--  trace (show ss) (return ())

  let smap  = M.fromList (zip (map show states) ss)
      lkup  x y = M.findWithDefault (error $ "error in lookup: " ++ show x ++ " " ++ show y ) x y  
      ts    = map (\(p, t, p') -> createTTransition (lkup (show p) smap) (lkup (show p') smap) t) transitions
      ts'   = ts
--      acts  = nub $ concatMap (\(_,_,TTr a _) -> case a of
--                                                  KAct _ (Act zz) -> [zz] 
--                                                  KAct _ (Bar zz) -> [zz]
--                                                  CCSAct (Act zz) -> [zz]
--                                                  CCSAct (Bar zz) -> [zz]
--                                                  XTau -> []
--                                                  KAct _ Tau -> []
--                                                  CCSAct Tau -> []
--                                                  ) ts'
  
  let lts@(_,_,_) = mkTLTS ss ts' (S.toList acts)      
  -- add the new states and the transitions to the LTS
  -- if no new states are added, terminate
  -- else repeat
  return lts

