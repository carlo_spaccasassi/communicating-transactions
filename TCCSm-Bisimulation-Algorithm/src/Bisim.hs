module Bisim where

import Data.Unique
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Graph.Inductive.Graph
import Control.Monad.State.Strict 

import Data.Map (adjust)

--import Debug.Trace (trace)
import LTS
import Data.List (nub)

type AreRelated  = Bool
data RevisitData = VisitP | VisitQ

type Rejects = S.Set (ProcID, ProcID, Delta)
type IndexMap = M.Map (ProcID, Gamma, Sigma, ProcID, ProcID, Side) Int

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

type MyState = (TrLTS,        -- P's LTS
                TrLTS,        -- Q's LTS
                Rejects,      -- rejected pairs
                BisimGraph,   -- computed bisimulation
                IndexMap)     -- high_<p, q, a>


getBisimGraph :: StateT MyState IO BisimGraph 
getBisimGraph = do
  (_,_,_,g,_) <- get
  return g 


putBisimGraph :: BisimGraph -> StateT MyState IO ()
putBisimGraph g = do
  (a,b,c,_,e) <- get
  put (a,b,c,g,e)


updateIndexMap :: IndexMap -> MyState -> MyState 
updateIndexMap m (a,b,c,d,_) = (a,b,c,d,m)


getIndexMap :: StateT MyState IO IndexMap
getIndexMap = do
  (_,_,_,_, m) <- get
  return m 


getRejects :: StateT MyState IO (S.Set (ProcID, ProcID, Delta))
getRejects = do
  (_,_,rejs,_,_) <- get
  return rejs 


putRejects :: Rejects -> StateT MyState IO ()
putRejects rs = do
  (a,b,_,c,d) <- get
  put (a,b,rs,c,d)
  
putIndexMap :: IndexMap -> StateT MyState IO ()
putIndexMap m = do
  (a,b,c,d,_) <- get
  put (a,b,c,d,m)


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

addBNode :: BNode -> StateT MyState IO (LNode BNode)
addBNode n = do
  (_,_,_,gr,_) <- get
  newId <- lift newUnique
  let bnode = (hashUnique newId, n)
  let gr'   = insNode bnode gr
  modify (\(a,b,c,_,e) -> (a,b,c,gr',e)) 
  return bnode

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

getSuccessors :: Int -> Side -> StateT MyState IO [(Int, TTransition)]
getSuccessors p side = do
  ((_,_,g1), (_,_,g2), _,_,_) <- get
  let g = case side of
                  L -> g1
                  R -> g2
  let successors = lsuc g p
  return successors


kmatch :: Gamma -> Gamma -> Bool
kmatch (CCSAct a) (CCSAct b) = a == b
kmatch (KAct _ fg) (KAct _ zz) = fg == zz
kmatch (KAct _ Tau) XTau  = True
kmatch XTau  (KAct _ Tau) = True
kmatch XTau XTau  = True
kmatch _ _ = False


getActSuccessors :: Node -> Gamma -> Side -> StateT MyState IO [(ProcID, TTransition)]
getActSuccessors p gamma side = do
  
--  trace ("getActSucc: " ++ show p ++ " --" ++ show gamma ++ "-> "  
--        ++ " from " ++ show side ) (return ())
        
  ((_,_,gr1), (_,_,gr2), _,_,_) <- get
  let gr = case side of
                  L -> gr1
                  R -> gr2
  -- TODO: does lsuc always return lists of nodes in the same order?
  let successors = lsuc gr p
--  trace ("succ: " ++ show successors) (return ())
  -- TODO: filter should test k(a) == l(a) and a == a
  return (filter (\(_, t) -> kmatch gamma (case t of TTr g' _ -> g'; WTr g' _ -> g')) successors)

isTTr :: TTransition -> Bool
isTTr TTr{} = True
isTTr WTr{} = False
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- TTr Config Gamma Config Sigma
cleanup :: LNode BNode -> Int -> StateT MyState IO Bool
cleanup (n, (p,q,d)) num = do
  g <- getBisimGraph

--  trace (show num ++ ") cleanup: " ++ show (p, q,d) ) (return ())
  if not (gelem n g)
    then --trace ("cleanup fail - still present") (
        return False
        --)
    else do
  let predecessors = lpre g n         -- get all bisimulation edges to (p,q,d)
--  trace (show num ++ ") to revisit: " ++ concatMap 
--    (\(predID, (act,side,sigma)) -> case side of 
--                            L -> case lab g predID of
--                                    Just (p', _, _) -> 
--                                      "Side: " ++ show side ++ "\n" ++ 
--                                      show p' ++ " -- " ++ show act ++ " "++ show sigma ++ " --> " ++ show p
--                                    Nothing -> error "Nothing found L"  
--                            R -> case lab g predID of
--                                    Just (_, q', _) -> 
--                                      "Side: " ++ show side ++ "\n" ++
--                                      show q' ++ " -- " ++ show act ++ " "++ show sigma ++" --> " ++ show q
--                                    Nothing -> error "Nothing found L" 
--                            ) 
--    predecessors) (return ())
--  trace (show num ++ ") Deleting node " ++ show (n, lab g n)) (return ())
  
  let g' = delNode n g
  
--  trace (show (map (lab g') $ nodes g')) (return ())
  putBisimGraph g'
  rs <- getRejects
  
--  trace ("rejecting " ++ show(p,q,d) ++ " during cleanup") (return ())
  putRejects ((p, q, d) `S.insert` rs)
  
  b <- mapM (\(predID, (gamma, side', sigma)) -> do
    g'' <- getBisimGraph 
    case lab g'' predID of
      Nothing -> --trace (show num ++ ") cleanup done - " ++ show predID) 
                (return False)
      Just (p0, q0, d0) -> do
        let (p0', q0') = case side' of
                    L -> (p0, q0)
                    R -> (q0, p0)
        
--        trace  ("g'' \n" ++  unlines (map show $ map (\x -> (x, case lab g'' x of
--          Nothing -> undefined
--          Just z -> z
--          )) $ nodes g'')) (return ())
--        trace  ("g'' \n" ++ unlines (map show (labEdges g''))) (return ())
--        trace ("predID = " ++ show predID) (return ())
        
        -- update high/low
        case side' of
          --
          L -> do
            mm' <- getIndexMap
            let mm'' = adjust (+1) (p,gamma,sigma,p0', q0,side') mm'
            putIndexMap mm''
          --
          R -> do
            mm' <- getIndexMap
            let mm'' = adjust (+1) (q,gamma,sigma,q0', p0,side') mm'
            putIndexMap mm''
          --
        
        b <- search (p0, p0', TTr gamma sigma) q0' d0 predID side' num
        if b
          then do
--            trace (show num ++ ") Cleanup done on " ++ show predID) (return ())
--            g''' <- getBisimGraph
--            trace  ("g''' \n" ++  unlines (map show $ map (\x -> (x, case lab g''' x of
--              Nothing -> undefined
--              Just z -> z
--              )) $ nodes g''')) (return ())
--            trace  ("g''' \n" ++ unlines (map show (labEdges g'''))) (return ())
--            trace (show num ++ ") Cleanup done.") (return ())
            
            return True
          else --trace (show num ++ ") more cleanup to do" ++ show predID) 
                     (cleanup (predID, (p0, q0, d0))) num 
       ) predecessors
  
--  g'' <- getBisimGraph
--  trace ("\nfinal bisim:" ++ show (map (\zz -> case zz of
--      Nothing -> error "169"
--      Just (x,y,z) -> (x, y, z) 
--    )$map (lab g'')$ nodes g''))
--        (return ())
  return (if null b
           then False
           else and b)

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- pTr is the transition to be matched
-- q is the ProcID
-- d is delta
-- n is the ID of the starting bisimulation node 
-- side is the side
search :: (ProcID, ProcID, TTransition) -> ProcID -> Delta -> ProcID -> Side 
 -> Int -> StateT MyState IO Bool
search pTr@(p, p', ttt) q d n side nn = do
  let (a, s1) = case  ttt of 
          TTr labelA sigma -> (labelA, sigma)
          WTr labelA sigma -> (labelA, sigma)
  mm <- getIndexMap
--  trace (show nn ++ ") search: " ++ show p ++ " --" ++ show a ++ "-> " ++ show p' 
--          ++ " from " ++ show (q, d, side) ) (return ())
  
  highOrLowIndex <- case M.lookup (p, a, s1, p', q, side) mm of 
              Nothing -> do  
                        let newIndex = 0
                            mm' = M.insert (p, a, s1, p', q, side) newIndex M.empty
                        putIndexMap mm' 
--                        trace "index added" (return ())
                        return newIndex
              Just x  ->  return x
  qts <- getActSuccessors q a (opposite side)
--  trace (show highOrLowIndex ++ "!!qts: " ++ show qts) (return ())
  if highOrLowIndex < length qts
    then do
      let (q', ttt2) = qts!!highOrLowIndex
      let (a', s2) = case  ttt2 of 
                          TTr ax' s2x -> (ax', s2x)
                          WTr ax' s2x -> (ax', s2x)
--      trace (show nn ++ ") match:  " ++ show q ++ " --" ++ show ttt2 ++ "-> " ++ show q' ) (return ())
      let d0 =  case side of
                  L   -> applySubDeltaL s1 (applySubDeltaR s2 d)
                  R   -> applySubDeltaR s1 (applySubDeltaL s2 d)
      let d' = case a of
                  KAct _ Tau -> nub d0
                  KAct k _ -> case a' of
                    KAct _ Tau -> nub d0
                    KAct l _ -> case side of
                      L   -> nub $ (k, l):d0
                      R   -> nub $ (l, k):d0
                    _        -> error "unexpected response"
                  _ -> nub d0
      
      b <- case side of 
            L -> preorder (p', q',d') (nn+1)
            R -> preorder (q', p',d') (nn+1)
      if b
        then do
          g <- getBisimGraph
          
          -- add edge to G
--          trace (show nn ++ ") Preorder done.") (return ())
          -- search (p', q', d') by label
          let z = filter (\x -> case lab g x of
                                        Just y  -> case side of 
                                                    L -> --trace (show y ++ " vs " ++ show (p', q', d')) 
                                                          (y == (p', q', d'))
                                                    R -> --trace (show y ++ " vs " ++ show (p', q', d')) 
                                                      (y == (q', p', d'))
                                        Nothing -> error "Bisim:271") 
                         (nodes g)
          let n' = head 
                    --trace ("woooooooof " ++ show z ++ " " ++ show b ++ "\n"
                    -- ++ show (map (lab g) (nodes g))
                    -- ++ "\n" ++ show (lab l1 p',lab l2 q',d')) 
                    z
          
--          trace (show nn ++ ") Inserting edge: " 
--            ++ show (n, n', (a, side, s1))
--            ) (return ())
          let g'' = insEdge (n, n', (a, side, s1)) g 
          
          -- update g
          putBisimGraph g''
          
          --trace (show nn ++ ") Search ok.") (return ())
          return True
        
        else do -- try another q'
          mm' <- getIndexMap
          let mm'' = adjust (+1) (p, a, s1, p',q,side) mm'
          putIndexMap mm''
          --trace (show nn ++ ") Search failed." ) (return ())
--          trace (show nn ++ ") Search retry: " ++ show 
--            pTr ++ " " ++ show q ++ " " ++ show d ++ " " ++ show n ++ " " ++ show side
--            ) (return ())
          search pTr q d n side nn   -- retry
    else  --trace (show nn ++ ") search failed") 
      (return False)

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

preorder :: BNode -> Int -> StateT MyState IO Bool 
preorder (p, q, d) num = do
  
  rejs <- getRejects
  gr   <- getBisimGraph

--  trace (""++show num ++ ") preorder: " ++ show (p,q,d) 
--  ++ "\n" ++         show num 
--         ++ ") current bisim:\n" ++  (unlines $ map (
--         \x -> case x of Just f -> show f; Nothing -> ""
--         )$ map (lab gr)$ nodes gr)
--         ) (return ())
  
  -- has the node been rejected previously?
  if (p, q, d) `S.member` rejs
    then   --trace (show num ++ ") already rejected") 
      (return False)
    else 
      -- are the nodes already in G?
      if (p, q, d) `elem` map snd (labNodes gr)
        then --trace (show num ++ ") already bisim") 
          (return True)
        else
          -- is \Delta consistent?
          if not $ all (uncurry consistent) d
            then do
              --trace (show num ++ ") rejected because inconsistent") (return ())
              rs <- getRejects
              putRejects ((p,q,d) `S.insert` rs)
              return False
            
            else do
              -- assume p and q bisimilar
              (n, nn) <- 
                --trace "adding it" 
                (addBNode (p, q, d))
              
              ----------------------------
              -- check their bisimilarity
              ----------------------------
              -- get successors of p
              pts <- getSuccessors p L
              let pts' = map (\(x, y) -> (p, x, y)) $ filter (isTTr . snd) pts
--              trace ("L-successors to " ++ show p ++ ": " ++ show pts') (return ())
              
              allLeftCovered <- foldM  (\acc pt -> if acc then search pt q d n L num
                                                          else return acc) 
                                       True
                                       pts' 
              if allLeftCovered
                then do
                  qts <- getSuccessors q R
                  let qts' = map (\(x, y) -> (q, x, y)) $ filter (isTTr . snd) qts
--                  trace ("R-successors to " ++ show q ++ ": " ++ show qts) (return ())
                  allRightCovered <- foldM (\acc qt -> if acc then search qt p d n R num
                                                              else return acc)
                                       True
                                       qts'
                  if allRightCovered
                    then return True
                    else do 
                          bb <- cleanup (n, nn) num
                          return False
                else do
                  bb <- cleanup (n, nn) num
                  return False
            

runBisim :: TrLTS -> TrLTS -> ProcID -> ProcID -> IO (Bool, MyState)
runBisim lts1 lts2 p q = runStateT (preorder (p, q, []) 0) state0 
  where state0 = (lts1, lts2, S.empty, empty, M.empty)
