module LTS where

import qualified Control.Arrow (first, second)
import Data.Graph.Inductive.PatriciaTree
import Data.Graph.Inductive.Graph
import Data.Graph.Inductive.Graphviz
import Data.List (nub)
import qualified Data.Map  as M
import qualified Data.Set  as S
import Data.Unique
import Prelude hiding ( id )
import Debug.Trace (trace)
import Data.Maybe (fromMaybe)

------------------------------------------------------------------------------
-- LTS and Bisimulation definitions
------------------------------------------------------------------------------
type Label           = String             -- decorates a state
type Action          = String             -- identifies a state transition
type State           = LNode Label        -- A labeled state, together with
                                          -- an internal unique ID (Integer)
type TransitionGraph = Gr Label Action   -- a graph whose nodes hold a String
                                          -- and whose edges hold an Action
type Var   = String
type Tid    = Int
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

data Act   =  Act Action 
            | Bar Action
            | Tau
  deriving ( Eq )

instance Show Act where
  show (Act l) = l
  show (Bar l) = '~' : l
  show Tau     = "tau"

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

data Beta  = Co Tid
           | Ab Tid           
           | New Tid
  deriving ( Eq )

instance Show Beta where
  show (Co  i) = "co k"  ++ show i
  show (Ab  i) = "ab k"  ++ show i
  show (New i) = "new k" ++ show i
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

data Gamma = CCSAct  Act 
          |  KAct    Dep Act
          |  XTau
  deriving ( Eq )

instance Show Gamma where
  show (CCSAct a) = show a
  show (KAct k a) = show k ++ "(" ++ show a ++ ")"
  show (XTau) = "tau"

instance Ord Gamma where                        -- TODO: find a better ordering
  compare g1 g2 = compare (show g1) (show g2)  

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

type  Sigma = M.Map Tid Dep

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  
data TTransition = TTr Gamma Sigma
                 | WTr Gamma Sigma
  deriving ( Eq, Show )

type TTransitionGraph = Gr Label TTransition
type TTran  = LEdge TTransition

createTTransition :: State -> State -> TTransition -> TTran 
createTTransition (n1, _) (n2, _) t = (n1, n2, t)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
type TrLTS = (  S.Set State,                 -- the set of states of the system 
                S.Set Action,                -- the set of actions 
                TTransitionGraph)            -- the transition relation


data Dep = Tr Tid | CoL | AbL  -- d ::= k | co | ab
  deriving (Eq, Ord)

instance (Show Dep) where
  show (Tr k) = show k
  show CoL    = "co"
  show AbL    = "ab"

applySubDep :: Sigma -> Dep -> Dep
--applySubDep Epsilon d = d
applySubDep _ AbL = AbL
applySubDep _ CoL = CoL
applySubDep s (Tr k) = fromMaybe (Tr k) (M.lookup k s)


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

type Delta = [(Dep, Dep)]

applySubDeltaL :: Sigma -> Delta -> Delta
applySubDeltaL s = map (Control.Arrow.first (applySubDep s))
                                            
applySubDeltaR :: Sigma -> Delta -> Delta
applySubDeltaR s = map (Control.Arrow.second (applySubDep s))

consistent :: Dep -> Dep -> Bool
consistent CoL CoL = True
consistent CoL _   = False
consistent _   CoL = False
consistent _   _   = True

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

data Side = L | R
  deriving (Eq, Ord, Show)

opposite :: Side -> Side
opposite L = R
opposite R = L

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

type ProcID = Int
type BNode = (ProcID, ProcID, Delta)  -- P x Q x \Delta
type BisimGraph = Gr BNode (Gamma, Side, Sigma)

------------------------------------------------------------------------------
-- Creation utilities
------------------------------------------------------------------------------

compAct :: Gamma -> Gamma -> Sigma -> Sigma -> Gamma
compAct (KAct _ Tau) (KAct _ Tau) _ _ = XTau
compAct (KAct _ Tau) b _ _            = b 
compAct (KAct d1 a1) _ _ s2 = KAct (applySubDep s2 d1) a1
compAct (CCSAct Tau) b _ _  = b
compAct a (CCSAct Tau) _ _  = a
compAct (CCSAct a2) _ _ _   = CCSAct a2
compAct XTau (KAct d2 a2) _ _ = KAct d2 a2 
compAct XTau XTau _ _ = XTau
compAct XTau (CCSAct (Act a)) _ _ = CCSAct (Act a) 
compAct XTau (CCSAct (Bar a)) _ _ = CCSAct (Bar a)

compSub :: Sigma -> Sigma -> Sigma
compSub s1 s2 = upS1 `M.union` s2
  where upS1 = M.map (applySubDep s2) s1

comp :: TTransition -> TTransition -> TTransition
comp t1 t2 = 
   let (g1, s1) = case t1 of
                      TTr g s -> (g, s)  
                      WTr g s -> (g, s)
    in let (g2, s2) = case t2 of
                              TTr g s -> (g, s)  
                              WTr g s -> (g, s)
    in WTr (compAct g1 g2 s1 s2) (compSub s1 s2)
  
mkG0 :: [State] -> [TTran] -> TTransitionGraph
mkG0 = mkGraph

isTau :: TTransition -> Bool
isTau (TTr (CCSAct Tau) _) = True
isTau (TTr (KAct _ Tau) _) = True
isTau (TTr XTau _)         = True
isTau (WTr (CCSAct Tau) _) = True
isTau (WTr (KAct _ Tau) _) = True
isTau (WTr XTau _)         = True
isTau _                    = False

canComp :: TTransition -> TTransition -> Bool
canComp t1 t2  = isTau t1 || isTau t2


weakClosure :: [TTran] -> [TTran]
weakClosure es = let es' = nub (es ++ concatMap (`f` es) es)
                 in if 
--                      trace 
--                        (show (length es) ++ " =?= " ++ show (length es') 
--                              ++ ": " ++ show (length es == length es')) 
                        (length es == length es')
                      then es
                      else weakClosure es'
        where f (n11,n12,l1) es0 =  map (\(_,n22,l2) -> (n11,n22, l1 `comp` l2)) 
                                 $ filter (\(n21,_,l2) -> n12 == n21 
                                              && --trace (show l1 ++ " vs " ++ show l2) 
                                                (canComp l1 l2)) --TODO: loop?
                                       es0 


mkTLTS :: [State] -> [TTran] -> [Action] -> TrLTS
mkTLTS s t a = (S.fromList s, S.fromList a, mkGraph s t'')
  where 
        wTaus  = weakClosure (filter (\(_, _, trType) -> case trType of
          TTr (KAct _ Tau) _ -> True
          TTr XTau  _        -> True
          TTr (CCSAct Tau) _ -> True
          _                  -> False
          ) t)
        t'' = nub $ t 
              ++ wTaus 
              -- add tau self loops
              ++ concatMap (\(x,_) -> [(x,x, WTr XTau M.empty)] 
                ++ map (\ac -> (x, x, WTr (KAct AbL (Act ac)) M.empty)) a
                ++ map (\ac -> (x, x, WTr (KAct AbL (Bar ac)) M.empty)) a
              ) s
              -- add weak a actions
              ++ concatMap 
                 (\(x1,x2,x3) ->
                  concatMap (\(z1,z2,z3) ->
                    concatMap (\(y1,y2,y3) -> 
                      [(x1, z2, (x3 `comp` y3) `comp` z3) 
                      | x2 == y1 && y2 == z1 && (x1 /= x2 && z1 /= z2)]
                    )
                    (filter (\(_,_,x) -> not (isTau x)) t)  --strong acts
                  )
                  wTaus
                 )
                 wTaus
              ++ concatMap 
                 (\(x1,x2,x3) ->
                    concatMap (\(y1,y2,y3) -> 
                      [(x1, y2, x3 `comp` y3) 
                      | x2 == y1 && (x1 /= x2)]
                    )
                    (filter (\(_,_,x) -> not (isTau x)) t)  --strong acts
                 )
                 wTaus
              ++ concatMap 
                 ( (\(z1,z2,z3) ->
                    concatMap (\(y1,y2,y3) -> 
                      [(y1, z2, y3`comp` z3) | y2 == z1 && z1 /= z2]
                    )
                    (filter (\(_,_,x) -> not (isTau x)) t)  --strong acts
                  )
                 )
                 wTaus
--              -- add ab actions
              ++ concatMap (\(x,y,tr) -> 
                let sigma = case tr of
                                  TTr _ sigma' -> sigma'
                                  WTr _ sigma' -> sigma'
                in map (\ac -> (x,y, WTr (KAct AbL (Bar ac)) sigma) ) a) wTaus
                
              ++ concatMap (\(x,y,tr) -> 
                let sigma = case tr of
                                  TTr _ sigma' -> sigma'
                                  WTr _ sigma' -> sigma'
                in map (\ac -> (x,y, WTr (KAct AbL (Act ac)) sigma) ) a) wTaus
  
------------------------------------------------------------------------------
-- Creation utilities
------------------------------------------------------------------------------

createState :: String -> IO State
createState s = do
  uniqueId <- newUnique             -- generates unique ID
  let id = hashUnique uniqueId 
  return ( id, s )                  -- an LNode is just a tuple :: (Integer, a) 



------------------------------------------------------------------------------
-- Printing routines to GraphViz for LTS and bisimulation.
-- Modified from the source code of Data.Graph.Inductive.Graphviz 
------------------------------------------------------------------------------
sq :: String -> String
sq s@[_]                     = s
sq ('"':s)  | last s == '"'  = init s
      | otherwise      = s
sq ('\'':s) | last s == '\'' = init s
      | otherwise      = s
sq s                         = s


sl :: (Show a) => a -> String
sl a =
    let l = sq (show a)
    in if l /= "()" then " [label = \"" ++ l ++ "\"]" else ""

i2d :: Int -> Double
i2d = fromInteger . toInteger


mygraphviz :: TransitionGraph
                             -> String
                             -> (Double, Double)
                             -> (Int, Int)
                             -> Orient
                             -> BisimGraph
                             -> IO ()
mygraphviz g t (w, h) p@(pw', ph') o bisimulation =
    let n = labNodes g
        e = labEdges g
        ns = concatMap sn n
        es = concatMap se e
        bs = concatMap sb (map snd $ labNodes bisimulation)
        sz w' h' = if o == Portrait 
                      then show w' ++ "," ++ show h' 
                      else show h' ++ "," ++ show w'
        ps = show w++","++show h
        (pw, ph) = if o == Portrait 
                    then p 
                    else (ph', pw')
        --gs = show ((w*(i2d pw))-m)++","++show ((h*(i2d ph))-m)
        gs = sz (w * i2d pw) (h * i2d ph)
          in putStrLn $ "digraph "++t++" {\n"
            ++"\tmargin = \"0\"\n"
            ++"\tpage = \""++ps++"\"\n"
            ++"\tsize = \""++gs++"\"\n"
            ++"\tratio = \"fill\"\n"
            ++ns
            ++es
            ++"\n\tsubgraph Rel1{\n\t\tedge[dir=none,color=red,style=dotted,constraint=false]\n"
            ++bs
            ++"\t}\n}"
          where 
                se (n1, n2, b) = '\t':(show n1 ++ " -> " ++ show n2 
                                                          ++ sl b ++ "\n")
                sn (n, a) | sl a == ""  = ""
                          | otherwise = '\t':(show n ++ sl a ++ "\n")
                sb (p1, p2, d) = "\t\t" ++ (show p1 ++ " -> " ++ show p2
                                                              ++ sl d ++ "\n" )


printTrBisimulation :: TrLTS -> TrLTS -> BisimGraph -> IO ()
printTrBisimulation (_, _, g1) (_, _, g2) bisim = do
   let g'' = insEdges (labEdges g2) (insNodes (labNodes g2) g1)
   let g' = --g''
           foldr delLEdge g'' 
                     (filter (\(_,_,t) -> case t of 
                                        TTr _ _ -> False 
                                        WTr _ _ -> True) 
                             (labEdges g'')) 
   let g  = emap (\tt -> case tt of 
                          TTr a s -> show a ++ " " ++ (concatMap (\(x,y) -> show x ++ " -> " ++ show y) (M.toList s)); 
                          WTr a s   -> show a ++ " " ++ concatMap (\ (x, y) -> show x ++ " -> " ++ show y) (M.toList s)) g'
   mygraphviz g "fgl" (18, 8) (1,1) Portrait bisim
