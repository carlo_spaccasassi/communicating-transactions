module Main where

import qualified Data.Set as S
import Data.Graph.Inductive.Graph
import Bisim
import Defs
import LTS
import GHC.IO.BufferedIO (flushWriteBuffer)

p1 :: Proc
p1 =  Tran 0 (Sum [(Act "a", prefix (Act "b") $ Commit nil),
                   (Act "b", prefix (Act "a") $ Commit nil)]) nil
        
p2 :: Proc
p2 =  Restr "p"
        (Par
           (Tran 0 (prefix (Act "a") $ prefix (Act "p") $ Commit nil) nil)
           (Tran 1 (prefix (Act "b") $ prefix (Bar "p") $ Commit nil) nil))

p3 :: Proc
p3 =  Par
       (Tran 0 (prefix (Act "a") $ Commit nil) nil)
       (Tran 0 (prefix (Act "b") $ Commit nil) nil)


p4 :: Proc
p4 = Par
       ( Tran 0 (prefix (Act "a") (Commit nil)) nil )
       ( Tran 1 (prefix (Act "b") (Commit nil)) nil )


p5 :: Proc
p5 = Restr "p" $ 
     Par (prefix (Act "p") nil) $
     Par
       ( Tran 0 (prefix (Act "a") $ prefix (Bar "p") $ Commit (prefix (Act "p") nil)) nil)
       ( Tran 1 (prefix (Act "b") $ prefix (Bar "p") $ Commit (prefix (Act "p") nil)) nil)

p6 :: Proc
p6 = Restr "a" 
      $ Par
       ( Rec "x" $ Atomic (prefix (Act "a") $ prefix (Act "b") $ Commit nil) (V "x"))
      $ Par  
       ( Rec "x" $ Atomic (prefix (Bar "a") $ Commit nil) (V "x"))
      $ Par  
       ( Rec "x" $ Atomic (prefix (Bar "a") $ prefix (Act "c") $ Commit nil) (V "x"))
       ( Rec "x" $ Atomic (prefix (Bar "b") $ prefix (Bar "c") $ Commit nil) (V "x"))

p6' :: Proc
p6' = Restr "a"  
      $ Par
       ( Tran 0 (prefix (Act "a") $ prefix (Act "b") $ Commit nil) (nil))
      $ Par  
       ( Tran 1 (prefix (Bar "a") $ Commit nil) (nil))
      $ Par  
       ( Tran 2 (prefix (Bar "a") $ prefix (Act "c") $ Commit nil) (nil))
       ( Tran 3 (prefix (Bar "b") $ prefix (Bar "c") $ Commit nil) (nil))
              
       
p7 :: Proc
p7 = Restr "a" 
      $ Par 
       ( Rec "x" $ Atomic (prefix (Act "a") $ prefix (Act "b") $ Commit nil) (V "x"))
      $ Par  
       ( Rec "x" $ Atomic (prefix (Bar "a") $ prefix (Act "c") $ Commit nil) (V "x"))
       ( Rec "x" $ Atomic (prefix (Bar "b") $ prefix (Bar "c") $ Commit nil) (V "x"))

p7' :: Proc
p7' = Restr "a" 
      $ Par 
       ( Tran 0 (prefix (Act "a") $ prefix (Act "b") $ Commit nil) (nil))
      $ Par  
       ( Tran 1 (prefix (Bar "a") $ prefix (Act "c") $ Commit nil) (nil))
       ( Tran 2 (prefix (Bar "b") $ prefix (Bar "c") $ Commit nil) (nil))


p8 :: Proc
p8 = Rec "x" $ Atomic (Sum [(Act "a", Commit nil), (Act "a", nil)]) (V "x")

p9 :: Proc
p9 = Rec "x" $ Atomic (Sum [(Act "a", Commit nil)]) (V "x")



-------       
mh1 :: Proc
mh1 = Tran 0 (prefix (Act "a") $ prefix (Act "b") $ Commit nil) nil

mh1' :: Proc
mh1' = Restr "p" $ 
  Par (Tran 0 (prefix (Act "a") $ Commit $ prefix (Act "p") nil) nil)
      (Tran 1 (prefix (Bar "p") $ prefix (Act "b") $  Commit nil) nil)

mh1'' :: Proc
mh1'' = Restr "p" $ 
  Par (Tran 0 (prefix (Act "a") $ prefix (Act "p") $ Commit nil) nil)
      (Tran 1 (prefix (Bar "p") $ prefix (Act "b") $  Commit nil) nil)

-------
-------
mh2 :: Proc
mh2 = Rec "x" $ Atomic (prefix (Act "a") $ Sum [(Act "b", Commit nil), (Act "c", Commit nil)]) (V "x")

mh2' :: Proc
mh2' = Rec "x" $ Atomic (Sum [(Act "a", prefix (Act "b") $ Commit nil), 
                              (Act "a", prefix (Act "c") $ Commit nil)]) (V "x")

-------
-------
mh3 :: Proc
mh3 = Rec "x" $ Atomic (prefix (Act "a") $ prefix  (Act "b") $ Commit nil) (V "x")

mh3' :: Proc
mh3' = Rec "x" $ Atomic (Sum [(Act "a", prefix (Act "b") $ Commit nil), 
                              (Act "a", prefix (Act "c") $ nil)]) (V "x")
-------
-------
mh4 :: Proc
mh4 = Par (Tran 0 (prefix (Act "a") $ Commit nil) nil) 
          (Tran 1 (prefix (Act "b") $ Commit nil) nil)

mh4' :: Proc
mh4' =  Restr "p" $ 
        Par (prefix (Bar "p") nil) $
        Par (Tran 0 (prefix (Act "a") $ prefix (Act "p") $ Commit (prefix (Bar "p") nil)) nil) 
            (Tran 1 (prefix (Act "b") $ prefix (Act "p") $ Commit (prefix (Bar "p") nil)) nil)

-------
-------
mh5 :: Proc
mh5 = Tran 0 (Sum [(Act "a", prefix (Act "b") $ Commit nil), 
                   (Act "b", prefix (Act "a") $ Commit nil)]) nil 

mh5' :: Proc
mh5' =  Restr "p" $ 
        Par (Tran 0 (prefix (Act "a") $ prefix (Act "p") $ Commit nil) nil) 
            (Tran 1 (prefix (Act "b") $ prefix (Bar "p") $ Commit nil) nil)
-------
-------
p10 :: Proc
p10 = nil

p11 :: Proc
p11 = Tran 0 (prefix (Act "a") nil) nil


collectActs :: Proc -> S.Set Action
collectActs proc = case proc of 
  Sum    acts   -> foldr (\(a, p) acc-> let pActs =  collectActs p `S.union` acc
                                        in case a of
                                          Act x -> x `S.insert` pActs
                                          Bar x -> x `S.insert` pActs
                                          Tau   -> pActs 
                          ) S.empty acts
  Par    p q    -> collectActs p `S.union` collectActs q
  Restr  a p    -> a `S.delete` collectActs p
  V      _      -> S.empty
  Rec    _ p    -> collectActs p
  Tran   _ p q  -> collectActs p `S.union` collectActs q
  Commit p      -> collectActs p
  Atomic p q    -> collectActs p `S.union` collectActs q
  

printTrLTS :: TrLTS -> IO ()
printTrLTS s = printTrBisimulation s s empty 

main::IO ()
main = do 
   let proc1 = mh3
       proc2 = mh3'
   
   let acts = collectActs proc1 `S.union` collectActs proc2
   lts1@(ss1, _, _) <- makeTLTS proc1 acts
   printTrLTS lts1
   lts2@(ss2, _, _) <- makeTLTS proc2 acts
   printTrLTS lts2 

   let pids1     = filter (\ (_, y) -> show proc1 == y ) (S.toList ss1)
       (pid1, _) = head pids1
       pids2     = filter (\ (_, y) -> show proc2 == y ) (S.toList ss2)
       (pid2, _) = head pids2 
   
   (b, (_, _, rejs, bisim, _)) <- runBisim lts1 lts2 pid1 pid2
   
   if b 
    then do
      putStrLn "Bisimulation found:"
      printTrBisimulation lts1 lts2 bisim
      putStrLn "Bisimulation found."      
    else do
      putStrLn "No bisimulation found:"
      putStrLn $ "rejs: " ++ unlines (map (\(x,y,delta) -> show (x, y, delta) ) (S.toList rejs))
      putStrLn "No bisimulation found."
      
   