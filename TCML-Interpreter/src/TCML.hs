 module TCML (runTCML) where

import Concurrency.Data.Log
import Concurrency.Data.LogRequests
import Concurrency.Data.Transactions
import Concurrency.Util.Stats
import Concurrency.Util.Util
import Concurrency.ServerInit
import Definitions
import Lambda
import Syntax hiding (pp)
import Prelude hiding (catch, log)

import Control.Exception
import Control.Concurrent (myThreadId, threadDelay, killThread, killThread, forkIO, ThreadId)
import Control.Monad (forever, when)
import Data.Unique (newUnique)
import Control.Concurrent.MVar (newEmptyMVar, takeMVar, putMVar, tryTakeMVar)
import Data.IORef (IORef, readIORef, writeIORef, writeIORef, newIORef)
import System.CPUTime (getCPUTime)
import System.Exit (exitSuccess)
import Data.Maybe (fromMaybe)

-------------------
-- Reduction Loop
-------------------
----------------------------------------------------
-- eval is the main execution loop of TCML, which 
-- either spawns a new thread or performs an
-- execution step
----------------------------------------------------
evalLoop :: RequestLock -> LogLock -> (TProcess -> IO TProcess) -> Printer -> IORef TProcess -> TProcess -> IO TProcess 
evalLoop r l doTStep pp pRef p =  case p of 
  TP _ _ ts _ True       -> do
    notifyThreadDead r ts
    return p
  
  -- evaluation ends when a process reduces to a value
  TP (V _) [] [] _ _    ->  do
    notifyThreadDead r []
    return p  

  -- remove NIL processes
  TP NIL _ [] _ _       -> do
    notifyThreadDead r []
    return p

  -- spawn process
  TP (Par e1 e2) cs ts _ _   ->
    mask $ \restore -> (do 
      tid <- myThreadId
      pushMVar r (Wait tid)
      
      restore (do
        let mainProc = makeProc e2 cs ts 
        ackMVar <- newEmptyMVar
        tid' <- forkIO $ do
          _ <- takeMVar ackMVar
          let parAlts = map (\t -> A (tname t) (TP NIL [] [] False True)) ts
          let parProc = setUpdate $ makeProc e1 [] parAlts
          parPRef <- newIORef parProc
          _ <- evalLoop r l doTStep pp parPRef parProc 
               `catches` evalExceptionsHandler r l doTStep pp pRef 
          return ()
        
        pushMVar r (Emb tid' ts)
        writeIORef pRef mainProc  -- save proc
        putMVar ackMVar ()
        pushMVar r (Emb tid ts)
        
        evalLoop r l doTStep pp pRef mainProc
         `catches` evalExceptionsHandler r l doTStep pp pRef
        )
          
     ) `catches` evalExceptionsHandler r l doTStep pp pRef


    
  _                ->  mask $ \restore -> 
                        (do
                        -- ppint debug traces
                        tid <- myThreadId
                        pp WorkerTraces $ show tid ++ ": " ++ show p
                        
                        -- do a lambda or concurrency step
                        p' <- doTStep p
                        writeIORef pRef p'
                        restore $ evalLoop r l doTStep pp pRef p')
                        `catches` evalExceptionsHandler r l doTStep pp pRef
                        


-- requests
notifyThreadDead :: RequestLock -> Alternatives-> IO ()        
notifyThreadDead r ts = do
    tid <- myThreadId
    let ks = map tname ts
    pushMVar r (Dead tid ks)


signalSuccess :: RequestLock -> IO ()
signalSuccess r = pushMVar r Success


-- exception handlers                        
evalExceptionsHandler :: RequestLock
                                    -> LogLock
                                    -> (TProcess -> IO TProcess)
                                    -> Printer
                                    -> IORef TProcess
                                    -> [Handler TProcess]
evalExceptionsHandler r l doTStep pp pRef = [
                          Handler (tsignalHandler r l doTStep pp pRef),
                          Handler (ignoreHandler pp pRef),
                          Handler (unignoreHandler l pp)
                        ] 

topLevelCatch :: RequestLock
                  -> LogLock
                  -> Integer
                  -> (TProcess -> IO TProcess)
                  -> Printer
                  -> IORef TProcess
                  -> ThreadId 
                  -> ThreadId
                  -> [Handler TProcess]
topLevelCatch r l start doTStep pp pRef gID sID= do
  tsigHandlers <- evalExceptionsHandler r l doTStep pp pRef
  let anyException = Handler (\e  -> do
      tid <- myThreadId
      
      threadDelay 10000
      when (show e == "<<timeout>>") $
        do
          killThread gID
          killThread sID
          log <- takeMVar l
          currTime <- getCPUTime
          
          maybeNewReqs <- tryTakeMVar r
          let reqs = fromMaybe [] maybeNewReqs
        
          let latestReqs = deleteExpiredUpdates reqs
          let log' = updateLog log latestReqs
          
          putStrLn $ getStatsCSV (currTime - start) (stats log')
          exitSuccess
          
      error ("Exception caught in main (" ++ show tid ++ "): " 
                                          ++ show (e :: SomeException)))
  return tsigHandlers ++ [anyException]
                        
-- handle signal
tsignalHandler :: RequestLock 
                              -> LogLock 
                              -> (TProcess -> IO TProcess) 
                              -> Printer 
                              -> IORef TProcess 
                              -> TSignal 
                              -> IO TProcess
tsignalHandler r l doTStep pp pRef e = uninterruptibleMask $ \restore ->
  do
    tid <- myThreadId
    pp WorkerTraces $ show tid ++ " signal \"" ++ show e ++ "\" received!"
    
    -- read latest process reduction
    p <- readIORef pRef
    
    -- handle tsignal
    let p'  = trHandler p e tid
    let p'' = setUpdate p'
    
    -- update latest process
    writeIORef pRef p'' 
    
    restore $ evalLoop r l doTStep pp pRef p''



ignoreHandler :: Printer -> IORef TProcess -> BlockedIndefinitelyOnMVar -> IO TProcess
ignoreHandler pp pRef e = uninterruptibleMask $ \restore -> do
    tid <- myThreadId
    pp ExceptionTrace (str tid)
    p <- readIORef pRef 
    restore $ return p
      where str tid = "IGNORED EXCEPTION: " ++ show tid ++ " >>> " ++ show e
      

unignoreHandler :: LogLock -> Printer -> SomeException -> IO TProcess
unignoreHandler l pp e = do
  --print debug info and stats 
  tid <- myThreadId
  pp ExceptionTrace (show tid ++ " >> " ++ show e)
  maybeLog <- tryTakeMVar l
  case maybeLog of
    Nothing -> throw e
    Just log  -> do
      currTime <- getCPUTime
      putStrLn (getStatsCSV (fromInteger currTime) (stats log))
      throw e
      

----------------------------------
 --- Transaction Evaluation -----
----------------------------------
getDoTStep :: (TProcess -> IO TProcess) 
                                        -> RequestLock 
                                        -> TProcess 
                                        -> IO TProcess
getDoTStep doStep r p = 
  case p of
  ----------------------------------
   --- Scheduler Update -----------
  ----------------------------------

  -- update the scheduler
  TP _ _ ts True _        ->  do
    -- send an update to the scheduler log about the thread's transaction status
    tid <- myThreadId
    pushMVar r (Emb tid ts)
    return p{toBeUpdated = False}

  ----------------------------------
   --- Value Evaluation -----------
  ----------------------------------
  -- wait for commit signal
  TP (V _) [] (_:_) _ _    ->  forever $ threadDelay 500000
  
  -- pop the context stack and apply the value
  TP (V val) (c:cs) ts _ _ ->  return (makeProc (c (V val)) cs ts)

  ----------------------------------
   --- NIL Evaluation -------------
  ----------------------------------

  TP SignalSuccess cs [] _ _ -> do
    signalSuccess r
    return $ makeProc (V Unit) cs []

  TP SignalSuccess _ (_:_) _ _ -> forever $ threadDelay 500000

  ----------------------------------
   --- NIL Evaluation -------------
  ----------------------------------
  -- wait for commit signal
  TP NIL _ (_:_) _ _    ->  forever $ threadDelay 500000


  ----------------------------------
   --- Transactions Evaluation ----
  ----------------------------------
  -- substitute transaction name binding with a fresh transaction name 
  TP (Tr (FTN _) def alt) cs ts _ _ ->  do
    trName <- newUnique
    
    let tvar = TN trName
    let def' = substTN 0 tvar def
    let e'   = Tr tvar def' alt
    let p'   = makeProc e' cs ts
    
    return p'

  
  -- build default and alternative ppocess
  TP (Tr (TN x) e1 e2) cs ts _ _ ->  do
    let p'  = setUpdate $ makeProc e2 cs []
    let t   = A (TN x) p'
    let ts' = ts ++ [t]
    let p'' = setUpdate $ makeProc e1 cs ts'
            
    -- run the default process
    return p''

  -- signal to the scheduler that the transaction is ready to commit
  TP (Co (TN k)) cs ts _ _ ->  do
    let ks = map tname ts     -- TODO: remove superfluous transaction names from ks
    pushMVar r (C ks (TN k))       -- send an updated to the scheduler  
    let p' = makeProc (V Unit) cs ts
    
    return p'
     
  _                         -> doStep p
                                     


  
  ------------------------------------
   -- Concurrency and Lambda steps --
  ------------------------------------
getDoStep :: (TProcess -> IO TProcess) -> TProcess -> IO TProcess
getDoStep concEval p =
    if isConcStep p
      then concEval p
      else return $ doLambdaStep p

     
        
-------------------------
-- TCML evaluators
-------------------------
-- inits concurrent evaluators
runTCML :: Integer -> Integer -> Int -> Int -> [DebugFlag] -> SchedulerKind -> Expression -> IO (Value, Stats)
runTCML start ath spt gpt b sched e = do
  -- enable printing debug info (pp= pretty printing)
  let pp = initPrinter b
  
  -- init scheduler 
  (concStep, rlock, llock, gID, sID) <- initScheduler ath spt gpt sched pp
  
  -- create reduction functions
  let doStep = getDoStep concStep
  let doTStep = getDoTStep doStep rlock
  
  -- start
  let p = setUpdate $ makeProc e [] []
  pRef <- newIORef p
  
  let allExceptions = topLevelCatch rlock llock start doTStep pp pRef gID sID
  p' <- evalLoop rlock llock doTStep pp pRef p `catches` allExceptions 
  
  
  -- clean up
  mainID <- myThreadId
  finalLog <- takeMVar llock
  strictLog <- evaluate finalLog
  pushMVar rlock (Terminate (Just mainID))
  killThread sID
  killThread gID
  
  -- result
  case expr p' of 
    V v -> return (v, stats strictLog)
    _   -> error $ "Unexpected result: " ++ show (expr p')
