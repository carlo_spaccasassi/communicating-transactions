module Main where

--import qualified Data.Set as Set

import Concurrency.Util.Stats
import Concurrency.Util.Util
import Concurrency.ServerInit
import Parser(parser)
import Syntax
--import Types.CustomSimplifications
--import Types.Transformations
--import Types.TAE
--import Types.AlgorithmR
--import Types.AlgorithmW
import TCML

import Control.Concurrent (myThreadId)
import System.Console.GetOpt
import System.CPUTime
import System.Environment
import System.Timeout (timeout)
--import System.Exit (exitSuccess)
--import Debug.Trace (trace)

version :: Double
version = 0.2



------------------------------------
-- TEST DEFAULT CONSTANTS
------------------------------------
defaultSchedPollingTime :: Int
defaultSchedPollingTime = 0

defaultAbortThreshold :: Integer
defaultAbortThreshold = 30000000000 -- 3 seconds in picoseconds


defaultGathererSchedPollingTime :: Int
defaultGathererSchedPollingTime = 10000

defaultTimeout :: Int
defaultTimeout = 2 * 60 * 1000000 -- 2 minutes

------------------------------------
-- COMMAND LINE SETUP
------------------------------------
-- the cases of the command-line arguments
data Flag = Help | Version | Input String | DFlag DebugFlag 
          | SFlag SchedulerKind 
          | SPollingTime String      -- scheduler polling time 
          | GPollingTime String      -- gatherer polling time 
          | SAbortThreshold String   -- scheduler abort threshold
          | Timeout String
          | Typecheck
  deriving Eq

{- This datatype is defined in Concurrency/Util/Util.hs
data DebugFlag = SchedOptions           -- Shows scheduler's options at each step
               | SchedChoice            -- Shows scheduler's choice at each step
               | WorkerTraces           -- Show the reduction steps taken by all TCML processes
               | SchedRequests          -- Show status updates sent from the TCML processes to the Request Gatherer
               | CurrentTTrie           -- Show the current Transactions Trie in the Log 
               | ExceptionTrace         -- Display any Haskell exception (except TSignals) caught by any process
               | All                    -- Activates all flags
-}
  
-- the syntax of command-line arguments
options :: [OptDescr Flag]
options = [
  -- info
  Option "?" ["help"]            (NoArg Help)               "shows help",
  Option "v" ["version"]         (NoArg Version)            "shows version number and exit",
  
  -- input
  Option "f" ["file"]            (ReqArg Input "FILE")      "reads source from file (default is standard input)",
  
  -- timed options
  Option "t" ["timeout"]         (ReqArg Timeout "TIMEOUT")      "stops execution after TIMEOUT in 10^-6 sec (the default timeout is 2 minutes)",
  Option ""  ["SPollingTime"]    (ReqArg SPollingTime "SPTIME")  "scheduler polling time in 10^-9 sec (default is 0 msec)",
  Option ""  ["GPollingTime"]    (ReqArg GPollingTime "GPTIME")  "gatherer polling time in 10^-9 sec (default is 10 msec)",
  Option ""  ["SAbortThreshold"] (ReqArg SAbortThreshold "ATTIME")  "time threshold to abort a transaction in 10^-12 seconds (default is 3 sec)",
  
  -- debug
  Option "s" ["DSchedOptions"]   (NoArg (DFlag SchedOptions))    "shows scheduler's options at each step",
  Option "c" ["DSchedChoice"]    (NoArg (DFlag SchedChoice))     "shows scheduler's choice at each step",
  Option "r" ["DSchedRequests"]  (NoArg (DFlag SchedRequests))   "shows status updates sent from the TCML processes to the Request Gatherer",
  Option "w" ["DWorkerTraces"]   (NoArg (DFlag WorkerTraces))    "shows the reduction steps taken by all TCML processes",
  Option "i" ["DCurrentTTrie"]   (NoArg (DFlag CurrentTTrie))    "shows the current Transactions Trie in the Log",
  Option "e" ["DExceptionTrace"] (NoArg (DFlag ExceptionTrace))  "displays all Haskell exception (except TSignals) caught by any process",
  Option ""  ["DStats"]          (NoArg (DFlag SStats))          "prints scheduler statistics",
  Option ""  ["DAll"]            (NoArg (DFlag All))             "Turns on all debug flags",
  
  -- type checking
  Option ""  ["typecheck"]       (NoArg Typecheck)             "typechecks a program without running it",

  -- scheduler
  Option "" ["SRandomSignals"]       (NoArg (SFlag RandomSignals))              "Random scheduling policy",
  Option "" ["SRandomAll"]           (NoArg (SFlag RandomAll))                  "Random scheduling policy and random synchronization policy",  
  Option "" ["SRandomStaged"]        (NoArg (SFlag RandomStaged))               "Random staged scheduling policy (Commit - Embed - Abort)",
  Option "" ["SInteractiveSignals"]  (NoArg (SFlag InteractiveSignals))         "(Interactive Signal - Random Concurrency) scheduling policy",
  Option "" ["SCommunicationDriven"] (NoArg (SFlag StagedCommunicationDriven))  "Random staged concurrency driven scheduling policy (Commit - CD_Embed - Embed - Abort)",
  Option "" ["SCommunicationDriven2"](NoArg (SFlag StagedCommunicationDriven2)) "Random staged concurrency driven scheduling policy, version 2 (Commit - CD_Embed - Abort (5%) or DoNothing (95%))",
  Option "" ["SCommunicationDriven3"](NoArg (SFlag StagedCommunicationDriven3)) "Random staged concurrency driven scheduling policy, version 3 (Commit - CD_Embed - Abort oldest transaction (threshold)",
  Option "" ["SCommunicationDriven4"](NoArg (SFlag StagedCommunicationDriven4)) "Random staged concurrency driven scheduling policy, version 3 (Commit - CD_Embed - Abort oldest transaction (threshold or deadlocked)",
  Option "" ["S3WayRendezvous"]      (NoArg (SFlag StagedThreeWayRendezvous))    "Dummy scheduler for the Three-Way Rendezvous example",
  
  Option "" ["SInteractive"]         (NoArg (SFlag InteractiveAll))             "(Interactive Signal - Interactive Concurrency) scheduling policy"
  ]


-- find a filename in a flag or else return 2nd argument
findFile :: Flag -> String -> String
findFile flag fname = case flag of
                        Input file -> file
                        _          -> fname

-- finds scheduler polling time
findSPT :: Flag -> Int -> Int
findSPT (SPollingTime n)  _   = read n
findSPT _                 def = def
                        
                        
-- finds gatherer polling time
findGPT :: Flag -> Int -> Int
findGPT (GPollingTime n)  _   = read n
findGPT _                 def = def

-- finds scheduler threshold to abort
findAT :: Flag -> Integer -> Integer
findAT (SAbortThreshold n)  _   = read n
findAT _                    def = def


-- find a filename in a flag or else return 2nd argument
findTimeout :: Flag -> Int -> Int
findTimeout (Timeout t) _   = read t
findTimeout _           def = def


-- collect all debug flags in a list
-- if "DAll" flag is set then ignore all others
collectDebugFlags :: Flag -> [DebugFlag] -> [DebugFlag]
collectDebugFlags flag dflags = case dflags of
  [All]     -> dflags
  _         -> case flag of
    DFlag All -> [All]
    DFlag f   -> f : dflags
    _         -> dflags


-- find a filename in a flag or else return 2nd argument
findSchedulerFlag :: Flag -> SchedulerKind -> SchedulerKind
findSchedulerFlag flag schedType = case flag of
                        SFlag x -> x
                        _       -> schedType




------------------------------------
-- TEST RUN
------------------------------------


-- returns a TCML run action, configured by the input flags  
configureRun :: [Flag] -> IO (Integer -> IO (Value, Stats))
configureRun flags = do
  -- read contents of file (or stdio)
  let filename = foldr findFile "" flags  -- find file name by going over flags
  code <- if length filename > 0       
    then readFile filename
    else getContents
        
  -- parse the contents
  case parser filename code of
        Left  err  -> error $ "parse error at " ++ show err
        Right p -> do
          -- collect remaining flags
          let debugFlags          = foldr collectDebugFlags [] flags
          let schedulerType       = foldr findSchedulerFlag RandomStaged flags
          let schedPollingTime    = foldr findSPT defaultSchedPollingTime flags 
          let gathererPollingTime = foldr findGPT defaultSchedPollingTime flags
          let abortThreshold      = foldr findAT defaultAbortThreshold flags
--          prog <- instantiate p
          
--          if Typecheck `elem` flags
--            then
--              do
--                let tc = TCtx []
--                (_, t, b, c) <- algorithmW tc prog
--                ( s1, t1, b1, c1 ) <- typeOfExpr tc prog      -- get program type
--                ( s2, c2 ) <- doAlgorithmF c1                 -- do algorithm F
--                let ( c3, t3, b3 ) = simplify tc (c2, apply s2 t1, ( apply s2 . apply s1 ) b1 )  -- do algorithm R
      
--                putStrLn "\nAfter W':"
--                putStrLn $ "Behaviour:   " ++ show b1
--                putStrLn $ "Type:  " ++ show t1
--                putStrLn "Constraints: " 
--                mapM_ print (Set.toList c1)
      
--                putStrLn "\nAfter F:"          
--                putStrLn $ "Behaviour (after substitution and remove nils):   " ++ show (apply s2 $ removeNils' b1)
--                putStrLn $ "Type:  " ++ show ( apply s2 t1 )
--                putStrLn "\nBroken Constraints: " 
--                mapM_ print (Set.toList c2)
--                          
--                putStrLn "\nAfter R:"
--                putStrLn $ "Behaviour: " ++ show b3
--                putStrLn $ "Type: " ++ show t3          
--                putStrLn "\nSimplified Constraints: " 
--                mapM_ (print . removeNils) (Set.toList c3)
--                
--                
--                putStrLn $ "\nFinal Behaviour:\n" ++ show ( unfold (Set.map removeNils c3) (removeNils' b3) )
--                putStrLn $ "\nFinal Behaviour + trasformations:\n" ++ show  
--                   (removeNils'
--                    (unfold (Set.map removeNils c3) 
--                      (unfold (Set.map removeNils c3) (removeNils' b3))))
--                
--                putStrLn "-- Final results --"
--                putStrLn  "Constraints (no nils):"
--                mapM_ print (Set.toList ( Set.map removeNils c ))
--                putStrLn $ "\nBehaviour (no nils): " ++ show ( removeNils' b )
--                
--                let (c4, b4) = simplifyConstraints c b
--                putStrLn "\n---------------------------\nAfter more simplifications:"
--                putStrLn  "Constraints:"
--                let c5 = Set.map removeNils c4
--                mapM_ print ( Set.toList c5 )
--                putStrLn "\nBehaviour:"
--                let b5 = unfold c5 (removeNils' b4)
--                print b5
--                
--                putStrLn "\nAfter even more simplifications:"
--                let (c6, _, b6) = algorithmR ( TCtx [] ) (c5, t, b5)
--                putStrLn  "Constraints:"
--                let c7 = Set.map removeNils c6
--                mapM_ print ( Set.toList c7 )
--                putStrLn "\nBehaviour:"
--                let b7 = removeNils' b6
--                print b7
                
--                putStrLn "\nAfter one more algorithmR:"
--                let (c5, _, b5) = algorithmR tc (c4, t, b4)
--                let (c6, b6) = simplifyConstraints c5 b5
--                putStrLn  "Constraints:" 
--                mapM_ print (Set.toList ( Set.map removeNils c6 ))
--                putStrLn "\nBehaviour:" 
--                print ( removeNils' b6 )
--                return $ \_ -> exitSuccess --return ( Unit, Stats 0 0 0 0 0 )
                
                
          
          -- return the interpreter function to be run
--            else 
          return (\start -> runTCML  start
                                            abortThreshold
                                            schedPollingTime
                                            gathererPollingTime
                                            debugFlags
                                            schedulerType
                                            p)


------------------------------------
-- MAIN FUNCTIONS 
------------------------------------
{- read the code and run the interpreter.
   If the interpreter takes more than RUN_TIMEOUT milliseconds to run, the 
   interpreter is aborted. Note that if any debug flag is selected, the timeout
   function will reset its internal timeout counter each time something is 
   printed on the IO. In most cases debug flags do not interact well with 
   timeouts (e.g. if there are any infinite loops in the input code). -}    
run :: [Flag] -> IO()
run flags
  -- print Help info
  | Help `elem` flags = do 
     progname <- getProgName
     putStr $ usageInfo ("Usage: " ++ progname ++ " [OPTION...]") options
  
  -- just print version and exit
  | Version `elem` flags =  
     putStr $ "Version: " ++ show version        
  
  -- run the interpreter with a timeout
  | otherwise = do                            
     -- configure run      
     code      <- configureRun flags
     let maxTime = foldr findTimeout defaultTimeout flags
               
     start <- getCPUTime                     
     retVal    <- timeout maxTime (code start)    -- run code
     end   <- getCPUTime  
     
     -- display results
     tid    <- myThreadId
     case retVal of
      Nothing -> error "Execution timed out."
      Just v  -> do
       -- print the result value
       putStrLn $ show tid ++ " >>>>> " ++ show (fst v)
       
       -- print the measurements
       let diff = end - start
       putStrLn $ getStatsCSV diff (snd v)
               
        
        
-- Main reads command-line arguments
main :: IO()
main = do
  progname  <- getProgName
  args      <- getArgs
  let header = "Usage: " ++ progname ++ " [OPTION...]"
  let usage  = usageInfo header options
  case getOpt RequireOrder options args of
    (flags, [],      [])  -> run flags
    (_,     nonOpts, [])  -> error $ "unrecognized arguments: " 
                                  ++ unwords nonOpts ++ "\n" 
                                  ++ usage
    (_,     _,       err) -> error $ concat err ++ usage
