
module Concurrency.InteractiveConcurrency where

import Syntax
import Data.Unique
import Control.Concurrent (myThreadId)
import Control.Concurrent.Chan (newChan, readChan)
import Concurrency.Util.Util
import Concurrency.Data.Log
import Concurrency.Data.LogRequests

doInteractiveConcurrency :: RequestLock -> TProcess -> IO TProcess
doInteractiveConcurrency reqLock p = case e of
    NewChannel l                  -> do 
                                      chanId <- newUnique
                                      let chanName = "c_" ++ show ( hashUnique l )
                                      let newCh  = V (Chan (CN chanId chanName) 0)
                                      return (makeProc newCh ct ts)      
                        
    Send  (Chan c _) v            -> do
                                    tID <- myThreadId
                                    sendCh <- newChan
                                    pushMVar reqLock (S (SR tID c v sendCh (map tname ts)))
                                    _ <- readChan sendCh --ack
                                    return (makeProc (V Unit) ct ts)
                                    
    Receive (Chan c _)            -> do
                                    tID <- myThreadId
                                    recvCh <- newChan
                                    pushMVar reqLock (R (RR tID c recvCh (map tname ts)))
                                    v <- readChan recvCh
                                    return (makeProc (V v) ct ts)
                                    
    _                           -> error $ "Unexpected expression " ++ show e ++ 
                                           " found during concurrency evaluation"
  where e   = expr p
        ct  = ctx p
        ts  = tr p