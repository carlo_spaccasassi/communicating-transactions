module Concurrency.Util.Util where

import Syntax hiding        (pp)

import Control.Monad (when, liftM)
import Control.Concurrent
import Debug.Trace          (trace)

import Prelude hiding       (lookup)
import System.Random (getStdRandom, randomR)
import Data.Maybe (fromJust)

--------------------------------------------------------------------
 ----------------- Concurrency Utils ------------------------------
--------------------------------------------------------------------

-- is this a concurrency step? Note that (Par e1 e2) is not a concurrency step.
-- This is because a concurrency process in TCML is represented by a Haskell thread
isConcStep :: TProcess -> Bool
isConcStep p = case expr p of
  (Send (Chan _ _) _)   -> True
  (Receive (Chan _ _))  -> True
  (NewChannel{})           -> True
  _                     -> False

                        
-- | pushes a new element on a stack in an mvar (if you are considering a list as a stack)
pushMVar :: MVar [a] -> a -> IO ()                 
pushMVar lock x = modifyMVar_ lock (\xs -> return $ x:xs)
                      

--------------------------------------------------------------------
 ----------------- Randomness Utils -------------------------------
--------------------------------------------------------------------
getRandomElem :: [a] -> IO (Maybe a)
getRandomElem [] = return Nothing
getRandomElem xs  = do
  idx <- getRandomIdx xs
  return $ Just (xs !! idx)


getRandomElem' :: [a] -> IO a
getRandomElem' x = liftM fromJust (getRandomElem x)


getRandomPercentage :: IO Integer
getRandomPercentage = getRandomElem' [0::Integer .. 100]


getRandomIdx :: [a] -> IO Int
getRandomIdx list = getStdRandom (randomR (0, length list - 1))


getRandomPermutation :: [a] -> IO (Maybe (a, [a]))
getRandomPermutation [] = return Nothing
getRandomPermutation xs = do
  i <- getRandomIdx xs
  let (xs1, xs2) = splitAt i xs
  let ret | null xs1  = (head xs2, tail xs2)
          | null xs2  = (head xs1, tail xs1)
          | otherwise = (head xs2, xs1 ++ tail xs2)
  return $ Just ret

--------------------------------------------------------------------
 ----------------- Printing Utils ---------------------------------
--------------------------------------------------------------------
data DebugFlag = SchedOptions  | SchedChoice  | WorkerTraces 
               | SchedRequests | CurrentTTrie | ExceptionTrace
               | SStats        | All
  deriving Eq
      
type Printer = DebugFlag -> String -> IO ()
                
initPrinter :: [DebugFlag] -> Printer
initPrinter flags = case flags of 
  []      -> \_ _ -> return ()
  [All]   -> \_ s -> trace s (return ())
  _       -> if All `elem` flags
              then initPrinter [All]
              else \f s -> when (f `elem` flags) $ trace s (return ()) 
         

numberList :: Show a => [a] -> String
numberList xs = concat $ numberList' 0 $ map show xs


numberList' :: Int -> [String] -> [String]
numberList' _ [] = []
numberList' i (x:xs) = (show i ++ ") " ++ x ++ "\n") : numberList' (i + 1) xs


--------------------------------------------------------------------
 ----------------- Miscelleanea ----------------------------------
--------------------------------------------------------------------
orElse :: [a] -> [a] -> [a]
orElse [] e = e
orElse e  _ = e

orElseM :: Monad m => m [a] -> m [a] -> m [a] 
orElseM e1 e2 = do
  r1 <- e1  
  if null r1
    then e2
    else return r1


orElseM2 :: Monad m => m ([a], b) -> m ([a], b) -> m ([a], b) 
orElseM2 e1 e2 = do
  (r1, b) <- e1  
  if null r1
    then e2
    else return (r1, b)