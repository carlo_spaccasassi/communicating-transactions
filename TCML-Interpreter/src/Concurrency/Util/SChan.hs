
module Concurrency.Util.SChan where

import Control.Concurrent.MVar
import Control.Exception

import Prelude hiding (catch)

data SChan a = SChan (MVar a) (MVar ())

newSChan :: IO (SChan a) 
newSChan = do
  val  <- newEmptyMVar
  ack  <- newEmptyMVar
  let newChannel = SChan val ack 
  return newChannel

-- this handler cleans up the "write" MVar of a channel when a TSignal is received
varHandler :: MVar a -> SomeException -> IO b
varHandler writeVar e = do 
    _ <- tryTakeMVar writeVar
    throw e  
  
  
-- writes on a synchronous channel. If a TSignal is received while a thread was waiting on the "ack" MVar,
-- the "write" Var is emptied to remove the old value it stored 
writeSChan :: SChan a -> a -> IO ()
writeSChan (SChan writeVar ackVar) v = catch 
  (do
    putMVar writeVar v
    takeMVar ackVar) 
  (varHandler writeVar)
  
readSChan :: SChan a -> IO a
readSChan (SChan writeVar ackVar) = mask $
  \restore ->
    (do v' <- takeMVar writeVar
        putMVar ackVar ()
        restore $ return v')
     `catch` varHandler ackVar
     
     
-- breaks the SChan abstraction to give one end of the channel.
-- Only use this if you know what you are doing...
getWriteVar :: SChan a -> MVar a
getWriteVar (SChan writeVar _) = writeVar

-- breaks the SChan abstraction to give one end of the channel.
-- Only use this if you know what you are doing...
getAckVar :: SChan a -> MVar ()
getAckVar (SChan _ ackVar) = ackVar 