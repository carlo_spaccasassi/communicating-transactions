
module Concurrency.Util.Stats where

import Data.CSV (genCsvFile)


data Stats = Stats {
  aborts    :: Int,
  commits   :: Int,
  embeds    :: Int,
  schedTime :: Int,
  throughPut :: Int
}
 deriving ( Show )
 

picoToNano :: Integer -> Integer
picoToNano n = truncate $ fromIntegral n / ((10 :: Double) ^^ (9 :: Int))



getStatsCSV :: Integer -> Stats -> String
getStatsCSV i statCounts = "MEASUREMENTS=" ++ toCSV i statCounts



toCSV :: Integer -> Stats -> String
toCSV execStr statCounts = genCsvFile [csvRecord]
  where 
    csvRecord = [execTime, totSchedTime, abortsCount, commitsCount, embedsCount, tp]   

    execTime     = show (picoToNano execStr) 
    abortsCount  = show (aborts  statCounts)
    commitsCount = show (commits statCounts)
    embedsCount  = show (embeds  statCounts)
    totSchedTime = show (picoToNano (fromIntegral $ schedTime statCounts))
    tp           = show (throughPut statCounts) 