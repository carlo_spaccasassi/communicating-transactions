
module Concurrency.Util.TrChan where

--import Control.Concurrent.Chan.Synchronous (Chan, newChan, writeChan, readChan, isEmptyChan)
import Concurrency.Util.SChan
import Data.Map (Map, insert, lookup, empty)
import Control.Concurrent.MVar (MVar, takeMVar, putMVar, newMVar)
import Prelude hiding ( lookup )
import Definitions
import Data.Unique (hashUnique, Unique, newUnique)
--import Debug.Trace

data TrChan a = TC Unique (MVar (Map TransactionNames (SChan a))) 
  
instance Eq (TrChan a)    where (==) (TC id1 _) (TC id2 _) = id1 == id2
instance Show (TrChan a)  where show (TC chanId _) = 'c' : show (hashUnique chanId)
instance Ord (TrChan a)   where compare (TC id1 _) (TC id2 _) = compare id1 id2

newTrChan :: IO (TrChan a)
newTrChan = do
  newvar <- newMVar empty
  chanId <- newUnique
  let newTrChannel = TC chanId newvar
  return newTrChannel

  
getNewChannel :: TransactionNames -> Map TransactionNames (SChan a) -> 
                MVar (Map TransactionNames (SChan a)) -> IO (SChan a)
getNewChannel ks m mvar = do
      newChannel <- newSChan
      let m' = insert ks newChannel m
      putMVar mvar m'
      return newChannel

send :: TransactionNames -> TrChan a -> a -> IO ()
send ks (TC _ mvar) v = do
  m <- takeMVar mvar
  -- get registered channel; create a new one if none is associated to ks
  c <- case lookup ks m of
    Nothing -> getNewChannel ks m mvar
    Just c' -> do
      putMVar mvar m
      return c'
      
  writeSChan c v
  
      
receive :: TransactionNames -> TrChan a -> IO a
receive ks tc = do
  c <- lookupChannel ks tc
  readSChan c

  
lookupChannel :: TransactionNames -> TrChan a -> IO (SChan a)
lookupChannel ks (TC _ mvar) = do
  m <- takeMVar mvar
  
  -- get registered channel; create a new one if none is associated to ks
  case lookup ks m of
    Nothing -> getNewChannel ks m mvar
    Just c' -> do
      putMVar mvar m
      return c'