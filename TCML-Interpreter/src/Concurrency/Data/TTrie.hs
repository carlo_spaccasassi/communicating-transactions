module Concurrency.Data.TTrie 
  (
  TTrie, 
  emptyTTrie, 
  lookupTTrie,
  lookupThreads,
  addThreads, 
  setCommit, 
  allCommits,
  allThreads, 
  allTransactionNames, 
  allTransactionNamesAt,
  allTraces, 
  getDeltaCandidates, 
  abortNode, 
  embedNode,
  commitNode,
  oldestTransactions,
  updateTTrie,
  initTTrieTime,
  removeProc
  )
    where

import Definitions
import Control.Concurrent
import Data.Map as M
import Data.Set as S
import Control.Monad (filterM)
import Data.List (minimumBy)

--import Concurrency.Data.Log

--------------------------------------------------------------------
 ----------------- Transaction Forest -----------------------------
--------------------------------------------------------------------
data TTrie = TTrie {
  commits    :: S.Set TransactionName, 
  threads    :: S.Set ThreadId, 
  children   :: Map TransactionName TTrie,
  lastUpdate :: Integer}



instance Show TTrie where
  show t = unlines $ draw "k0 " t
    where 
      draw s (TTrie b pids kids lu) = (s ++ comm b ++ lUp lu ++ show (S.toList pids)) : drawSubTrees (M.toList kids)
      drawSubTrees [] = []
      drawSubTrees [(k, t')] =
        "|" : shift "`- " "   " (draw (show k ++ " ") t')
      drawSubTrees ((k, t'):ts) =
        "|" : shift "+- " "|  " (draw (show k ++ " ") t') ++ drawSubTrees ts

      comm b = if S.null b then "" else "(Co: " ++ show (S.toList b) ++ ") "
      lUp z = if z <= 0 then "" else "(timestamp: " ++ show z ++ ") " 
      shift first other = zipWith (++) (first : repeat other)


emptyTTrie :: TTrie
emptyTTrie = TTrie S.empty S.empty M.empty 0


lookupTTrie :: TransactionNames -> TTrie -> Maybe TTrie
lookupTTrie ks t = Prelude.foldl
                    (\trie k -> 
                        case trie of
                          Nothing     -> Nothing
                          Just trie'  -> M.lookup k (children trie')
              ) (Just t) ks

lookupThreads :: TransactionNames -> TTrie -> Maybe [ThreadId]
lookupThreads ks t = case lookupTTrie ks t of 
                      Nothing -> Nothing
                      Just trie -> Just (S.toList (threads trie))


initTTrieTime :: Integer -> TTrie -> TTrie
initTTrieTime ct (TTrie a b c transTimer) = t' 
  where 
    t'  = if transTimer <= 0 
            then TTrie a b c' ct
            else TTrie a b c' transTimer
    c' = M.map (initTTrieTime ct) c  
            

updateTTrie :: Integer -> TransactionNames -> TTrie -> TTrie
updateTTrie _ [] t = t
updateTTrie ct (k:ks) t@(TTrie a b _ _) = TTrie a b c' ct
  where 
    c' = adjust f k (children t)
    f = updateTTrie ct ks    


oldestTransactions :: TTrie -> Maybe [(TransactionNames, Integer)]
oldestTransactions t = leastTransactions
  where
    leastTransactions = 
      if Prelude.null minTransactions 
        then Nothing
        else Just $ Prelude.map fst minTransactions
    minTransactions = Prelude.filter (not . Prelude.null . fst . fst) minTransactions'
    minTransactions' = Prelude.filter oldestFilter allTransactions
    allTransactions = oldestTransaction'' [] t
    
    oldestFilter ((_, time), _) = time == snd (fst minTransaction)

    minTransaction  = minimumBy compareTransactions allTransactions
    compareTransactions t' t'' =
      let timestamp1 = lastUpdate (snd t') 
          timestamp2 = lastUpdate (snd t'') 
      in  compare timestamp1 timestamp2
          
    oldestTransaction'' acc trie = ((acc, lastUpdate t), t) : concatMap (\ (x, y) -> oldestTransaction'' (acc ++ [x]) y) (M.toList (children trie))


addThreads :: TransactionNames -> [ThreadId] -> TTrie -> TTrie
addThreads []     tids  t = t { threads = Prelude.foldr S.insert (threads t) tids}
addThreads [k]    tids  t = t { threads = Prelude.foldr S.delete (threads t) tids, 
                                children = M.alter f k (children t)}
  where
    f Nothing   = Just (addThreads [] tids emptyTTrie)
    f (Just t') = Just (addThreads [] tids t')
  
  
addThreads (k:ks) tids  t = t { children = M.alter f k (children t)}    
  where
    f Nothing   = Just (addThreads ks tids emptyTTrie)
    f (Just t') = Just (addThreads ks tids t')


setCommit :: TransactionNames -> TransactionName -> TTrie -> TTrie
setCommit []      k _ = error $ "Trying to commit a process not embedded into a transaction (Co" ++ show k ++ ")"
setCommit [k']    k t = t { children = adjust (\ t' -> t' { commits = S.insert k (commits t')}) k' (children t) } 
setCommit (k':ks) k t = t { children = adjust (setCommit ks k) k' (children t) }


allThreads :: TTrie -> [ThreadId]
allThreads t = S.toList (threads t) ++ concatMap allThreads (M.elems $ children t)


allTransactionNames :: TTrie -> TransactionNames
allTransactionNames t = keys (children t) ++ concatMap allTransactionNames (M.elems $ children t) 

allTransactionNamesAt :: TransactionNames -> TTrie -> Maybe TransactionNames 
allTransactionNamesAt ks t = case lookupTTrie ks t of
  Nothing -> Nothing
  Just x  -> Just (allTransactionNames x) 


sizeTrie :: TTrie -> Int
sizeTrie t = 1 + sum (Prelude.map sizeTrie (M.elems $ children t))


allCommits :: TTrie ->  [ (TransactionNames, TransactionName, Set TransactionName) ]
allCommits = allCommits' []
  where
    allCommits' ks t = concatMap 
                        (\(k, t') -> (ks, k, commits t') : allCommits' (ks ++ [k]) t')
                        (M.toList (children t))



allTraces :: TTrie -> [(TransactionNames, TransactionName)]
allTraces = allTraces' []
  where
    allTraces' acc trie = Prelude.map (\ x -> (acc, x)) (keys (children trie)) ++
                          concatMap 
                            (\ (x, y) -> allTraces' (acc ++ [x]) y)
                            (M.toList (children trie))



-- removes node under k
abortNode :: TransactionNames -> TransactionName -> TTrie -> TTrie
abortNode [] k      t = t { children = M.delete k (children t) }
abortNode (k':ks) k t = t { children = adjust (abortNode ks k) k' (children t) }



removeProc :: TransactionNames -> ThreadId -> TTrie -> TTrie
removeProc []     pid t = t {threads = S.delete pid (threads t)}
removeProc (k:ks) pid t = t {children = adjust (removeProc ks pid) k (children t)}

embedNode :: TransactionNames -> [TransactionNames] -> TransactionName -> TTrie -> TTrie
embedNode []      delta k t = t { children = children''}
  where
    ksToEmbed   = M.filterWithKey (\key val -> (key : allTransactionNames val) `elem` delta) (children t)
    children'   = M.difference (children t) ksToEmbed
    outerEmbed  = M.lookup k (children t)
    children''  = case outerEmbed of 
                      Nothing -> children t
                      Just t' -> let  
                                   embChildren = children t' `M.union` ksToEmbed
                                   outerEmbed' = t' {children = embChildren }
                                     in M.update (const (Just outerEmbed')) k children'
    
embedNode (k':ks) delta k t = t'
    where
      t' = t { children = adjust (embedNode ks delta k) k' (children t) }
    
    
    
commitNode :: TransactionNames -> TransactionName -> TTrie -> TTrie
commitNode []       k t = t''
  where
    t'' = t' { commits = commits' }
    t'  = t  { children = commitChildren }
    c = M.lookup k (children t)
    (commitChildren, commits') = case c of
      Nothing -> (children t, commits t)
      Just t''' -> (M.delete k (children t) `M.union` children t''', S.delete k (commits t'''))

commitNode (k':ks)  k t = t { children = adjust (commitNode ks k) k' (children t) }



getDeltaCandidates :: TransactionNames -> TransactionName -> TTrie -> [(TransactionNames, [TransactionNames], [ThreadId], TransactionName)]
getDeltaCandidates ks k t = case lookupTTrie ks t of
  Nothing ->  []
  Just t' -> if k `elem` keys (children t')
              then candidates' 
              else [] 
    where
      singleDeltas   = M.keys $ M.filterWithKey (\key t'' -> key /= k && not (S.null (threads t''))) (children t')
      singleDeltas'  = Prelude.map (\x -> x : (case lookupTTrie [x] t' of
                                                         Nothing    -> []
                                                         Just t'''  -> allTransactionNames t''' )) singleDeltas
      powerset       = filterM (const [True, False]) singleDeltas'
      powerset'      = if S.null (threads t')
                        then Prelude.filter (not . Prelude.null) powerset
                        else powerset
      candidates     = Prelude.map (\d -> case d of 
                                            [] -> (ks, [], [], k)
                                            _ -> (ks, d, [], k)) 
                                   powerset' -- adds the empty set
      candidates'    = if S.null (threads t')
                        then candidates
                        else 
                          let 
                            pids  = filterM (const [True, False]) (S.toList $ threads t')
                            pids' = Prelude.filter (not . Prelude.null) pids
                            f (a, b, _, c) = Prelude.map (\x -> (a, b, x, c)) pids'
                          in 
                            concatMap f candidates  
                            
split3wrChoices []        = ([], [])
split3wrChoices ( c:cs )  =
  let (c1, c2) = split3wrChoices cs 
  in if M.size ( children c ) == 0 -- this transaction has no 3wr partner 
         then (c:c1, c2)
         else if M.size ( children c ) == 2  -- nested transactions have not been merged yet 
                then (c1, c:c2)
                else (c1, c2) -- nested transactions have been
                            

get3wrChoices = undefined

--get3wrChoices tt (CR sendReqs recvReqs) = do
--  let (c1, c2) = split3wrChoices ( children tt )  
--  if null sendReqs || null recvReqs
--    then return Nothing
--    else do
--      ret <- getRandomPermutation recvReqs
--      case ret of
--         Nothing -> return Nothing
--         Just (RR _ _ _ ts, rs) -> 
--          if length ts == 0 && length c2 > 2
--            then undefined -- find two receive partners   
--            else if length ts < 3
--              then undefined -- further embed all partners
--              else return Nothing           
  
  
  
  
  
  
  
  