module Concurrency.Data.Transactions (trHandler, TSignal, commit, abort, embed) where

import Data.List (isPrefixOf)
import Definitions
import Syntax

import Prelude hiding (catch)
import GHC.Conc (ThreadId(..))

--import Debug.Trace (trace)

---------------------------------------------------------------------------------------------
---------------------------  CUSTOM EXCEPTIONS  ---------------------------------------------
---------------------------------------------------------------------------------------------

commit :: TransactionNames -> TransactionName -> TSignal
commit = Commit

abort :: TransactionNames -> TransactionName -> TSignal
abort = Abort

embed :: [TransactionName] -> [TransactionNames] -> [ThreadId] -> TransactionName -> TSignal
embed = Embed

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
trHandler       :: TProcess -> TSignal -> ThreadId -> TProcess
trHandler p sig pid = 
  case sig of
    Commit  ks k -> if (ks ++ [k]) `isPrefixOf` ts
                      then makeProc e c (dropEmbedding k alts)
                      else p
    
    Embed   ks d pids k -> if k `elem` ftn
                          || not (ks `isPrefixOf` ts)  
                          || (not (any (\x -> all (`elem` x) ftn) d) 
                                && (length ts /= length ks || pid `notElem` pids))
                        then p 
                        else  
                          let 
                              embeddingIndex = length ks
                              (ts1, ts2)    = splitAt embeddingIndex alts
                              p'            = if pid `elem` pids
                                                then makeProc e c []
                                                else makeProc e c ts2
                                                
                              t'            = A k p'
                              ts'           = ts1 ++ [t'] ++ ts2
                          in 
                            makeProc e c ts'
        where 
          ftn = map tname (drop (length ks) alts) ++ freeTNames (expr p)
                          
    Abort ks k   ->    if (ks ++ [k]) `isPrefixOf` ts
                        then getAlternative ks alts []
                        else p {expr = forceAbort (expr p) k}   -- ignore signal
                              
  where e    = expr p
        c    = ctx p
        alts = tr p
        ts   = map tname alts 


dropEmbedding   :: TransactionName -> Alternatives -> Alternatives
dropEmbedding _ [] = []
dropEmbedding k (a:alts) 
  | k == tname a  = alts 
  | otherwise     = a : dropEmbedding k alts


getAlternative :: TransactionNames -> Alternatives -> Alternatives-> TProcess
getAlternative _ [] _ = TP NIL [] [] False True -- happens only if the thread was spawned in transaction k
getAlternative [] (A _ p' : _) ts = p' {tr = ts ++ tr p'}
getAlternative (_:ks) (t:ts) ts' = getAlternative ks ts (ts' ++ [t])   
  

forceAbort :: Expression -> TransactionName -> Expression
forceAbort e k = case e of
  V v             -> V v
  
  App e1 e2       -> App    (forceAbort e1 k)   (forceAbort e2 k)
  Let x e1 e2     -> Let x  (forceAbort e1 k)   (forceAbort e2 k)
  If e1 e2 e3     -> If     (forceAbort e1 k)   (forceAbort e2 k)  (forceAbort e3 k)
  Op op e1 e2     -> Op op  (forceAbort e1 k)   (forceAbort e2 k)
  NIL             -> NIL
  
  Tup e1 e2       -> Tup    (forceAbort e1 k)   (forceAbort e2 k)
  Fst e1          -> Fst    (forceAbort e1 k)
  Snd e1          -> Snd    (forceAbort e1 k)
  
  Par e1 e2       -> Par    (forceAbort e1 k)    (forceAbort e2 k)
  Send v1 v2      -> Send   v1 v2 
  Receive v       -> Receive v
  NewChannel l    -> NewChannel l
  
  Co k'            -> Co k'
  Tr k' e1 e2      -> if k' == k 
                        then e2 
                        else Tr k (forceAbort e1 k) e2
  
  SignalSuccess    -> SignalSuccess