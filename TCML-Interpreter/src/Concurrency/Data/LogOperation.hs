
module Concurrency.Data.LogOperation (
  getSignalChoices,
  getCommitChoices,
  getAbortChoices,
  getAbortChoice,
  getEmbedChoices,
  getEmbedChoice, 
  getDoNothingChoice,
  getCommunicationChoices,
  doLogOperation
)where

import Concurrency.Data.Log
import Concurrency.Data.LogRequests
import Concurrency.Data.TTrie
import Concurrency.Util.Stats
import Definitions
import Syntax

import Control.Concurrent
import Control.Concurrent.Async (mapConcurrently)
import Data.Map  ( lookup, update, empty, keys)
import qualified Data.Set as S( member, fromList )
import Prelude hiding (lookup, log)



-- Util
deleteNth :: Int -> [a] -> [a]
deleteNth n xs | n > 0      = take n xs ++ drop (n+1) xs
               | n == 0     = tail xs
               | otherwise  = error $ "negative number in deleteNth ( " ++ show n  ++ " )"

                  
-- signals           
sendSignal :: RequestLock -> Log -> TSignal -> IO Log
sendSignal r log signal = do
  {-
      FIXED BUG:  some processes might insert requests 
                  while the scheduler is sending them signals
  -}
  reqs <- takeMVar r
  
  let latestReqs = deleteExpiredUpdates reqs
  let log'       = updateLog log latestReqs
  
  let receiveingThreads = allThreads $ ttrie log'
  _ <- mapConcurrently  (`throwTo` signal) receiveingThreads
  putMVar r []
  
  let log'' = log' {acks = S.fromList receiveingThreads}
  
  return log''


doLogOperation :: RequestLock -> Log -> LogOperation -> IO Log
doLogOperation r log op = case op of
  Just op' -> case op' of 
    Commit ks k       -> doCommit r log ks k
    Embed  ks d1 d2 k -> doEmbed  r log ks d1 d2 k  
    Abort  ks k       -> doAbort  r log ks k 
  Nothing -> return log
          
doCommit :: RequestLock -> Log -> [TransactionName] -> TransactionName -> IO Log
doCommit r log ks k = do
  log' <- sendSignal r log (Commit ks k)

  -- update log
  let log''  = log'  {reqTable = empty} 
  let log''' = log'' {ttrie = commitNode ks k (ttrie log'')} 

  -- update stats
  let s = stats log
  let s'      = s {commits = commits s + 1} 
  let log'''' = log'''{stats = s'} 

  return log''''
  

doAbort :: RequestLock -> Log -> [TransactionName] -> TransactionName -> IO Log
doAbort r log ks k = do
  log' <- sendSignal r log (Abort ks k)
  
  --update log
  let log''   = log' {reqTable = empty} 
  let log'''  = log'' {ttrie = abortNode ks k (ttrie log'')}
  
  -- update stats
  let s = stats log
  let s' = s {aborts = aborts s + 1} 
  let log'''' = log'''{stats = s'} 
  
  return log''''
      

doEmbed :: RequestLock -> Log -> TransactionNames -> [TransactionNames] -> [ThreadId] -> TransactionName -> IO Log
doEmbed r log ks d pids k = do
  log' <- sendSignal r log (Embed ks d pids k)

  -- update log
  let log''  = log'  {reqTable = empty}
  let log''' = log'' {ttrie = embedNode ks d k (ttrie log'')}
  
  -- update stats
  let s = stats log
  let s' = s {embeds = embeds s + 1} 
  let log'''' = log'''{stats = s'} 
   
  return log''''
  
  
  
  
getDeltas :: Log -> [(TransactionNames, [TransactionNames], [ThreadId], TransactionName)]
getDeltas log = deltas
  where   
    traces = allTraces $ ttrie log
    deltas = concatMap (\(x,y) -> getDeltaCandidates x y (ttrie log)) traces

getCommitChoices :: Log -> [LogOperation]
getCommitChoices log = map 
                        (\(ks, k, _) -> getCommitChoice ks k)   
                        (filter (\(_,k,ks) -> S.member k ks) $ allCommits $ ttrie log)
-- (ks `txt` k, ks `co` k)
--  where 
--    txt ks k  = show ks ++ " co " ++ show k
--    co        = doCommit  r log


getCommitChoice :: TransactionNames -> TransactionName -> LogOperation
getCommitChoice ks k = Just (Commit ks k)
    
    
    
getAbortChoices  :: Log -> [LogOperation]
getAbortChoices log  = map (uncurry getAbortChoice) (allTraces $ ttrie log)


getAbortChoice :: [TransactionName] 
                      -> TransactionName 
                      -> LogOperation
getAbortChoice ks k = Just (Abort ks k) 
--  (show ks ++ " ab "  ++ show k, (ks `doAbort` k) r log)





getEmbedChoices  :: Log -> [LogOperation]
getEmbedChoices log  = map (\(a,b,c,d) -> getEmbedChoice a b c d) deltas 
  where
    deltas = getDeltas log

getEmbedChoice ::  TransactionNames 
                -> [TransactionNames] -- delta2
                -> [ThreadId] -- delta1 
                -> TransactionName 
                -> LogOperation 
getEmbedChoice a b c d  = Just (Embed a b c d) 
--  (show ks ++ " emb "  ++ show d ++ " " ++ show pids ++" in " ++ show k, 
--   doEmbed ks d pids k r log)


getDoNothingChoice :: Log -> LogOperation
getDoNothingChoice _ = Nothing 
--("Do nothing", return log)


getSignalChoices :: Log -> [LogOperation]
getSignalChoices log = doNothingChoice : commitChoices ++ embedChoices ++ abortChoices
  where   
          doNothingChoice = getDoNothingChoice log 
          commitChoices   = getCommitChoices   log
          abortChoices    = getAbortChoices    log
          embedChoices    = getEmbedChoices    log
            
            
getCommunicationChoices :: Log -> [(String, IO Log)]
getCommunicationChoices log = foldr (\x acc -> getChannelChoices x ++ acc) [] channels
  where 
    channels  = keys reqTab
    reqTab    = reqTable log
    getChannelChoices ch = case lookup ch reqTab of
      Just reqs -> 
        [(syncDesc s r, doSyncReqs log (sIdx, ss) (rIdx, rs)) | 
          sIdx <- [0 .. length ss - 1], 
          rIdx <- [0 .. length rs - 1],
          let r = rs !! rIdx, 
          let s = ss !! sIdx, 
          rts r == sts s]
        where ss = sends reqs
              rs = recvs reqs
      Nothing   -> []
      
      
syncDesc :: SendRequest -> ReceiveRequest -> String
syncDesc s r = "Synch " ++ show (sid s) ++ " with " ++ show (rid r) ++ " over channel " ++ show (sch s)  


doSyncReqs :: Log -> (Int, [SendRequest]) -> (Int, [ReceiveRequest]) -> IO Log
doSyncReqs log (sIdx, ss) (rIdx, rs) = do
  let s = ss !! sIdx
  let r = rs !! rIdx
        
  writeChan (rchan r) (sval s)
  writeChan (schan s) Unit
  
  let ss' = deleteNth sIdx ss
  let rs' = deleteNth rIdx rs
    
  let userChannel = sch s
  let reqTab = reqTable log
  let tab' = update (\_ -> Just (CR ss' rs')) userChannel reqTab
  
  let log' = log {reqTable = tab'}
  
  return log'
      