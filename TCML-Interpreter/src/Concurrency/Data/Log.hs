module Concurrency.Data.Log where

import Concurrency.Data.TTrie
import Concurrency.Util.Stats
import Definitions
import Syntax

import Control.Concurrent
import Data.Map ( Map, alter, empty , assocs, elems)
import qualified Data.Set as S ( Set, empty, delete )
import Prelude hiding (log)

-- main data structure that keeps track of all communcation requests and transactions
data Log = Log { 
  reqTable  :: ReqTable, 
  stats     :: Stats,
  ttrie     :: TTrie,
  acks      :: S.Set ThreadId,
  solutions :: [[LogOperation]] 
}
 deriving ( Show )

type LogOperation = Maybe TSignal

type ReqTable = Map ChannelName ChannelRequests
type EmbeddingsTable = Map ThreadId Alternatives    -- the embeddings of a thread

--type LogOperation = (String, IO Log)


-- communication requests
data SendRequest =  SR {
  sid   ::ThreadId, 
  sch   :: ChannelName, 
  sval  :: Value, 
  schan :: Chan Value, 
  sts   :: [TransactionName]
} 

instance Show SendRequest
  where
    show req = show (sid req) ++ ": "  ++ show (sch req) ++ " ! " ++ show (sval req) ++ ", " ++ show (sts req)   

data ReceiveRequest = RR {
  rid   :: ThreadId, 
  rch    :: ChannelName, 
  rchan  :: Chan Value, 
  rts    :: [TransactionName]
}

instance Show ReceiveRequest
  where
    show req = show (rid req) ++ ": "  ++ show (rch req) ++ " ? " ++ ", " ++ show (rts req)   



-- log entries
data ChannelRequests = CR {
  sends :: [SendRequest], 
  recvs:: [ReceiveRequest]
}
  deriving ( Show )



-- pretty print Requests Table
ppReqTable :: ReqTable -> String
ppReqTable rt = foldr (\(k, a) rest -> line k a ++ rest) "" tuples 
  where 
    tuples            = assocs rt
    line c (CR ss rs) = show c ++ " / sends: " ++ concatMap (\(SR tid _ _ _ ks) -> show ks ++ " " ++ show tid) ss ++ "\n"
                      ++  replicate (length (show c)) ' ' -- insert as many white spaces as the length of the channel name 
                               ++ " \\ recvs: " ++ concatMap (\(RR tid  _ _ ks) -> show ks ++ " " ++ show tid) rs ++ "\n"


--------------------------------------------------------------------
 ----------------- Log Manipulation -------------------------------
--------------------------------------------------------------------
newEmptyStats :: Stats
newEmptyStats = Stats 0 0 0 0 0


newEmptyLog :: Log
newEmptyLog = Log empty newEmptyStats emptyTTrie S.empty [] 


newLog :: TTrie -> S.Set ThreadId -> Log
newLog x y = Log empty newEmptyStats x y [] 


setTTrie :: TTrie -> Log -> Log
setTTrie t log = log'
  where log' = log {ttrie = t}


addSend :: Log -> SendRequest -> Log 
addSend log req = log'
  where 
        log'  = log {reqTable = reqs'} 
        reqs' = alter updateSend reqChannel reqs
        
        updateSend e  = case e of 
            Just chReqs   -> Just chReqs{sends = req : sends chReqs} 
            Nothing       -> Just (CR [req] [])
            
        reqChannel  = sch req
        reqs        = reqTable log
        

addRecv :: Log -> ReceiveRequest -> Log 
addRecv log req = log'
  where 
        log'      = log { reqTable = reqs' }
        reqs'     = alter updateRecv reqChannel reqs
        
        updateRecv e  = case e of 
            Just chReqs   -> Just chReqs{recvs = req : recvs chReqs} 
            Nothing       -> Just (CR [] [req])
            
        reqChannel = rch req
        reqs       = reqTable log


addCommit :: TransactionNames -> TransactionName -> Log -> Log
addCommit ks k log = log'
  where 
    log'  = log {ttrie = ttrie'}
    ttrie'  = setCommit ks k (ttrie log)


updateEmbeddings :: Log -> (ThreadId, Alternatives) -> Log
updateEmbeddings log (tid, ks) = log'
  where
    log'   = log {ttrie = ttrie', acks= acks'}
    acks'  = S.delete tid (acks log)
    ttrie'  = addThreads ts [tid] (ttrie log)
    ts      = map tname ks
        
isDeadlocked :: Log -> Bool
isDeadlocked log = numberOfThreads == numberOfDeadlockedThreads
  where
    numberOfThreads           = length . allThreads $ ttrie log
    numberOfDeadlockedThreads = deadlockedThreads (elems reqs)
    reqs                      = reqTable log
    

deadlockedThreads :: [ChannelRequests] -> Int
deadlockedThreads []        = 0
deadlockedThreads (CR [] [] : reqs) = deadlockedThreads reqs
deadlockedThreads (CR ss [] : reqs) = length ss + deadlockedThreads reqs
deadlockedThreads (CR [] rs : reqs) = length rs + deadlockedThreads reqs
deadlockedThreads (CR _  _ : reqs) = deadlockedThreads reqs