
module Concurrency.Data.LogRequests (
  Request(S,R,UndoS,UndoR,C,Sp,Emb,Dead,Terminate,Wait,Success), 
  LogLock, RequestLock,
  updateLog, 
  deleteExpiredUpdates, 
  newEmptyLogLock,
  newEmptyRequestLock
  ) where

import Definitions
import Concurrency.Data.Log
import Concurrency.Data.TTrie
import Concurrency.Util.Stats
import Syntax

import Control.Concurrent
import Control.Exception (mask_)
import Data.List ( foldl' )
import Data.Map  ( lookup, update, empty, elems, insert )
import qualified Data.Set as S( delete, insert )
import Prelude hiding (lookup, log)
import System.IO.Unsafe (unsafePerformIO)
import Data.Maybe (catMaybes)


-- Scheduler API 
data Request  = S SendRequest 
              | R ReceiveRequest
              | UndoS ChannelName ThreadId 
              | UndoR ChannelName ThreadId
              | C TransactionNames TransactionName 
              | Sp ThreadId Alternatives 
              | Emb ThreadId Alternatives 
              | Dead ThreadId TransactionNames
              | Wait ThreadId
              | Terminate (Maybe ThreadId)
              | Success
  deriving Show



-- data accessors
type LogLock = MVar Log
type RequestLock = MVar [Request]


newEmptyLogLock :: IO LogLock
newEmptyLogLock =  newMVar newEmptyLog

newEmptyRequestLock :: IO RequestLock
newEmptyRequestLock = newMVar []


-----------------------------------
 -- Gatherer Requests Handling ---
-----------------------------------

killAllThreads :: Log -> Maybe ThreadId  -> IO ()
killAllThreads log mainThreadId = do
  let threadIDs = allThreads (ttrie log)
  
  mapM_ killThread $ 
    case mainThreadId of
        Nothing   -> threadIDs
        Just main -> filter (/= main) threadIDs
  myThreadId >>= killThread

removeDeadWorker :: Log -> TransactionNames -> ThreadId -> Log
removeDeadWorker log ks tid = log''
  where
    log'' = log' {ttrie = removeProc ks tid (ttrie log')}
    log'  = log {acks = acks'}
    acks' = S.delete tid (acks log)
    

addAck :: Log -> ThreadId -> Log
addAck log tid = log'
  where
    log' = log {acks = acks'}
    acks' = S.insert tid (acks log)


removeSend :: ChannelName -> ThreadId -> Log -> Log
removeSend c tid log = log'
  where
    log'        = log {reqTable = reqTable'}
    reqTable'   = update f c (reqTable log)
    f (CR xs y) = Just $ CR (filter g xs) y
    g (SR tid' _ _ _ _) = tid /= tid'

removeRecv :: ChannelName -> ThreadId -> Log -> Log
removeRecv c tid log = log'
  where
    log'        = log {reqTable = reqTable'}
    reqTable'   = update f c (reqTable log)
    f (CR x ys) = Just $ CR x (filter g ys)
    g (RR tid' _ _ _) = tid /= tid'


addSuccess :: Log -> Log
addSuccess l = log
  where
    log = l {stats = s'}
    s' = s {throughPut = throughPut s + 1}
    s = stats l 


updateLog_ :: Log -> Request -> Log    
updateLog_ log request = case request of
  S req             -> addSend log req 
  R req             -> addRecv log req
  C ks k            -> addCommit ks k log
  Sp tid ks         -> updateEmbeddings log (tid, ks)
  Emb tid ks        -> updateEmbeddings log (tid, ks)
  Dead tid ks       -> removeDeadWorker log ks tid
  Wait tid          -> addAck log tid
  Terminate mainId  -> unsafePerformIO $ mask_ $ killAllThreads log mainId >> return log
  Success           -> addSuccess log
  UndoS chan tid    -> removeSend chan tid log
  UndoR chan tid    -> removeRecv chan tid log


updateLog :: Log -> [Request] -> Log    
updateLog = foldl' updateLog_


deleteExpiredUpdates :: [Request] -> [Request]
deleteExpiredUpdates reqs = f reqs empty empty empty
  where 
    f []     m com chm = elems m ++ elems com ++ catMaybes (elems chm) 
    f (r:rs) m com chm = case r of
      Sp tid _    ->     f rs (onlyInsertLatest tid r m) com chm
      Emb tid _   ->     f rs (onlyInsertLatest tid r m) com chm
      Dead tid _  ->     f rs (onlyInsertLatest tid r m) com chm
      C _ k       ->     f rs m (onlyInsertLatest k r com) chm
      Wait _      -> r : f rs m com chm
      S sr        ->     f rs m com (onlyInsertLatest (sid sr) (Just r) chm)
      R rr        ->     f rs m com (onlyInsertLatest (rid rr) (Just r) chm)
      UndoS _ t   -> r : f rs m com (onlyInsertLatest t Nothing chm)
      UndoR _ t   -> r : f rs m com (onlyInsertLatest t Nothing chm)
                              
      Terminate _ -> r : f rs m com chm
      Success     -> r : f rs m com chm
                      
    onlyInsertLatest tid r m = case lookup tid m of
      Nothing -> insert tid r m
      Just _  -> m