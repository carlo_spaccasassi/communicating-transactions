module Concurrency.GuidedHaskellConcurrency where

import Concurrency.Data.Log
import Concurrency.Data.LogRequests
import Concurrency.Util.TrChan hiding ( TC )
import Concurrency.Util.SChan 
import Concurrency.Util.Util
import Syntax 
import Control.Concurrent (myThreadId, forkIO)
import System.IO.Unsafe (unsafePerformIO)
import Control.Concurrent.Chan (Chan, newChan)
import Control.Concurrent.MVar (takeMVar, putMVar, tryTakeMVar)
import Control.Exception (catch, SomeException(..), throw)
import Prelude hiding (catch)
import Data.Unique (hashUnique)

dummyChan :: Chan Value
dummyChan = unsafePerformIO newChan 

doGuidedHaskellConcurrency :: RequestLock -> TProcess -> IO TProcess
doGuidedHaskellConcurrency rlock p = case e of
    NewChannel l         ->     do 
                                    trChan <- newTrChan
                                    let chanName = "c_" ++ show ( hashUnique l )
                                    let newCh  = V (Chan (TrC trChan chanName) 0)
                                    return (makeProc newCh ct ts)      
                        
    Send  (Chan chan@(TrC c _) _) v  ->  
      do
        -- make request
        let ks = map tname (tr p)
        tid <- myThreadId
        pushMVar rlock (S $ SR tid chan Unit dummyChan ks)
        
        -- send
        syncChan <- ks `lookupChannel` c
        
        (do
          putMVar (getWriteVar syncChan) v
        
          -- send ks c v
          takeMVar (getAckVar syncChan)
          _ <- forkIO $ pushMVar rlock (UndoS chan tid)  --async undo send
        
          -- update scheduler
          return (makeProc (V Unit) ct ts))
         `catch` (\ex -> tryTakeMVar (getWriteVar syncChan) >> throw (ex :: SomeException))
                                    
                                    
    Receive (Chan chan@(TrC c _) _)   -> 
      do  
        -- make request
        let ks = map tname (tr p)
        tid <- myThreadId
        pushMVar rlock (R $ RR tid chan dummyChan ks)
        
        -- receive
        syncChan <- ks `lookupChannel` c
        
        v <- takeMVar (getWriteVar syncChan)
        
        putMVar (getAckVar syncChan) ()         
        _ <- forkIO $ pushMVar rlock (UndoR chan tid)         --async undo req
        
        -- update scheduler
        {- NOTE: there is no "catch" operation here because
               putMVar is guaranteed NOT to block if the
               MVar it is going to write on is guaranteed
               to be empty                                 -}
        
        return (makeProc (V v) ct ts)
                                    
    _                        -> error $ "Unexpected expression " ++ show e ++ 
                                        " found during concurrency evaluation"
  where e   = expr p
        ct  = ctx p
        ts  = tr p