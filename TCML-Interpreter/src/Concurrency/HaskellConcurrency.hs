module Concurrency.HaskellConcurrency where

import Data.Unique
import Concurrency.Util.TrChan hiding ( TC )
import Syntax 

doHaskellConcurrency :: TProcess -> IO TProcess
doHaskellConcurrency p = case e of
    NewChannel l            -> do 
                                    trChan <- newTrChan
                                    let chanName = "c_" ++ show ( hashUnique l )
                                    let newCh  = V (Chan (TrC trChan chanName) 0)
                                    return (makeProc newCh ct ts)      
                        
    Send  (Chan (TrC c _) _) v  -> do
                                    let ks = map tname (tr p)
                                    send ks c v
                                    return (makeProc (V Unit) ct ts)
                                    
    Receive (Chan (TrC c _) _)  -> do  
                                    let ks = map tname (tr p)
                                    v <- ks `receive` c

                                    return (makeProc (V v) ct ts)
                                    
    _                        -> error $ "Unexpected expression " ++ show e ++ 
                                        " found during concurrency evaluation"
  where e   = expr p
        ct  = ctx p
        ts  = tr p