
module Concurrency.ServerInit where

import Concurrency.Data.Log
import Concurrency.Data.LogRequests
import Concurrency.GuidedHaskellConcurrency
import Concurrency.HaskellConcurrency
import Concurrency.InteractiveConcurrency
import Concurrency.Scheduler.Random.HRandom
import Concurrency.Scheduler.Interactive.HInteractive
import Concurrency.Scheduler.Interactive.Interactive
import Concurrency.Scheduler.Random.Random
import Concurrency.Scheduler.Staged.SRandom
import Concurrency.Scheduler.Staged.SConcurrencyDriven
import Concurrency.Scheduler.Staged.SConcurrencyDrivenRandomAborts
import Concurrency.Scheduler.Staged.SConcurrencyDrivenTimedAborts
import Concurrency.Scheduler.Staged.SConcurrencyDrivenDeadlocked
import Concurrency.Scheduler.Staged.ThreeWayRendezvous
import Concurrency.Util.Stats


import Syntax                             hiding (pp)
import Concurrency.Util.Util

import Control.Concurrent           (ThreadId, forkIO, threadDelay)
import Control.Concurrent.MVar      (takeMVar, tryTakeMVar, putMVar)
import Control.Monad                (forever)
import Data.Set                     (null)
import Prelude                      hiding (log, null, catch)
import System.CPUTime (getCPUTime, getCPUTime)
import Control.Exception (throw, SomeException, catch)

-- schedule options
data SchedulerKind = 
  RandomSignals        
  {- random signals mode: actions on transactions are selected randomly by the 
     scheduler; the Haskell platform manages channel communications -}
  
  | RandomAll          
  {- random mode: actions on transactions and channel communications are 
     selected randomly by the scheduler -}
  
  | RandomStaged       
    {- random staged mode: as in the random signals mode, but the scheduler 
       follows a staged strategy for selecting actions: 
        do a commit, if none do an embed, if none do an abort -}
  
  | StagedCommunicationDriven 
  {- staged communication driven mode: as in the random staged mode, but now 
     embeddings are communication driven. The scheduler follows the following
     strategy:
      1) commit one of the available transactions, if any is ready
      2) if two processes are trying to synchronize on the same channel but in
         different transactions, try to perform one process into the transaction
         in which the other process is, or try to embed one transaction into the 
         transaction in which the other process is 
      3) if no communication driven embedding is available, perform a random 
         embedding
      4) otherwise perform an abort -}
  
  | StagedCommunicationDriven2
  {- staged communication driven mode 2: as in the previous mode, but 
     without step 3); moreover, step 4) is replaced with the following step:
       4) otherwise, 5% of the times perform a random abort and 95% of the times
          do nothing -}
          
  | StagedCommunicationDriven3
  {- staged communication driven mode 3: as in the mode 2, but 
     without step 4); instead, only the oldest transaction is removed 
     at each step -}
     
  | StagedCommunicationDriven4
  {- staged communication driven mode 4: as in the mode 3, but 
     aborts also occur when the system is deadlocked -}

  | StagedThreeWayRendezvous
  {- Three-way rendezvous scheduler: a scheduler tailored to orchestrate 
     specifically three-way rendezvous. This dummy scheduler simulates a   
     scheduler that implementes solutions from type and effect systems static 
     analisys -}
     
  | InteractiveSignals   
  {- interactive mode: the user will be prompted to choose actions on 
     transactions; channel communications are managed by the Haskell platform -}
  
  | InteractiveAll       
  {- interactive mode: the user will be prompted to choose actions on 
     transactions and channel communications -}
    deriving Eq
  
data ConcurrencyKind = NativeHaskell     -- Processes communicate with Haskell concurrency primitives 
                     | UserInteractive   -- Processes communicate only if the user decides so
                     | GuidedHaskell     {- As in NativeHaskell, but now process will inform the scheduler
                                            whenever they are trying to send or receive a value v on a
                                            channel c -} 
  
whichConcurrency :: SchedulerKind -> ConcurrencyKind
whichConcurrency schedType  = case schedType of
        InteractiveSignals         -> UserInteractive
        InteractiveAll             -> UserInteractive
        RandomAll                  -> NativeHaskell
        RandomSignals              -> NativeHaskell
        RandomStaged               -> NativeHaskell
        StagedCommunicationDriven  -> GuidedHaskell
        StagedCommunicationDriven2 -> GuidedHaskell
        StagedCommunicationDriven3 -> GuidedHaskell
        StagedCommunicationDriven4 -> GuidedHaskell
        StagedThreeWayRendezvous   -> GuidedHaskell

isTimed :: SchedulerKind -> Bool
isTimed StagedCommunicationDriven3 = True
isTimed StagedCommunicationDriven4 = True
--isTimed StagedThreeWayRendezvous   = True
isTimed _                          = False


restoreLog :: LogLock -> Log -> SomeException -> IO ()
restoreLog s log e = putMVar s log >> throw e


-- | init the request gathering thread
doGatherRequests :: Int -> Printer -> RequestLock -> LogLock -> IO ()
doGatherRequests pt pp r s = do
    {- 
      FIXED BUG: newReq was taken before log, and this caused
                 phantom requests that were taken by the gatherer
                 but killed (with abort) from the scheduler 
                 
                 (e.g: 1) gatherer takes a request
                       2) sched takes log
                       3) sched aborts all transactions
                       4) sched puts log back
                       5) gatherer puts phantom request in the log
    -}
    log  <- takeMVar s
    (do
      log' <- waitForAcks pt pp r s log
      putMVar s log'
      pp SStats $ toCSV 0 (stats log')
     )
     `catch` restoreLog s log
    

waitForAcks :: Int -> Printer -> RequestLock -> LogLock -> Log -> IO Log
waitForAcks pt pp r s log = do
    {-
      FIXED BUG 2 : if the log is taken and there is no new request, 
                      the scheduler is starved
    -}
    maybeNewReqs <- tryTakeMVar r
    case maybeNewReqs of
      Nothing       -> do
            threadDelay pt     
            waitForAcks pt pp r s log
      Just reqs  ->  do
        -- print requests in arrival order
        mapM_ (pp SchedRequests . (++) "Req: " . show) (reverse reqs)
        
        let latestReqs = deleteExpiredUpdates reqs
        let log' = updateLog log latestReqs
        putMVar r []
        if (null . acks) log'
          then return log'
          else waitForAcks pt pp r s log'




getScheduler :: SchedulerKind
                           -> Integer 
                           -> Printer
                           -> RequestLock
                           -> Log
                           -> IO Log
getScheduler schedKind abortThreshold = case schedKind of
          InteractiveSignals  -> doHInteractiveScheduler
          InteractiveAll      -> doInteractiveScheduler   
          RandomSignals       -> doHRandomScheduler 
          RandomAll           -> doRandomScheduler 
          RandomStaged        -> doSmarterRandom1Scheduler
          StagedCommunicationDriven  -> doCommunicationDrivenScheduler
          StagedCommunicationDriven2 -> doCommDrivenSchedulerRandomAborts
          StagedCommunicationDriven3 -> doCommDrivenSchedulerTimedAborts abortThreshold
          StagedCommunicationDriven4 -> doCommDrivenSchedulerDeadlocked abortThreshold
          StagedThreeWayRendezvous   -> do3wrScheduling abortThreshold


doServer :: (Log -> IO Log)
                       -> Int
                       -> LogLock
                       -> IO ()
doServer schedule spt slock = do
  -- check the log every 'pt' milliseconds
  threadDelay spt     
  
  log <- takeMVar slock
  (do
    start <- getCPUTime
    log' <- do
      let ackSet = acks log
      if (not . null) ackSet
        then return log
        else schedule log
  
    end <- getCPUTime
    let sTime = end - start
    let currStats = stats log'
    let currentSchedTime = schedTime currStats
    let log'' = log'{stats = currStats{schedTime = currentSchedTime + fromIntegral sTime}}
    
    putMVar slock log'')
    `catch` restoreLog slock log
       



-- | init the gatherer and scheduler threads    
initScheduler :: Integer -> Int -> Int -> SchedulerKind -> Printer 
              -> IO (TProcess -> IO TProcess, RequestLock, LogLock, ThreadId, ThreadId)
initScheduler ath spt gpt sched pp = do 
  -- generate communication vars
  slock       <- newEmptyLogLock
  rlock       <- newEmptyRequestLock 

  -- init scheduler
  let scheduler = getScheduler sched ath pp rlock
  schedID  <- forkIO $ forever (doServer scheduler spt slock)
                    
  -- start gathering request
  gathererID   <- forkIO $ forever (doGatherRequests gpt pp rlock slock)
                           
    
  -- return interactive concurrent reduction with request var
  let doStep = case whichConcurrency sched of
                      UserInteractive -> doInteractiveConcurrency rlock  
                      NativeHaskell   -> doHaskellConcurrency
                      GuidedHaskell   -> doGuidedHaskellConcurrency rlock
                  
  return (doStep, rlock, slock, gathererID, schedID)
