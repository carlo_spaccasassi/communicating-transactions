module Concurrency.Scheduler.Random.Random (doRandomScheduler) where

import Concurrency.Data.Log
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Util.Util

import Prelude hiding (log)

doRandomScheduler :: Printer -> RequestLock -> Log -> IO Log
doRandomScheduler pp r log = do 
    
    let sig     = getSignalChoices log
    let comm    = getCommunicationChoices log
    
    let sigL    = length sig
    let commL   = length comm
    pp CurrentTTrie $ "Transactions Tree:" ++ show (ttrie log)
    pp SchedOptions $ numberList sig
    pp SchedOptions $ concat $ numberList' sigL $ map fst comm
    opChoiceIdx <- getRandomElem' [0.. sigL + commL - 1]
    
    if opChoiceIdx < sigL
      then do
        let choice = sig !! opChoiceIdx
        pp SchedChoice  $ "Sys ==> " ++ show opChoiceIdx ++ ") " ++ show choice
        doLogOperation r log choice
      else do
        let commIdx = opChoiceIdx - sigL
        let choice  = comm !! commIdx
        pp SchedChoice  $ "Sys ==> " ++ show opChoiceIdx ++ ") " ++ fst choice                      
        snd choice