module Concurrency.Scheduler.Random.HRandom (doHRandomScheduler) where

import Concurrency.Data.Log
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Util.Util

import Prelude hiding (log)


-- inits a scheduler that only sends signals. It does not interfere with channel synchronization.
-- The choice of which signal to send is random. 
doHRandomScheduler :: Printer -> RequestLock -> Log -> IO Log
doHRandomScheduler pp r log = do
    let choices = getSignalChoices log    

    if null choices
      then return log
      else
        do
          pp CurrentTTrie "Current Transactions Tree:"
          pp CurrentTTrie $ show (ttrie log)
          pp SchedOptions $ numberList choices
          
          -- pick a random choice     (liftM fromJust unwraps a Maybe)
          choice <- getRandomElem' choices
          pp SchedChoice  $ "Sys ==> " ++ show choice                      
          doLogOperation r log choice