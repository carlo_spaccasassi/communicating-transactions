
module Concurrency.Scheduler.Interactive.HInteractive (doHInteractiveScheduler) where

import Concurrency.Data.Log
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Util.Util

import Prelude hiding (log)


--------------------------------------------------------------------
 ----------------- Guided Concurrency -----------------------------
--------------------------------------------------------------------       
       
-- user options
data UserOption = ChooseSignal | ChooseSync | DoNothing | TerminateThreads


askChoice :: Printer -> String -> RequestLock -> Log -> [LogOperation] -> IO Log
askChoice pp question r log ops = do
  pp SchedOptions question
  opIdx <- getLine
  doLogOperation r log (ops !! (read opIdx :: Int))


-- | initialiases an interactive scheduler. 
--   The interactive scheduler inspects the current log and asks the users 
--   which of the available actions it should take. 
--
--   Actions only comprise "signal" actions.
--     A "signal" action is sending either an "abort", "commit" or "embed" signal to all
--   running processes. Note that the signal is sent to _all_ processes, regardless
--   of which transactions a process might be in.
doHInteractiveScheduler :: Printer -> RequestLock -> Log -> IO Log
doHInteractiveScheduler pp r log = do
    
    -- show commits and print embeds
    let trForest = ttrie log
    pp CurrentTTrie "Current Transactions Forest:"
    pp CurrentTTrie $ show trForest 
    
    -- get available reductions
    let sigChoices = getSignalChoices log
    
    -- prepare user message
    let sigOption   = if null sigChoices  then "" else "  (1) send a signal\n"
    let options     = "Choose one:\n" ++ sigOption ++ "  (2) nothing\n  (3) terminate\n" 
    pp SchedOptions options
    
    -- get user choice
    userOp <- getLine
    let userChoice = case userOp of
          "1" -> ChooseSignal
          "3" -> TerminateThreads
          _   -> DoNothing   
    
    -- return the new log depending on the user choice
    case userChoice of
      ChooseSignal -> do 
            let choices = getSignalChoices log 
            
            pp SchedOptions "Signals available:\n"
            pp SchedOptions $ numberList choices
            askChoice pp "Choose a signal number to send." r log choices
            
      DoNothing         -> return log
    
      TerminateThreads  -> pushMVar r (Terminate Nothing) >> return log
      ChooseSync        -> error "unexpected user choice ChooseSync"