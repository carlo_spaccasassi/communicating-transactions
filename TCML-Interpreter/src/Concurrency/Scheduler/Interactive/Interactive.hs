module Concurrency.Scheduler.Interactive.Interactive (doInteractiveScheduler) where


import Concurrency.Data.Log
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Util.Util

import Prelude hiding (log)


--------------------------------------------------------------------
 ----------------- Guided Concurrency -----------------------------
--------------------------------------------------------------------       
       
-- user options
data UserOption = ChooseSignal | ChooseSync | DoNothing | TerminateThreads


askChoice :: Printer -> String -> RequestLock -> Log -> [LogOperation] -> IO Log
askChoice pp question r log ops = do
  pp SchedOptions question
  opIdx <- getLine
  doLogOperation r log (ops !! (read opIdx :: Int))

askCommChoice :: Printer -> String -> [IO Log] -> IO Log
askCommChoice pp question ops = do
  pp SchedOptions question
  opIdx <- getLine
  ops !! (read opIdx :: Int)
  
-- | initialiases an interactive scheduler. 
--   The interactive scheduler inspects the current log and asks the users 
--   which of the available actions it should take. 
--
--   Actions comprise either a "signal" or "communication" action.
--     A "signal" action is sending either an "abort", "commit" or "embed" signal to all
--   running processes. Note that the signal is sent to _all_ processes, regardless
--   of which transactions a process might be in.
--     A "communication" action is the synchonization of a sending process and a receiveing
--   process over the same channel. Communication is allowed only if the two processes
--   are in the same stack of transactions.
--
--   While communication is delayed by allowing "communication" actions, it makes
--   the interactive concurrency scheduler a great help in investigating 
--   the concurrent behaviour of processes, as if we were using a "concurrency" debugger.
doInteractiveScheduler :: Printer -> RequestLock -> Log -> IO Log
doInteractiveScheduler pp r log  = do
  -- show commits and print embeds
  let trForest = ttrie log
  pp CurrentTTrie "Current Transactions Forest:"
  pp CurrentTTrie $ show trForest 
  
  -- get available reductions
  let sigChoices = getSignalChoices log
  let commChoices = getCommunicationChoices log
  
  -- prepare user message
  let sigOption   = if null sigChoices  then "" else "  (1) send a signal\n"
  let commOption  = if null commChoices then "" else "  (2) let two processes communicate\n"
  let options     = "Choose one:\n" ++ sigOption ++ commOption ++ "  (3) nothing\n  (4) terminate\n" 
  pp SchedOptions options
  
  -- get user choice
  userOp <- getLine
  let userChoice = case userOp of
        "1" -> ChooseSignal
        "2" -> ChooseSync
        "4" -> TerminateThreads
        _   -> DoNothing   
  
  -- return the new log depending on the user's choice
  case userChoice of
    ChooseSignal -> do 
          let choices = getSignalChoices log 
          
          pp SchedOptions "Signals available:\n"
          pp SchedOptions $ numberList choices
          askChoice pp "Choose a signal number to send." r log choices
          
    ChooseSync -> do
          let choices = getCommunicationChoices log
          
          if (not . null) choices 
            then 
              do 
                pp SchedOptions "Communications available:\n"
                pp SchedOptions $ concat $ numberList' 0 $ map fst choices
                    
                pp SchedOptions "Choose a synchronization option.\n"
                askCommChoice pp "Choose a synchronization option.\n" (map snd choices)

            else 
                return log
        
    DoNothing         -> return log
  
    TerminateThreads  -> pushMVar r (Terminate Nothing) >> return log