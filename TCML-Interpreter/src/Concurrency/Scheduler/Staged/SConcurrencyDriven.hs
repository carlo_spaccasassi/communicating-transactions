
module Concurrency.Scheduler.Staged.SConcurrencyDriven where

import Concurrency.Data.Log
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Scheduler.Staged.CommDrivenHelper
import Concurrency.Util.Util

import Data.Map (elems)
import Prelude hiding (log)






getPrioritizedChoices :: Printer -> Integer -> Log -> IO [LogOperation] 
getPrioritizedChoices pp p log 
  |            p < 98 = commits  `orElseM`  getPrioritizedChoices pp 98 log
  | 98 <= p && p < 99 = embeds   `orElseM`  getPrioritizedChoices pp 99 log
  | otherwise         = aborts   `orElseM`  doNothings
  where 
    commits    = return (getCommitChoices log)
    embeds     = getCDEmbeddingChoices log channels 
    aborts     = sometimes (return $ getAbortChoices  log)
    doNothings = return [getDoNothingChoice log]
    
    channels   = elems (reqTable log)


-- inits a scheduler that only sends signals. It does not interfere 
-- with channel synchronization.
-- The choice of which signal to send is random. 
doCommunicationDrivenScheduler :: Printer -> RequestLock -> Log -> IO Log
doCommunicationDrivenScheduler pp r log = do
    
    p <- getRandomPercentage
    choices <- getPrioritizedChoices pp p log
    --let embeds  = getEmbedChoices r log
    
    pp CurrentTTrie "Current Transactions Tree:"
    pp CurrentTTrie $ show (ttrie log)
    pp CurrentTTrie "Current Req Table:"
    pp CurrentTTrie $ ppReqTable (reqTable log)
    
    --pp SchedOptions $ numberList $ map fst choices
--    pp SchedChoice $ "right before choices "++ show choices
    choice <- getRandomElem' choices
    pp SchedChoice  $ "Sys ==> " ++ show choice                      
    doLogOperation r log choice