module Concurrency.Scheduler.Staged.SConcurrencyDrivenTimedAborts 
(doCommDrivenSchedulerTimedAborts) where


import Concurrency.Data.Log 
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Data.TTrie
import Concurrency.Scheduler.Staged.CommDrivenHelper
import Concurrency.Util.Util

import Data.Map (elems)
import Prelude hiding (log)
import System.CPUTime (getCPUTime)


getPrioritizedChoices :: Integer  
                         -> Integer 
                         -> Integer 
                         -> Log 
                         -> IO [LogOperation] 
getPrioritizedChoices c p t log 
  |            p < 98 = return commits `orElseM` getPrioritizedChoices c 98 t log
  | 98 <= p && p < 99 = embeds         `orElseM` getPrioritizedChoices c 99 t log
  | otherwise         = aborts         `orElseM` return doNothings
  where 
    commits    = getCommitChoices log
    embeds     = getCDEmbeddingChoices log channels 
    aborts     = sometimes (getTimedOutTransactions c t log)
    doNothings = [getDoNothingChoice log]
    channels   = elems (reqTable log)


-- inits a scheduler that only sends signals. It does not interfere with channel synchronization.
-- The choice of which signal to send is random. 
doCommDrivenSchedulerTimedAborts :: Integer -> Printer -> RequestLock -> Log -> IO Log
doCommDrivenSchedulerTimedAborts threshold pp r l = do
    -- init the transaction trie timestamps
    currentTime <- getCPUTime
    let log = l {ttrie = initTTrieTime currentTime (ttrie l) }
    
    p <- getRandomPercentage
    choices <- getPrioritizedChoices currentTime p threshold log 
    
    pp CurrentTTrie "Current Transactions Tree:"
    pp CurrentTTrie $ show (ttrie log)
    pp CurrentTTrie "Current Req Table:"
    pp CurrentTTrie $ ppReqTable (reqTable log)
    
    --pp SchedOptions $ numberList $ map fst choices
    choice <- getRandomElem' choices
    pp SchedChoice  $ "Sys ==> " ++ show choice                      
    doLogOperation r log choice