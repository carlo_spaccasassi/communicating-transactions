module Concurrency.Scheduler.Staged.SRandom (doSmarterRandom1Scheduler) where

import Concurrency.Data.Log
import Concurrency.Data.LogOperation 
import Concurrency.Data.LogRequests
import Concurrency.Util.Util

import Prelude hiding (log)

import System.Random

-- Staged scheduler: sends a random signal, prioritizing commits first, 
-- then embeds and then aborts. This does not interfere with message passing.

getPrioritizedChoices :: Integer -> Log -> [LogOperation] 
getPrioritizedChoices p log 
  |            p < 98 = commits `orElse` getPrioritizedChoices 98 log
  | 98 <= p && p < 99 = embeds  `orElse` getPrioritizedChoices 99 log
  | otherwise         = aborts  `orElse` doNothings
  where 
    commits    = getCommitChoices log
    embeds     = getEmbedChoices  log
    aborts     = getAbortChoices  log
    doNothings = [getDoNothingChoice log]


doSmarterRandom1Scheduler :: Printer -> RequestLock -> Log -> IO Log
doSmarterRandom1Scheduler pp r log = do
    
    p <- getRandomPercentage
    let choices = getPrioritizedChoices p log

    pp CurrentTTrie "Current Transactions Tree:"
    pp CurrentTTrie $ show (ttrie log)
    pp SchedOptions $ numberList choices
    opChoiceIdx <- getStdRandom (randomR (0, length choices - 1))
    let choice = choices !! opChoiceIdx
    pp SchedChoice  $ "Sys ==> " ++ show opChoiceIdx ++ ") " ++ show choice                      
    doLogOperation r log choice