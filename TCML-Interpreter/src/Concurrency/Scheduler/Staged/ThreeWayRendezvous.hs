
module Concurrency.Scheduler.Staged.ThreeWayRendezvous 
(do3wrScheduling) where


import Concurrency.Data.Log 
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Data.TTrie
import Concurrency.Scheduler.Staged.CommDrivenHelper
import Concurrency.Util.Util

import Data.Map (elems)
import Prelude hiding (log)
import System.CPUTime (getCPUTime)
import Debug.Trace (trace)


--get3wrEmbedChoices log channels 


getPrioritizedChoices :: Integer  
                         -> Integer 
                         -> Integer 
                         -> Log 
                         -> IO ([[LogOperation]], Log) 
getPrioritizedChoices c p t log 
  |            p < 98 = commits  `orElseM2` getPrioritizedChoices c 98 t log
  | 98 <= p && p < 99 = embeds   `orElseM2` getPrioritizedChoices c 99 t log
  | otherwise         = aborts   `orElseM2` return (doNothings, log)
  where 
    commits    =  let coch = getCommitChoices log
                  in if null coch
                      then return ([], log)
                      else return ([coch], log)
    embeds     = do log' <- get3wrSolutions log channels
                    (cs, l) <- get3wrChoice log' channels
--                    trace ("embed choices: " ++ show cs ) ( return () )
                    return (cs, l)
    aborts     = do z <- sometimes (do ts <- getTimedOutTransactions c t log
                               -- lift each element of ts to a singleton list
                                       return ( map (: []) ts ) )
                    return (z, log)            
    doNothings = [[getDoNothingChoice log]]
    channels   = elems (reqTable log)





-- embeds groups of three transactions into each other. 
-- It does not interfere with channel synchronization.
-- The choice of which signal to send is random. 
do3wrScheduling :: Integer -> Printer -> RequestLock -> Log -> IO Log
do3wrScheduling threshold pp r l = do
    -- init the transaction trie timestamps
    currentTime <- getCPUTime
    let log = l {ttrie = initTTrieTime currentTime (ttrie l) }
    
    p <- getRandomPercentage
    (choices, log') <- getPrioritizedChoices currentTime p threshold log 
    
    pp CurrentTTrie "Current Transactions Tree:"
    pp CurrentTTrie $ show (ttrie log')
    pp CurrentTTrie "Current Req Table:"
    pp CurrentTTrie $ ppReqTable (reqTable log')
    pp CurrentTTrie "Current Solutions:"
    pp CurrentTTrie $ show (solutions log')
    
    --pp SchedOptions $ numberList $ map fst choices
    choice <- getRandomElem' choices
    trace ("choices: " ++ show choices ) (return ())
    case choice of 
      cc@(c : cs)   -> do pp SchedChoice  $ "Sys ==> " ++ show cc
                          let log'' = if null cs 
                                      then log'
                                      else log' {solutions = cs : solutions log' }
                          doLogOperation r log'' c
                                           
      []            -> do let c = getDoNothingChoice log'
                          pp SchedChoice  $ "Sys ==> " ++ show c
                          doLogOperation r log' c