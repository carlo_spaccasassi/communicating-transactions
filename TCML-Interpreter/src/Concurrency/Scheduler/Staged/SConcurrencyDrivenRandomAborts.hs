
module Concurrency.Scheduler.Staged.SConcurrencyDrivenRandomAborts where

import Concurrency.Data.Log 
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Scheduler.Staged.CommDrivenHelper
import Concurrency.Util.Util

import Data.Map (elems)
import Prelude hiding (log)


getPrioritizedChoices :: Integer  
                         -> Log 
                         -> IO [LogOperation] 
getPrioritizedChoices p log 
  |            p < 75 = return commits `orElseM` getPrioritizedChoices 75 log
  | 75 <= p && p < 95 = embeds         `orElseM` getPrioritizedChoices 95 log
  | otherwise         = aborts         `orElseM` return doNothings
  where 
    commits    = getCommitChoices log
    embeds     = getCDEmbeddingChoices log channels 
    aborts     = sometimes $ return (getAbortChoices log)  
    doNothings = [getDoNothingChoice log]
    channels   = elems (reqTable log)



-- inits a scheduler that only sends signals. It does not interfere with channel synchronization.
-- The choice of which signal to send is random. 
doCommDrivenSchedulerRandomAborts :: Printer -> RequestLock -> Log -> IO Log
doCommDrivenSchedulerRandomAborts pp r log = do
    -- init the transaction trie timestamps
    p <- getRandomPercentage
    choices <- getPrioritizedChoices p log 
    
    pp CurrentTTrie "Current Transactions Tree:"
    pp CurrentTTrie $ show (ttrie log)
    pp CurrentTTrie "\nCurrent Req Table:"
    pp CurrentTTrie $ ppReqTable (reqTable log)
    
    choice <- getRandomElem' choices
    pp SchedChoice  $ "Sys ==> " ++ show choice                      
    doLogOperation r log choice