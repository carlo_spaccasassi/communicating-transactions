module Concurrency.Scheduler.Staged.CommDrivenHelper where

import Concurrency.Util.Util
import Concurrency.Data.TTrie
import Concurrency.Data.Log
import Concurrency.Data.LogOperation
import Definitions

import Control.Concurrent (ThreadId)
import Data.Maybe (fromJust)
import qualified Data.Set as Set
import Prelude hiding (log)
--import Debug.Trace (trace)

data EmbedKind = ProcessEmbed TransactionNames ThreadId TransactionName
               | TransactionEmbed TransactionNames TransactionName TransactionName



--------------------------------------------------------------------
 ----------------- Communication Driven Embedding -----------------
--------------------------------------------------------------------

checkWhichEmbedding' :: TransactionNames 
                          -> TransactionNames 
                          -> (ThreadId, ThreadId) 
                          -> TransactionNames
                          -> Maybe EmbedKind
checkWhichEmbedding' [] [] _ _                 = Nothing
checkWhichEmbedding' []     (rk:_) (i, _) ks   = Just (ProcessEmbed ks i rk)
checkWhichEmbedding' (sk:_) []     (_, i) ks   = Just (ProcessEmbed ks i sk)
checkWhichEmbedding' (sk:sks) (rk:rks) pids ks = 
  if sk == rk
    then checkWhichEmbedding' sks rks pids (ks ++ [sk])
    else Just (TransactionEmbed ks sk rk)   


checkWhichEmbedding :: TransactionNames 
                        -> TransactionNames 
                        -> (ThreadId, ThreadId) 
                        -> Maybe EmbedKind
checkWhichEmbedding sks rks pids = checkWhichEmbedding' sks rks pids []


getCommDrivenEmbedding :: Log -> ChannelRequests -> IO LogOperation
getCommDrivenEmbedding log (CR sendReqs recvReqs) = 
  if null sendReqs || null recvReqs
    then return Nothing
    else do
      
      SR id1 _ _ _ sks <- getRandomElem' sendReqs
      RR id2 _ _ rks   <- getRandomElem' recvReqs
      
      case checkWhichEmbedding sks rks (id1, id2) of
            Just (ProcessEmbed ks pid k) -> 
              return $ getEmbedChoice ks [] [pid] k
              
            Just (TransactionEmbed ks sk' rk') -> do
              let embChoices = [(sk', rk'), (rk', sk')]
              (ck, ck2) <- getRandomElem' embChoices
              let tnamesIn_ck2= fromJust $ allTransactionNamesAt (ks ++ [ck2]) (ttrie log)
              let delta2 = ck2 : tnamesIn_ck2
              let tEmbed = getEmbedChoice ks [delta2] [] ck
              return tEmbed
              
            Nothing -> return Nothing


getCDEmbeddingChoices :: Log 
                          -> [ChannelRequests] 
                          -> IO [LogOperation]
getCDEmbeddingChoices log chans = do
  maybePerm <- getRandomPermutation chans
  case maybePerm of
    Nothing       -> return []
    Just (c, cs)  -> do
      maybeChoice <- getCommDrivenEmbedding log c
      case maybeChoice of
        Nothing -> getCDEmbeddingChoices log cs
        Just _  -> return [maybeChoice]





get3wrEmbedding :: Log -> ChannelRequests -> IO Log
get3wrEmbedding log chans = do
  let ts = getSolBoundTNames ( solutions log )
  let (CR sendReqs recvReqs) = filterChannelRequests ts chans
--  trace ("chans are " ++ show chans ++
--    "\nts is "     ++ show ts ++
--    "\nrecvs are " ++ show recvReqs ++
--    "\nsends are " ++ show sendReqs ) (return ())
  if null recvReqs            -- no leaders left
    || length sendReqs <= 1   -- no or only one follower left
    then return log
    else do
      -- get a random leader and two random followers      
      Just (RR id1 _ _   [k1], recvReqs')  <- getRandomPermutation recvReqs
      Just (SR id2 _ _ _ [k2], sendReqs')  <- getRandomPermutation sendReqs
      Just (SR _   _ _ _ [k3], sendReqs'') <- getRandomPermutation sendReqs'
      
      -- prepare a sequence of log operations that will make the rendezvous succeed
      let solution = [
                        Just (Embed []        [[k2], [k3]]  []        k1),
                        Just (Embed [k1]      [[k3]]      [id1]       k2), 
                        Just (Embed [k1, k2]  []          [id1, id2]  k3)
                    ] 
      
      -- calculate more solutions, if any
      let log' = log {solutions = solution : solutions log }
      get3wrEmbedding log' (CR sendReqs'' recvReqs' )


getSolBoundTNames :: [[LogOperation]] -> [TransactionName]
getSolBoundTNames x = Set.toList $ foldr 
                        ( flip
                          (foldr
                             (\ maybeTSignal names' ->
                                case maybeTSignal of
                                    Nothing -> names'
                                    Just (Commit ts t) -> Set.fromList (t : ts) `Set.union` names'
                                    Just (Abort ts t) -> Set.fromList (t : ts) `Set.union` names'
                                    Just (Embed ts ts' _ t) -> Set.fromList (t : ts ++ concat ts')
                                                                 `Set.union` names')) 
                            )
                        Set.empty x


get3wrSolutions :: Log -> [ChannelRequests] -> IO Log
get3wrSolutions log chans = do
          maybePerm <- getRandomPermutation chans
          case maybePerm of
            Nothing       -> return log
            Just (c, cs)  -> do
              log' <- get3wrEmbedding log c
              get3wrSolutions log' cs
              


filterChannelRequests :: TransactionNames -> ChannelRequests -> ChannelRequests
filterChannelRequests ts ( CR ss rr ) = CR ss' rr' 
  where ss' = filter (\(SR _ _ _ _ ks)  -> not (any (`elem` ts) ks) && length ks == 1) ss 
        rr' = filter (\(RR _ _ _ ks)    -> not (any (`elem` ts) ks) && length ks == 1) rr



--------------------------------------------------------------------
 ----------------- 3-way rendezvous -------------------------------
--------------------------------------------------------------------


--get3wrChoices' :: Log -> ChannelRequests -> IO LogOperation
--get3wrChoices' log (CR sendReqs recvReqs) = 
--  if null sendReqs || null recvReqs
--    then return Nothing
--    else do
--      
--      SR id1 _ _ _ sks <- getRandomElem' sendReqs
--      RR id2 _ _ rks   <- getRandomElem' recvReqs
--      
--      case checkWhichEmbedding sks rks (id1, id2) of
--            Just (ProcessEmbed ks pid k) -> 
--              return $ getEmbedChoice ks [] [pid] k
--              
--            Just (TransactionEmbed ks sk' rk') -> do
--              let embChoices = [(sk', rk'), (rk', sk')]
--              (ck, ck2) <- getRandomElem' embChoices
--              let tnamesIn_ck2= fromJust $ allTransactionNamesAt (ks ++ [ck2]) (ttrie log)
--              let delta2 = ck2 : tnamesIn_ck2
--              let tEmbed = getEmbedChoice ks [delta2] [] ck
--              return tEmbed
--              
--            Nothing -> return Nothing


get3wrChoice :: Log -> [ChannelRequests] -> IO ([[LogOperation]], Log)
get3wrChoice log chans = do
  let sols = solutions log
  randomSols <- getRandomPermutation sols
  case randomSols of
    Nothing           ->  return ([], log)
    Just ([], rest)   ->  let log' = log {solutions = rest } 
                          in get3wrChoice log' chans
    Just (s:ss, rest) ->  let log' = log {solutions = if null ss
                                                          then rest
                                                          else ss:rest}
                          in return ([[s]], log')

  

--  maybePerm <- getRandomPermutation chans
--  case maybePerm of
--    Nothing       -> return []
--    Just (c, cs)  -> do
--      maybeChoice <- get3wrChoices' log c
--      case maybeChoice of
--        Nothing -> get3wrChoices log cs
--        Just _  -> return [maybeChoice]








--------------------------------------------------------------------
 ----------------- Timed Transactions -----------------------------
--------------------------------------------------------------------
getTimedOutTransactions :: Integer 
                            -> Integer 
                            -> Log 
                            -> IO [LogOperation]   
getTimedOutTransactions c t log = 
  case oldestTransactions (ttrie log) of
    Nothing -> return []
    Just mins -> do
      let (_, oldestTime) = head mins
      if c - oldestTime > t
        then do
          i <- getRandomIdx mins
          let (ks, _) =  mins !! i
          let abortOldest = getAbortChoice [] (last ks) 
           
          return [abortOldest]
        else
          return []
          

sometimes :: IO [a] -> IO [a]
sometimes a = do
  p <- getRandomPercentage
  if p > 85 
    then a
    else return []