module Concurrency.Scheduler.Staged.SConcurrencyDrivenDeadlocked (
  doCommDrivenSchedulerDeadlocked
) where

import Concurrency.Data.Log
import Concurrency.Data.LogOperation
import Concurrency.Data.LogRequests
import Concurrency.Data.TTrie
import Concurrency.Scheduler.Staged.CommDrivenHelper
import Concurrency.Util.Util

import Data.Map (elems)
import Prelude hiding (log)
import System.CPUTime (getCPUTime)

getPrioritizedChoices :: Integer  
                         -> Printer ->Integer 
                         -> Integer 
                         -> Log
                         -> IO [LogOperation] 
getPrioritizedChoices c pp p t log 
  | isDeadlocked log  = forcedAborts `orElseM` doNothings
  |            p < 98 = commits `orElseM` getPrioritizedChoices c pp 98 t log
  | 98 <= p && p < 99 = embeds  `orElseM` getPrioritizedChoices c pp 99 t log
  | otherwise         = aborts  `orElseM` doNothings
  where 
    commits      = return (getCommitChoices log)
    embeds       = getCDEmbeddingChoices log channels 
    forcedAborts = return (getAbortChoices log)
    aborts       = sometimes (getTimedOutTransactions c t log)
    doNothings   = return [getDoNothingChoice log]
    channels     = elems (reqTable log)




-- inits a scheduler that only sends signals. It does not interfere with channel synchronization.
-- The choice of which signal to send is random. 
doCommDrivenSchedulerDeadlocked :: Integer -> Printer -> RequestLock -> Log -> IO Log
doCommDrivenSchedulerDeadlocked threshold pp r l = do
    -- init the transaction trie timestamps
    currentTime <- getCPUTime
    let log = l {ttrie = initTTrieTime currentTime (ttrie l) }
    
    p <- getRandomPercentage
    choices <- getPrioritizedChoices currentTime pp p threshold log 
    
    pp CurrentTTrie "Current Transactions Tree:"
    pp CurrentTTrie $ show (ttrie log)
    pp CurrentTTrie "\nCurrent Req Table:"
    pp CurrentTTrie $ ppReqTable (reqTable log)
    
    choice <- getRandomElem' choices
    pp SchedChoice  $ "Sys ==> " ++ show choice                      
    doLogOperation r log choice