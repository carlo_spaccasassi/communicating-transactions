module Parser (parser) where

import qualified Text.ParserCombinators.Parsec.Token as T
import qualified Text.ParserCombinators.Parsec as P
import qualified Text.Parsec.Expr as Ex
import Text.ParserCombinators.Parsec.Language
import Text.Parsec.Prim

import Data.Functor.Identity

import Definitions
import Syntax
--import Debug.Trace (trace)

-------------------
-- Parser
-------------------

-- Lexer & Parser
tcmlStyle :: LanguageDef st
tcmlStyle = haskellStyle
              {
                reservedOpNames= ["=", "\\", "->", "<=", "==", "||", "[", "|>", "]", ","],
                reservedNames  = ["let", "in", "if", "then", "else", "true", "false", "unit", "fun", "send", "receive", "newChannel", "co", "fst", "snd", "NIL", "Int", "Bool", "Unit", "Tup", "Arrow", "Chan", "success"] 
              }

lexer :: T.TokenParser ()
lexer = T.makeTokenParser tcmlStyle

whiteSpace :: Text.Parsec.Prim.ParsecT String () Data.Functor.Identity.Identity ()
whiteSpace = T.whiteSpace lexer

parens :: ParsecT String () Identity a -> ParsecT String () Identity a
parens     = T.parens lexer

identifier :: ParsecT String () Identity String
identifier = T.identifier lexer

reserved :: String -> ParsecT String () Identity ()
reserved = T.reserved lexer

reservedOp :: String -> ParsecT String () Identity ()
reservedOp = T.reservedOp lexer

integer :: ParsecT String () Identity Integer
integer = T.integer lexer




-- Expression parsing
pAtom :: P.Parser Expression
pAtom = 
        do
          _   <- reserved "fun"
          f   <- identifier
          -- TODO: check that test examples compile
          x <- parens identifier
          _   <- whiteSpace
          _   <- P.string "="
          _   <- whiteSpace
          e   <- pExpr
          return $ V $ Fun f x (abstract 1 x  (abstract 0 f e))
     <|> 
        do
        _ <- reserved "true"
        return $ V $ B True
     <|> 
        do
        _ <- reserved "false"
        return $ V $ B False
     <|>
        do
        _ <- reserved "()"
        return $ V Unit
     <|>
        P.try (
          do
            n <- integer
            return $ V $ N n
        )        
     <|>
        do
        s <- identifier
        return $ V $ Free s
     <|>
        parens (do 
            e1 <- pExpr
            P.choice [ P.try (
                      do
                        _ <- reserved ","
                        e2 <- pExpr
                        return $ Tup e1 e2
                    ),
                    return e1])
     <?> "parsing a pAtom"


pTerm :: P.Parser Expression
pTerm =
        do
          _   <- reserved "success"
          return SignalSuccess
     <|>
        do
          _   <- reserved "let"  
          x   <- identifier
          _   <- reservedOp "="
          e1  <- pExpr
          _   <- reserved "in"
          e2  <- pExpr
          return $ Let x e1 (abstract 0 x e2)
     <|>
        do
          _   <- reserved "if"  
          e1  <- pExpr
          _   <- reservedOp "then"
          e2  <- pExpr
          _   <- reserved "else"
          e3  <- pExpr
          return $ If e1 e2 e3
     <|>
        do
          _   <- reserved "newChannel"  
--          _   <- reserved ":"
--          t'  <- pType
--          let t = TChan 
--                (error "Trying to evaluate an uninstantiated newChan (type) type.")
--                (error "Trying to evaluate an uninstantiated newChan (region) type.")
--          e   <- pExpr
          return $ NewChannel undefined -- (Free c t) (abstract 0 t c e) t (error "trying to eval newchan2") 
     <|>
        do
          _   <- reserved "send"  
          v1   <- pAtom
          v2   <- pAtom
          return $ Send (unwrapVal v1) (unwrapVal v2)
     <|>
        do
          _    <- reserved "receive"  
          v1   <- pAtom
          return $ Receive (unwrapVal v1)
     <|> do
          _   <- reserved "co"
          k   <- identifier
          return $ Co (FTN k)
     <|> do
          _   <- reservedOp "["
          e1  <- pExpr
          _   <- reservedOp "|>"
          k   <- identifier
          e2  <- pExpr
          _   <- reservedOp "]"
          return $ Tr (FTN k) (abstractFTN 0 k e1) e2
     <|> do
          _   <- reservedOp "fst"
          e1  <- pExpr
          return $ Fst e1
     <|> do
          _   <- reservedOp "snd"
          e1  <- pExpr
          return $ Snd e1
     <|> do
          _   <- reservedOp "NIL"
          return NIL
     <|> pAtom
     <?> "failed to match expression"

pExpr :: ParsecT String () Identity Expression
pExpr = do
  es <- P.many1 (pOp <|> pTerm)
  return (foldl1 App es)


pOp :: ParsecT String () Identity Expression
pOp = Ex.buildExpressionParser table pTerm
  where infixOp x f = Ex.Infix (reservedOp x >> return f)
        table = [[infixOp "*"  (Op "*")  Ex.AssocLeft],
                 [infixOp "+"  (Op "+")  Ex.AssocLeft,
                  infixOp "-"  (Op "-")  Ex.AssocLeft],
                 [infixOp "<=" (Op "<=") Ex.AssocLeft,
                  infixOp "==" (Op "==") Ex.AssocLeft],
                 [infixOp "||" Par       Ex.AssocLeft]]


--allexprs = buildExpressionParser [
--  [],
--  [],
--  [],
--  [],   do
--      
--     e1 <- 
--     choice [ P.try (
--                do
--                  _   <-  reservedOp "+"
--                  e2  <-  pTerm
--                  return $ Op "+" e1 e2 
--              ),
--              P.try (
--                do
--                  _   <-  reservedOp "-"
--                  e2  <-  pTerm
--                  return $  e1 e2 
--              ),
--              P.try (
--                do 
--                  _ <- reservedOp "*"
--                  e2 <- pTerm
--                  return $ Op e1 e2 "*"
--              ),
--              P.try (
--                do 
--                  _ <- reservedOp "<="
--                  e2 <- pTerm
--                  return $ Op e1 e2 "<="
--              ),
--              P.try (
--                do 
--                  _ <- reservedOp "=="
--                  e2 <- pTerm
--                  return $ Op e1 e2 "=="
--              ),
--              P.try (
--                do
--                  _ <- reservedOp "||"
--                  e2 <- pTerm
--                  return $ Par e1 e2
--              ),
--              P.try (
--              do 
--                e2 <- pAtom
--                return $ App e1 e2
--              ),
--              return e1]

pTop :: P.Parser Expression
pTop = do
        whiteSpace
        t <- pExpr
        P.eof
        return t


-- The main parser function.
-- First argument is file name, second argument is the program in a string.
-- Returns either a parse error or the AST of the top-level program expression.
parser :: String -> String -> Either P.ParseError Expression
parser = parse pTop 
