module Syntax where

import Concurrency.Util.TrChan
import Data.Unique (Unique, hashUnique)
import Text.PrettyPrint.HughesPJ(integer, text, (<>), Doc, render, parens, nest, ($$))
import Definitions




-- TCML values syntax
data Value = 
    N Integer 
  | B Bool 
  | Unit 
  | VTup Value Value
  | Free Var
  | Bound Int Var                 -- Int is a De-Brujin index
  | Fun Var Var Expression
  | Chan ChannelName Int          -- Int is a De-Brujin index
  deriving (Eq)

instance Show Value where show = render . ppValue 0


-- TCML expressions syntax
data Expression =
            -- values 
              V Value
            -- tuples
            | Tup Expression Expression
            | Fst Expression
            | Snd Expression
            -- lambda terms
            | App Expression Expression
            | Let Var Expression Expression
            | If Expression Expression Expression
            | Op String Expression Expression
            | NIL
            -- ccs terms
            | Par Expression Expression
            | Send Value Value               -- first arg must be a channel
            | Receive Value                  -- arg must be a channel
            | NewChannel Unique 
            -- tccs terms
            | Co TransactionName
            | Tr TransactionName Expression Expression
            -- debug terms
            | SignalSuccess
              deriving (Eq) -- Show is defined below

instance Show Expression where show = pp  


-- new types
type Var = String

-- channel names
data ChannelName = CN Unique Var | TrC (TrChan Value) Var
  deriving (Eq, Ord)

instance Show ChannelName where
  show cn = case cn of 
    CN c  z -> z ++ "_" ++ (show.hashUnique) c
    TrC v z -> z ++ "_" ++ show v






-- context definition
type ContextHole = Expression -> Expression
type Context = [ContextHole]

-- transactions stack definition
data Alternative = A {tname :: TransactionName, pr :: TProcess}

instance Show Alternative where
  show a = show (tname a) ++ ": " ++  show (pr a)
  
type Alternatives = [Alternative]

-- TCML process
data TProcess = TP {
  expr :: Expression, 
  ctx :: Context, 
  tr :: Alternatives, 
  toBeUpdated :: Bool,
  toBeKilled :: Bool
}

instance Show TProcess
  where show (TP e _ ts _ _) = show (map tname ts) ++ " " ++ pp e 

----------------------
-- Utils
----------------------
unwrapVal :: Expression -> Value
unwrapVal (V v) = v
unwrapVal _     = error "cannot unwrap expression"

hasEmbedding :: Alternatives -> TransactionName -> Bool
hasEmbedding ts k = k `elem` map tname ts

setUpdate :: TProcess -> TProcess
setUpdate p = p {toBeUpdated = True}


makeProc :: Expression -> Context -> Alternatives -> TProcess
makeProc e c t = TP e c t False False

----------------------
-- Pretty Printing
----------------------

ppValue :: Int -> Value -> Doc
ppValue _ (N n)             = integer n
ppValue _ (B b)             = if b then text "tt" else text "ff"
ppValue _ Unit              = text "unit"
ppValue i (VTup v1 v2)      = text "<" <> ppValue i v1 <> text ", " <> ppValue i v2 <> text ">"
ppValue _ (Free x)        = text x 
ppValue _ (Bound _ x)       = text x
ppValue i (Fun f x e)       = text ("( " ++ f ++ "(" ++ x ++ ") = ") 
                                 <> ppExpr i (subst 0 (V (Free f)) 
                                           (subst 1 (V (Free x)) e)) <> text " )"
ppValue _ ( Chan c _ )        = parens $ text ("chan " ++ show c)


ppOp :: String -> Expression -> Expression -> Doc
ppOp op e1 e2 = parens $ ppExpr 0 e1 <> text (" " ++ op ++ " ") <> ppExpr 0 e2

ppExpr :: Int -> Expression -> Doc
ppExpr i (V v)          = ppValue i v
ppExpr i (App e1 e2)    = parens $ ppExpr i e1 <> text "  " <> ppExpr (i+1) e2
ppExpr i (Let x e1 e2)  = text "let " <> ppValue i (Free x) <> text " = " <> ppExpr i e1 <> text "\nin " <> ppExpr i (subst 0 (V (Free x)) e2)
ppExpr i (If e1 e2 e3)  = text "if (" <> ppExpr i e1 <> text ") " $$ nest (i+1) (text "then " <> ppExpr (i+1) e2 <> text "\n else " <> ppExpr (i+1) e3)
ppExpr _ (Op op e1 e2)  = ppOp op e1 e2
ppExpr _ (NIL)          = text "NIL"

ppExpr i (Tup e1 e2)    = text "(" <> ppExpr i e1 <> text ", " <> ppExpr i e2 <> text ")"
ppExpr i (Fst e1)       = text "fst " <> ppExpr i e1
ppExpr i (Snd e1)       = text "snd " <> ppExpr i e1

ppExpr i (Par e1 e2)    = parens $ ppExpr i e1 <> text "\n || " <> ppExpr i e2
ppExpr _ (NewChannel l) = text "newChannel_" <> text (show (hashUnique l)) 
ppExpr i (Send c v)     = text "send " <> ppValue i c <> text " "  <> ppValue i v
ppExpr i (Receive c)    = text "recv " <> ppValue i c

ppExpr _ (Co k)         = text "co " <> text (show k)
ppExpr i (Tr k e1 e2)   = text "[" 
                     <> ppExpr i e1
                     <> text "\n |> " <> text (show k) <> text " "  
                     <> ppExpr i e2 
                     <> text "]"

ppExpr _ (SignalSuccess)  = text "<success>"

pp :: Expression -> String
pp = render . ppExpr 0
       
----------------------
-- De-Brujin indexes
----------------------

-- shifts values
shiftValBy :: Int -> Int -> Value -> Value
shiftValBy acc i (Bound s x) | s == acc    = Bound (s-i) x
                               | otherwise   = Bound s x
-- (App (Fun f x body) e2) evaluates to (Let x e1 e2[body/f]), 
-- the variable bound by x must be updated (e.g. Bound 1 --> Bound 0)
shiftValBy acc i (Fun x f e)       = Fun x f (shiftBy (acc+2) i e) 
shiftValBy acc i (VTup e1 e2)         = VTup (shiftValBy acc i e1) (shiftValBy acc i e2)
shiftValBy acc i (Chan c s) | s == acc    = Chan c (s-i)
                            | otherwise   = Chan c s
shiftValBy _   _ v                    = v


-- shifts expressions
shiftBy :: Int -> Int -> Expression -> Expression
shiftBy acc i (V v)         = V (shiftValBy acc i v)

shiftBy acc i (If e1 e2 e3)     = If        (shiftBy acc i e1) (shiftBy acc i e2)     (shiftBy acc i e3)
shiftBy acc i (Let x e1 e2)     = Let     x (shiftBy acc i e1) (shiftBy (acc+1) i e2)
shiftBy acc i (App e1 e2)       = App       (shiftBy acc i e1) (shiftBy acc i e2)
shiftBy acc i (Op op e1 e2)     = Op    op  (shiftBy acc i e1) (shiftBy acc i e2)     
shiftBy _   _ NIL               = NIL

shiftBy acc i (Tup e1 e2)       = Tup       (shiftBy acc i e1) (shiftBy acc i e2)
shiftBy acc i (Fst e1)          = Fst       (shiftBy acc i e1)
shiftBy acc i (Snd e1)          = Snd       (shiftBy acc i e1)

shiftBy acc i (Par e1 e2)       = Par       (shiftBy acc i e1)    (shiftBy acc i e2)
shiftBy _   _ (NewChannel l)    = NewChannel l 
shiftBy acc i (Send c v)        = Send      (shiftValBy acc i c)  (shiftValBy acc i v)
shiftBy acc i (Receive c)       = Receive   (shiftValBy acc i c)

shiftBy _   _ (Co k)            = Co k
shiftBy acc i (Tr k e1 e2)      = Tr k      (shiftBy acc i e1) (shiftBy acc i e2)

shiftBy _   _ SignalSuccess     = SignalSuccess

-- shifts a De-Brujin index
shift :: Int -> Expression -> Expression
shift i = shiftBy i 1


-- creates De-Brujin indexes
abstractVal :: Int -> String -> Value -> Value
abstractVal i s (Free s') | s == s'   = Bound i s'
                               | otherwise = Free s' 
abstractVal i s (Fun f x e)     = if s == f || s == x
                                          then Fun f x e
                                          else Fun f x (abstract (i+2) s e)
abstractVal i s (VTup e1 e2)        = VTup (abstractVal i s e1) (abstractVal i s e2)
abstractVal _ _ v                   = v

abstract :: Int -> String -> Expression -> Expression
abstract i s e = case e of 
  V v           ->  V (abstractVal i s v)
  
  If e1 e2 e3     -> If        (abstract i s e1)    (abstract i s e2)   (abstract i s e3)
  Let x t1 t2     -> Let x     (abstract i s t1)    (abstract (i+1) s t2)
  App t1 t2       -> App       (abstract i s t1)    (abstract i s t2)
  Op op t1 t2     -> Op     op (abstract i s t1)    (abstract i s t2)
  NIL             -> NIL
  
  Tup t1 t2       -> Tup       (abstract i s t1)    (abstract i s t2)
  Fst t1          -> Fst       (abstract i s t1)
  Snd t1          -> Snd       (abstract i s t1)
  
  Par t1 t2       -> Par       (abstract i s t1)    (abstract i s t2)
  NewChannel l    -> NewChannel l
  Send c v        -> Send      (abstractVal i s c)  (abstractVal i s v)
  Receive c       -> Receive   (abstractVal i s c)

  Co k            -> Co k
  Tr k e1 e2      -> Tr k      (abstract i s e1)    (abstract i s e2)
  SignalSuccess   -> SignalSuccess    

-- creates De-Brujin indexes for transaction names
abstractValFTN :: Int -> String -> Value -> Value
abstractValFTN i s (Fun f x e)     = Fun f x (abstractFTN i s e)
abstractValFTN i s (VTup v1 v2)       = VTup (abstractValFTN i s v1) (abstractValFTN i s v2)
abstractValFTN _ _ v                  = v

abstractFTN :: Int -> String -> Expression -> Expression
abstractFTN i s e = case e of 
  V v           ->  V (abstractValFTN i s v)
  
  If  e1 e2 e3    -> If        (abstractFTN i s e1)     (abstractFTN i s e2)       (abstractFTN i s e3)
  Let x  t1 t2    -> Let     x (abstractFTN i s t1)     (abstractFTN i s t2) 
  App t1 t2       -> App       (abstractFTN i s t1)     (abstractFTN i s t2)
  Op  op t1 t2    -> Op     op (abstractFTN i s t1)     (abstractFTN i s t2)
  NIL             -> NIL
  
  Tup t1 t2       -> Tup       (abstractFTN i s t1)     (abstractFTN i s t2)
  Fst t1          -> Fst       (abstractFTN i s t1)
  Snd t1          -> Snd       (abstractFTN i s t1)
  
  Par t1 t2       -> Par       (abstractFTN i s t1)     (abstractFTN i s t2)
  NewChannel l    -> NewChannel l
  Send c v        -> Send      (abstractValFTN i s c)   (abstractValFTN i s v)
  Receive c       -> Receive   (abstractValFTN i s c)

  Co k            -> case k of 
                    FTN tn -> if tn == s 
                                then Co (BTN i)
                                else Co k
                    _      -> Co k

  Tr k e1 e2      -> if k == FTN s
                      then Tr k e1 e2  
                      else Tr k (abstractFTN (i+1) s e1) e2
  SignalSuccess   -> SignalSuccess


-- substitutes the i-th bound variable with u in an expression e
substVal :: Int -> Expression -> Value -> Expression
substVal i u v = case v of
  Bound i' x    ->  if i' == i
                        then  u
                        else  V (Bound i' x)  -- non-local to t
  Fun f x  e -> V $ Fun f x (subst (i+2) u e)
  VTup e1 e2 -> V $ let 
                         V v1 = (substVal i u e1) 
                         V v2 = (substVal i u e2)
                     in 
                         VTup v1 v2
  _            -> V v

subst :: Int -> Expression -> Expression -> Expression
subst i u e = case e of 
  V v             ->  substVal i u v

  If e1 e2 e3     -> If    (subst i u e1) (subst i u e2) (subst i u e3)
  Let x t1 t2     -> Let x (subst i u t1) (subst (i+1) u t2)
  App t1 t2       -> App   (subst i u t1) (subst i u t2)
  Op op t1 t2     -> Op op (subst i u t1) (subst i u t2)
  NIL             -> NIL
  
  Tup t1 t2       -> Tup   (subst i u t1) (subst i u t2)
  Fst t1          -> Fst   (subst i u t1)
  Snd t1          -> Snd   (subst i u t1)
  
  Par t1 t2       -> Par (subst i u t1) (subst i u t2)
  NewChannel l    -> NewChannel l
  Send c v        -> Send c' v'
    where c' = case substVal i u c of
                  V (Chan i' z) -> Chan i' z
                  V (Free z)    -> Free z
                  V (Bound z w) -> Bound z w
                  _             -> error $ "cannot substitute expression for channel in Send (" ++ show c ++ ")"
          v' = case substVal i u v of
                  V z -> z
                  _   -> error $ "cannot substitute expression for channel message in Send (" ++ show v ++ ")"

  Receive c     -> Receive c'
    where c' = case substVal i u c of
                  V (Chan i' z) -> Chan i' z
                  V (Free z)    -> Free z
                  V (Bound z w) -> Bound z w
                  _             -> error $ "cannot substitute expression for channel in Receive (" ++ show c ++ " in " ++ show e ++ " )"
                                              
  Co k          ->  Co k
  
  Tr k e1 e2    -> Tr k (subst i u e1) (subst i u e2)
  
  SignalSuccess   -> SignalSuccess
  
-- substitute transaction name
substValTN :: Int -> TransactionName -> Value -> Value
substValTN i u v = case v of
  Fun f x e -> Fun f x (substTN i u e)
  VTup e1 e2   -> VTup (substValTN i u e1) (substValTN i u e2)
  _            -> v

substTN :: Int -> TransactionName -> Expression -> Expression
substTN i u e = case e of 
  V v           -> V     (substValTN i u v)

  If e1 e2 e3     -> If    (substTN i u e1) (substTN i u e2) (substTN i u e3)
  Let x t1 t2     -> Let x (substTN i u t1) (substTN i u t2)
  App t1 t2       -> App   (substTN i u t1) (substTN i u t2)
  Op op t1 t2     -> Op op (substTN i u t1) (substTN i u t2)
  NIL             -> NIL
  
  Tup t1 t2       -> Tup   (substTN i u t1) (substTN i u t2)
  Fst t1          -> Fst   (substTN i u t1)
  Snd t1          -> Snd   (substTN i u t1)
  
  Par t1 t2       -> Par       (substTN i u t1) (substTN i u t2)
  NewChannel l    -> NewChannel l
  Send c v        -> Send      (substValTN i u c) (substValTN i u v)
  Receive c       -> Receive   (substValTN i u c)
     
  Co k            ->  case k of
                      BTN j -> if i == j 
                                then Co u
                                else Co k
                      _           -> Co k
  
  Tr k e1 e2      -> Tr k (substTN (i+1) u e1) (substTN i u e2)
  
  SignalSuccess   -> SignalSuccess
  
  -- gathers the free transaction names in an expression
freeValNames :: Value -> [TransactionName]
freeValNames v = case v of
        Fun _ _ e -> freeTNames e
        VTup e1 e2  -> freeValNames e1 ++ freeValNames e2
        _ -> []

freeTNames :: Expression -> [TransactionName]
freeTNames e = case e of
    -- lambda terms
    V v         -> freeValNames v
    
    App e1 e2       -> freeTNames e1 ++ freeTNames e2
    Let _ e1 e2     -> freeTNames e1 ++ freeTNames e2
    If e1 e2 e3     -> freeTNames e1 ++ freeTNames e2 ++ freeTNames e3
    Op _ e1 e2      -> freeTNames e1 ++ freeTNames e2
    NIL             -> []
    
    Tup e1 e2       -> freeTNames e1 ++ freeTNames e2
    Fst e1          -> freeTNames e1
    Snd e1          -> freeTNames e1
    
    -- ccs terms
    Par e1 e2       -> freeTNames e1 ++ freeTNames e2
    Send _ v2       -> freeValNames v2
    Receive _       -> []
    NewChannel _    -> []
    -- tccs terms
    Co k            -> case k of 
      FTN z         -> [FTN z]
      _             -> []
    Tr k e1 _       -> filter (not . (== k)) (freeTNames e1)
    
    -- debug terms
    SignalSuccess   -> []