
module Definitions where

import Data.Unique (Unique, hashUnique)
import Control.Concurrent (ThreadId)
import Data.Typeable (Typeable)
import Control.Exception (Exception)

-- transaction names
data TransactionName = TN {trId :: Unique} | BTN Int | FTN String
  deriving (Eq, Ord)

instance Show TransactionName where
  show (TN k)  = 'k' : (show.hashUnique) k
  show (BTN k) = 'k' : show k
  show (FTN k) = k

type TransactionNames = [TransactionName]


data TSignal = Commit TransactionNames TransactionName 
             | Abort TransactionNames TransactionName 
             | Embed [TransactionName] [TransactionNames] [ThreadId] TransactionName
     deriving (Typeable)
     
instance Exception TSignal
instance Show TSignal where
  show signal = case signal of 
    Abort  ks k          -> show ks ++ " ab " ++ show k
    Embed  ks d pids k   -> show ks ++ " emb " ++ show d ++ " " ++ show pids ++ show k  
    Commit ks k          -> show ks ++ " co " ++ show k